<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarModels extends Model
{
     protected $table = 'car_models';

     public $timestamps = false;
}
