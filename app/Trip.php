<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    protected $casts = [
        'trip_distance' => 'string',
    ];
    public function driver()
    {
        return $this->belongsTo('App\User','fk_driver_id','id');
         /*//* 
         return $this->belongsTo('App\User','fk_driver_id','id')
         ->join('car_models', 'users.car_model_id', '=', 'car_models.id');  */
    }
    public function rider()
    {
        return $this->belongsTo('App\User','fk_rider_id','id');
    }
    public function company()
    {
        return $this->belongsTo('App\Company','fk_company_id','id');
    }

   
}
