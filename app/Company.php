<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //

    public function drivers()
    {
        return $this->hasMany('App\User','fk_company_id');
    }
}

