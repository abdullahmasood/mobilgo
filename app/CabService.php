<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CabService extends Model
{
    //
    public function drivers()
    {
        return $this->hasMany('App\User','fk_cab_service_id');
    }
}
