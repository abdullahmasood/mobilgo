<?php

namespace App\Http\Controllers\Apis\Passengers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use File;
use Auth;
use App\User;
use App\Company;
use App\Trip;
use App\CabService;
use App\Notification;
use App\Devices;
Use App\Promo;
use \Illuminate\Support\Facades\Validator;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use  App\Http\Controllers\Apis\BaseController as apiBaseController;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Credentials: true');

class PassengersApisController extends apiBaseController
{


  private $stripe_secret; 

  public function __construct()
  {
    $this->stripe_secret = 'sk_test_qxcWlawAwx2qqqjFvshiHVlB';

  }


	public function passengerLogin(Request $request){
		$response = new \stdClass();
		$validator = Validator::make($request->all(), [
			'email' => 'required',
			'password' => 'required',
			'device_type' => 'required',
			'device_token' => 'required',

		]);

		if($validator->fails()){
			$response->status = false;
			$response->code = 400;
			$response->message = 'validation failed';
			$response->data = $validator->errors();
			return response()->json($response);
		} else {
			$user = User::where('email', $request->email)->where('user_type', 'rider')->first();
			if($user){

       // $payment_flag = ( $user->stripe_customer_id == null ) ? 0 : 1 ;
        $payment_flag = $this->CheckCustomerCards($user->id);

                //  $device =  Devices::where('user_id', $user->id)->where([
                //     ['device_type', '=', $request->device_type],
                //     ['device_token', '=', $request->device_token]

                // ])->first();
				if (Hash::check($request->password, $user->password)) {
					$user->remember_token = md5(time());
					$user->latitude = $request->lat;
					$user->longitude = $request->long;
					$user->save();
					$login_user = new \stdClass();
					$login_user->id = $user->id;
					$login_user->name = $user->name;
					$login_user->email = $user->email;
					$login_user->_token = $user->remember_token;
					$login_user->trips = $user->customer_trips;
					$user->password = $request->password;
					$user_device = new Devices();
					$user_device->user_id = $user->id;
					$user_device->device_type = $request->device_type;
					$user_device->device_token = $request->device_token;
					$user_device->save();
          $user->payment_flag = $payment_flag;



					$response->status = true;
					$response->code = 200;
					$response->message = 'login successful';
                    $response->data = $user;// $login_user;
                    return response()->json($response);

                  } 
                  else {

                   $response->status = true;
                   $response->code = 401;
                   $response->message = 'password does not match';
                   $response->data = [];
                   return response()->json($response);
                 }
               }
               else{
                 $response->status = false;
                 $response->code = 404;
                 $response->message = 'Rider not found';
                 $response->data = [];
                 return response()->json($response);
               }
             }
           }



    /*
    * Api for social login
    */

    public function passengerSocialLogin(Request $request)
    {
    	$response = new \stdClass();
    	$user = User::where('email', $request->email)->where('user_type', 'rider')->first();

    	if ($user) {

    		if ($user->oauth_provider == $request->login_type) {
    			$user->oauth_uid_token = $request->oauth_uid_token;
    			$user->save();
    			unset($user->car_model_id, $user->car_number, $user->license_img, $user->fk_company_id ,$user->fk_cab_service_id);

    			$response->status = true;
    			$response->code = 200;
    			$response->message = 'login successful';
                    $response->data = $user;// $login_user;
                    return response()->json($response);

                  }
                  else{
                   unset($user->car_model_id, $user->car_number, $user->license_img, $user->fk_company_id ,$user->fk_cab_service_id);
                   $response->status = false;
                   $response->code = 401;
                   $response->message = 'Your email is already registered with us using '.$user->oauth_provider. ' platform, hence please try to login with correct platform';
                    $response->data = $user;// $login_user;
                    return response()->json($response);

                  }

                }else{

                 $validator = Validator::make($request->all(), [
                  'login_type' => 'required',
                ]);



                 if($validator->fails()){
                  $response->status = false;
                  $response->code = 400;
                  $response->message = 'validation failed';
                  $response->data = $validator->errors();
                  return response()->json($response);
                } 

                else {


                  if ($request->login_type == 'facebook') {
                   $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'email' => 'required',
                    'oauth_uid_token' => 'required',
                    'device_type' => 'required',
                    'device_token' => 'required'
                  ]);



                   if($validator->fails()){
                    $response->status = false;
                    $response->code = 400;
                    $response->message = 'validation failed';
                    $response->data = $validator->errors();
                    return response()->json($response);
                  } else {
                    $user_details = new User();
                    $user_details->remember_token = md5(time());
                    $user_details->name = $request->name;
                    $user_details->email = $request->email;
                    $user_details->oauth_uid_token = $request->oauth_uid_token;
                    $user_details->oauth_provider = $request->login_type;               
                    $user_details->status = "Active";
                    $user_details->user_type = "rider";
                    $user_details->latitude = $request->lat;
                    $user_details->longitude = $request->long;
                    $user_details->save();
                    $user_device = new Devices();
                    $user_device->user_id = $user->id;
                    $user_device->device_type = $request->device_type;
                    $user_device->device_token = $request->device_token;
                    $user_device->save();

                    $details = User::where('id',$user_details->id)->first(); 
                    unset($details->car_model_id, $details->car_number, $details->license_img, $details->fk_company_id ,$details->fk_cab_service_id);
                    $response->status = true;
                    $response->code = 200;
                    $response->message = 'login successful';
                    $response->data = $details;// $login_user;
                    return response()->json($response);


                  }



            } // End of FB register


            //Google register

            if ($request->login_type == 'google') {
            	$validator = Validator::make($request->all(), [
            		'name' => 'required',
            		'email' => 'required',
            		'oauth_uid_token' => 'required',
            		'device_type' => 'required',
            		'device_token' => 'required'
            	]);



            	if($validator->fails()){
            		$response->status = false;
            		$response->code = 400;
            		$response->message = 'validation failed';
            		$response->data = $validator->errors();
            		return response()->json($response);
            	} else {
            		$user_details = new User();
            		$user_details->remember_token = md5(time());
            		$user_details->name = $request->name;
            		$user_details->email = $request->email;
            		$user_details->oauth_uid_token = $request->oauth_uid_token;
            		$user_details->oauth_provider = $request->login_type;
            		$user_details->status = "Active";
            		$user_details->user_type = "rider";
            		$user_details->latitude = $request->lat;
            		$user_details->longitude = $request->long;
            		$user_details->save();

            		$user_device = new Devices();
            		$user_device->user_id = $user_details->id;
            		$user_device->device_type = $request->device_type;
            		$user_device->device_token = $request->device_token;
            		$user_device->save();

            		$details = User::where('id',$user_details->id)->first(); 
            		unset($details->car_model_id, $details->car_number, $details->license_img, $details->fk_company_id ,$details->fk_cab_service_id);
            		$response->status = true;
            		$response->code = 200;
            		$response->message = 'login successful';
                    $response->data = $details;// $login_user;
                    return response()->json($response);


                  }


            } // End of google register


          }


        }




      } 



      /*Find a ride*/

      public function requestRide( Request $request){
       $response = new \stdClass();

       if($request->trip_type == 'schedule_trip'):
        	 $validator = Validator::make($request->all(), [
		        'id' => 'required',
		        'start_lat' => 'required',
		        'start_long' => 'required',
		        'end_lat' => 'required',
		        'end_long' => 'required',
		        'from_address' => 'required',
		        'to_address' => 'required',
		        'cab_service_id' => 'required',
		        'fare' => 'required',
		        'trip_distance' => 'required',
		        'trip_type' => 'required',
		        'trip_date' => 'required',
		        'trip_schedule_time' => 'required',

		      ]);

        	else:
        		 $validator = Validator::make($request->all(), [
			        'id' => 'required',
			        'start_lat' => 'required',
			        'start_long' => 'required',
			        'end_lat' => 'required',
			        'end_long' => 'required',
			        'from_address' => 'required',
			        'to_address' => 'required',
			        'cab_service_id' => 'required',
			        'fare' => 'required',
			        'trip_distance' => 'required',
			        'trip_type' => 'required',
			      ]);
        endif;
     

       if($validator->fails()){
        $response->status = false;
        $response->code = 400;
        $response->message = 'validation failed';
        $response->data = $validator->errors();
        return response()->json($response);
      } else {

        $riderDetails = User::where('id', $request->id)->first();

        $riderRequestProcess = Trip::where('fk_rider_id',$request->id)->where('trip_status','requested')->first(); 

      //  dd($riderRequestProcess);


        if ($riderRequestProcess !== null) {
         $response->status = false;
         $response->code = 402;
         $response->message = 'Your have already requested a ride, please wait for response or cancel the request and try again.';
         return response()->json($response);
       }
       else{
         $number = rand(100,100000);
         $t=time();
         $random = $number.''.$t;
         $trip = new Trip();
         $trip->booking_no = $random;
         $trip->destination_from = $request->from_address;
         $trip->destination_to =  $request->to_address;
         $trip->destination_from_lat = $request->start_lat;
         $trip->destination_from_lng = $request->start_long;
         $trip->destination_to_lat = $request->end_lat;
         $trip->destination_to_lng = $request->end_long;
         $trip->trip_status = 'requested';
         $trip->fk_rider_id = $request->id;
         $trip->fk_service_type_id = $request->cab_service_id;
         $trip->fare = $request->fare;
         $trip->fk_promo_id = ($request->promo_id !== null) ? $request->promo_id : null; 
         $trip->trip_category = $request->trip_type;
         $trip->trip_distance = $request->trip_distance;
         $trip->trip_date = date('Y-m-d');
         if($request->trip_type == 'schedule_trip'):
         	$trip->trip_date = $request->trip_date;
         	$trip->trip_schedule_time = date("Y-m-d H:i:s", strtotime($request->trip_date.' '.$request->trip_schedule_time));	
         endif;
         $trip->save();
               //  User::where('id',$request->id)->update(['trip_status'=>'0']);

         for ($i=0; $i < 3 ; $i++) { 
          $unique_array = array();
          if ($i == 1) {
           $distance = 2;
           $nearby_drivers = $this->getDriverLocationByLatLong($request->start_lat,
            $request->start_long, $request->cab_service_id ,$distance);
           if (count($nearby_drivers)> 0) {
             foreach ($nearby_drivers as $value) {
              $unique_array[] = $value;
              $msg = $this->FindRideNotiFunction($value,$riderDetails,$request,$trip);

					} // End of for each
				}

                    //print_r(count($unique_array));


       $rider_trip_status = User::where('id', $request->id)->first();
                  //  var_dump($rider_trip_status); die;
       if( (int)$rider_trip_status->on_trip_status !== 0 ){
        $response->status = false;
        $response->code = 403;
        $response->message = 'Ride already assigned.';
        return response()->json($response);
        break;
      }

      if (sizeof($unique_array) > 0) {
        $response->status = true;
        $response->code = 200;
        $response->message = 'Ride requested';
                        $response->data = $msg;// $login_user;
                        return response()->json($response);
                        break;
                      }else{
                       continue;
                     }


                } // it 1 ended

                if ($i == 1) {
                	$distance = 5;
                	$unique_array = array();
                	$nearby_drivers = $this->getDriverLocationByLatLong($request->start_lat,
                		$request->start_long, $request->cab_service_id ,$distance);
                  if (count($nearby_drivers)> 0) {
                   foreach ($nearby_drivers as $value) {
                    $unique_array[] = $value;
                    $msg = $this->FindRideNotiFunction($value,$riderDetails,$request,$trip);

                 } //END OF FOREACH
               }


               $rider_trip_status = User::where('id', $request->id)->first();
               if( (int)$rider_trip_status->on_trip_status !== 0 ){
                $response->status = false;
                $response->code = 403;
                $response->message = 'Ride already assigned.';
                return response()->json($response);
                break;
              }


              if (sizeof($unique_array) > 0) {
                $response->status = true;
                $response->code = 200;
                $response->message = 'Ride requested';
                                                $response->data = $msg;// $login_user;
                                                return response()->json($response);
                                                break;
                                              }else{
                                               continue;
                                             }

                } // it 2 ended

                if ($i == 2) {
                	$distance = 15;
                	$unique_array = array();
                	$nearby_drivers = $this->getDriverLocationByLatLong($request->start_lat,
                		$request->start_long, $request->cab_service_id ,$distance);
                  if (count($nearby_drivers)> 0) {

                   foreach ($nearby_drivers as $value) {
                    $unique_array[] = $value;
                    $msg = $this->FindRideNotiFunction($value,$riderDetails,$request,$trip);

                 }//END OF FOREACH
               }


               $rider_trip_status = User::where('id', $request->id)->first();
               if( (int)$rider_trip_status->on_trip_status !== 0 ){
                $response->status = false;
                $response->code = 403;
                $response->message = 'Ride already assigned.';
                return response()->json($response);
                break;
              }

              if (sizeof($unique_array) > 0) {
                $response->status = true;
                $response->code = 200;
                $response->message = 'Ride requested';
                                                $response->data = $msg;// $login_user;

                                                return response()->json($response);
                                                break;
                                              }else{

                                                $trip->delete();
                                                $response->status = false;
                                                $response->code = 403;
                                                $response->message = 'No drivers found at the moment, please try again later.';
                                                return response()->json($response);
                                              }


                } // it 3 ended

              }


            }


          }

        }




        public function FindRideNotiFunction($value,$riderDetails,$request,$trip)
        {

          $schedule_trip = Trip::where('fk_driver_id', $value->id)
          ->where('trip_category', 'schedule_trip')->where('trip_status', 'accepted')->first();
          $devices_ = Devices::where('user_id', $value->id)->first();
          $noti_type =  ($request->trip_type == 'schedule_trip') ? 'request_scheduled' : 'request';
          if(!$schedule_trip){

           $notiArray= array(
             "reciever_id" =>$value->id,
             "sender_id" => 0,
             "title" => 'Requesting ride.',
             "message" => ucfirst($riderDetails->name)." "."has requested a ride",
             "status" => "sent",
             "is_read" => 1,
             "sent_on" => date('Y-m-d'),
             "sent_at" =>date('h:i:s a'),
           );

           if($devices_->device_type == "android" && $devices_->device_token !== null){

           

             $msg=array(
              'message'=>$notiArray['message'],
              "ride_id"=>$trip->id,
              "rider_details"=>[
               "rider_id"=> $riderDetails->id,
               "email" => $riderDetails->email,
               "name" =>$riderDetails->name,
               "phone" =>$riderDetails->phone_no,
               "picture" =>$riderDetails->image_url ? url('/').'/uploads/passengers/'.$request->id.'/'.$riderDetails->image_url :"",
               "start_lat_long"=>[
                "latitude"=>$request->start_lat,
                "longitude"=>$request->start_long ],
                "end_lat_long"=>[
                 "latitude"=>  $request->end_lat,
                 "longitude"=>$request->end_long ],
                 "driver_lat_long" =>[
                  "latitude"=>$value->latitude,
                  "longitude"=> $value->longitude  ],

                ] ,
                'fare' => $request->fare,
                'pickup_address' => $request->to_address,
                'drop_address' => $request->from_address,
                'type'=>$noti_type, );
             $notifymsg=array(
              "title" =>$notiArray['title'],
              'data'=>$msg,
              'to'  =>$devices_->device_token,
              'sent_on'=>$notiArray['sent_on'],
              'sent_at'=>$notiArray['sent_at'],
              'type'=> $noti_type,
            );
             $this->android_deviceNotification($notifymsg);


           }
           if($devices_->device_type =="ios" && $devices_->device_token !== null){

             $msg=array(
              'message'=>$notiArray['message'],
              "ride_id"=>$trip->id,
              "rider_details"=>[
               "rider_id"=> $riderDetails->id,
               "email" => $riderDetails->email,
               "name" =>$riderDetails->name,
               "phone" =>$riderDetails->phone_no,
               "picture" =>$riderDetails->image_url ? url('/').'/uploads/passengers/'.$request->id.'/'.$riderDetails->image_url :"",
               "start_lat_long"=>[
                "latitude"=>$request->start_lat,
                "longitude"=>$request->start_long ],
                "end_lat_long"=>[
                 "latitude"=>  $request->end_lat,
                 "longitude"=>$request->end_long ],
                 "driver_lat_long" =>[
                  "latitude"=>$value->latitude,
                  "longitude"=> $value->longitude  ],

                ] ,
                'fare' => $request->fare,
                'pickup_address' => $request->to_address,
                'drop_address' => $request->from_address,
                'type'=>$noti_type, );
             $body['aps'] = array(
              'alert' => $msg['message'],
              'sound' => 'default',
              'badge' => 1,
              'content-available' => 1,
              "title" =>$notiArray['title'],
              'data'=>$msg,
              'type'=> $noti_type,

            );
             $this->ios_deviceNotification( $devices_->device_token,$body);


           }

         }
          //IF TRIP IS SCHEDULED
         else{

          $time_now =  date('h:i:s a');
          $today_date = date('Y-m-d');
          $schedule_time = $schedule_trip->trip_schedule_time;
          $hoursToSubtract = 1;
              //Convert those hours into seconds so
              //that we can subtract them from our timestamp.
          $timeToSubtract = ($hoursToSubtract * 60 * 60);
              //Subtract the hours from our Unix timestamp.
          $timeInPast = strtotime($time_now) - $timeToSubtract;

          $check_schedule_date = (strtotime($today_date) ==  $schedule_trip->trip_date) ? true : false;

          if($check_schedule_date == true){

            if(! strtotime($time_now) == $timeInPast):
              
             $notiArray= array(
               "reciever_id" =>$value->id,
               "sender_id" => 0,
               "title" => 'Requesting ride.',
               "message" => ucfirst($riderDetails->name)." "."has requested a ride",
               "status" => "sent",
               "is_read" => 1,
               "sent_on" => date('Y-m-d'),
               "sent_at" =>date('h:i:s a'),
             );

             if($devices_->device_type == "android" && $devices_->device_token !== null){

               $msg=array(
                'message'=>$notiArray['message'],
                "ride_id"=>$trip->id,
                "rider_details"=>[
                 "rider_id"=> $riderDetails->id,
                 "email" => $riderDetails->email,
                 "name" =>$riderDetails->name,
                 "phone" =>$riderDetails->phone_no,
                 "picture" =>$riderDetails->image_url ? url('/').'/uploads/passengers/'.$request->id.'/'.$riderDetails->image_url :"",
                 "start_lat_long"=>[
                  "latitude"=>$request->start_lat,
                  "longitude"=>$request->start_long ],
                  "end_lat_long"=>[
                   "latitude"=>  $request->end_lat,
                   "longitude"=>$request->end_long ],
                   "driver_lat_long" =>[
                    "latitude"=>$value->latitude,
                    "longitude"=> $value->longitude  ],

                  ] ,
                  'fare' => $request->fare,
                  'pickup_address' => $request->to_address,
                  'drop_address' => $request->from_address,
                  'type'=>$noti_type, );
               $notifymsg=array(
                "title" =>$notiArray['title'],
                'data'=>$msg,
                'to'  =>$devices_->device_token,
                'sent_on'=>$notiArray['sent_on'],
                'sent_at'=>$notiArray['sent_at'],
                'type'=>$noti_type,
              );
               $this->android_deviceNotification($notifymsg);


             }
             if($devices_->device_type =="ios" && $devices_->device_token !== null){

               $msg=array(
                'message'=>$notiArray['message'],
                "ride_id"=>$trip->id,
                "rider_details"=>[
                 "rider_id"=> $riderDetails->id,
                 "email" => $riderDetails->email,
                 "name" =>$riderDetails->name,
                 "phone" =>$riderDetails->phone_no,
                 "picture" =>$riderDetails->image_url ? url('/').'/uploads/passengers/'.$request->id.'/'.$riderDetails->image_url :"",
                 "start_lat_long"=>[
                  "latitude"=>$request->start_lat,
                  "longitude"=>$request->start_long ],
                  "end_lat_long"=>[
                   "latitude"=>  $request->end_lat,
                   "longitude"=>$request->end_long ],
                   "driver_lat_long" =>[
                    "latitude"=>$value->latitude,
                    "longitude"=> $value->longitude  ],

                  ] ,
                  'fare' => $request->fare,
                  'pickup_address' => $request->to_address,
                  'drop_address' => $request->from_address,
                  'type'=>$noti_type, );
               $body['aps'] = array(
                'alert' => $msg['message'],
                'sound' => 'default',
                'badge' => 1,
                'content-available' => 1,
                "title" =>$notiArray['title'],
                'data'=>$msg,
                'type'=>$noti_type,

              );
               $this->ios_deviceNotification( $devices_->device_token,$body);


             }

           endif;
         }

         


          } //END OF ELSE
          

          
          return $msg;


        }




//End of find ride


/*
*Api to get estimate of ride wrt service
*/

public function getRideEstimate(Request $request)
{
       // $price_per_km = 1.70;
	$response = new \stdClass();
	$validator = Validator::make($request->all(), [
		'start_lat' => 'required',
		'start_long' => 'required',
		'end_lat' => 'required',
		'end_long' => 'required',
		'id' => 'required'
	]);

	if($validator->fails()){
		$response->status = false;
		$response->code = 400;
		$response->message = 'validation failed';
		$response->data = $validator->errors();
		return response()->json($response);
	} else {
    $promo_apply = (isset($request->promo_code)) ? $request->promo_code : 0;
    $distance = $this->getDrivingDistance($request->start_lat ,  $request->end_lat , $request->start_long , $request->end_long);


    if($promo_apply !== 0):
        //CHECK IF COUPON EXIST
      $check_promo = Promo::where('promo_code',$request->promo_code)->first();
      if(!$check_promo):
        $response->status = false;
        $response->code = 402;
        $response->message = 'Invalid promo code';
        return response()->json($response);

      else:
        if(strtotime($check_promo->expiry_date) > strtotime(date('Y-m-d'))):

              //CHECK IF ALREADY BEEN USED

          $check_usage_counter = Trip::where('fk_promo_id' ,$check_promo->id)->get();


        if(count($check_usage_counter) == $check_promo->usage_count):

          $response->status = false;
          $response->code = 402;
          $response->message = 'This promo code is already been used upto max usage.';
          return response()->json($response);

        else:
          //dd($check_promo->discount_type);

          if(strtolower($check_promo->discount_type) == 'flat'):

            $discount_amt = (int)$check_promo->discount_value;

            if (sizeof($distance) > 0) {
              $cab_services = CabService::orderBy("name")->get();
              foreach ($cab_services as $key => $value ) {
                $calculated_fare = ( (int)$value->price_per_kilometer * (int)$distance['distance'] + 6);
                if($calculated_fare > $discount_amt):
                 $cab_services[$key]->estimated_fare = $calculated_fare - $discount_amt;
                 $cab_services[$key]->amount_issue = false;
                 $cab_services[$key]->promo_id = $check_promo->id;
               else:
                 $cab_services[$key]->estimated_fare = $calculated_fare;
                 $cab_services[$key]->amount_issue = true;
                 $cab_services[$key]->promo_id = $check_promo->id;

               endif;

               
               $cab_services[$key]->distance = $distance['distance'];

             }


             $response->status = true;
             $response->code = 200;
             $response->message = 'Fare estimation fetched successfully';
             $response->data = $cab_services;
             return response()->json($response);

           }else{
             $response->status = false;
             $response->code = 401;
             $response->message = 'google api response error';
             return response()->json($response);

           }




         elseif(strtolower($check_promo->discount_type) == 'percentage'):  

          if (sizeof($distance) > 0) {
            $cab_services = CabService::orderBy("name")->get();
            foreach ($cab_services as $key => $value ) {
              $calculated_fare = ( (int)$value->price_per_kilometer * (int)$distance['distance'] + 6);
              $discount_amt = ($check_promo->discount_value / 100) * $calculated_fare;
              if($calculated_fare > $discount_amt):
               $cab_services[$key]->estimated_fare = round($calculated_fare - $discount_amt);
               $cab_services[$key]->amount_issue = false;
               $cab_services[$key]->promo_id = $check_promo->id;
             else:
               $cab_services[$key]->estimated_fare = round($calculated_fare);
               $cab_services[$key]->amount_issue = true;
               $cab_services[$key]->promo_id = $check_promo->id;

             endif;


             $cab_services[$key]->distance = $distance['distance'];

           }


           $response->status = true;
           $response->code = 200;
           $response->message = 'Fare estimation fetched successfully';
           $response->data = $cab_services;
           return response()->json($response);

         }else{
           $response->status = false;
           $response->code = 401;
           $response->message = 'google api response error';
           return response()->json($response);

         }


       endif;



     endif;


   else:
     $response->status = false;
     $response->code = 402;
     $response->message = 'This promo code is expired';
     return response()->json($response);

   endif;
 endif;

else:


 if (sizeof($distance) > 0) {
   $cab_services = CabService::orderBy("name")->get();

   foreach ($cab_services as $key => $value ) {
    $cab_services[$key]->estimated_fare = ( (int)$value->price_per_kilometer * (int)$distance['distance'] + 6 );
    $cab_services[$key]->amount_issue = false;
    $cab_services[$key]->promo_id = "";
    $cab_services[$key]->distance = $distance['distance'];

  }

  $response->status = true;
  $response->code = 200;
  $response->message = 'Fare estimation fetched successfully';
  $response->data = $cab_services;
  return response()->json($response);
}else{
 $response->status = false;
 $response->code = 401;
 $response->message = 'google api response error';
 return response()->json($response);

}


endif;






}

} //End of ride estimation


/*
*Api to get trip history
*id(rider id)
*/

public function riderTripsHistory(Request $request)
{
  $response = new \stdClass();
  $validator = Validator::make($request->all(), [
    'id' => 'required'
  ]);

  if($validator->fails()){
    $response->status = false;
    $response->code = 400;
    $response->message = 'validation failed';
    $response->data = $validator->errors();
    return response()->json($response);
  }
  else 
  {
    $response_data = [];
    $details = Trip::where('trips.fk_rider_id' ,$request->id)
    ->where('trip_status','=','completed')
    ->orWhere('trip_status','=','cancelled')
    ->leftJoin('users', 'trips.fk_driver_id' ,'=' ,'users.id')
    ->leftJoin('cab_services' , 'cab_services.id'  ,'=', 'users.fk_cab_service_id')
    ->leftJoin('car_models' ,'users.car_model_id' ,'=', 'car_models.id')
    ->select('*', 'trips.id as ride_id', 'users.id as driver_id','cab_services.name as cab_service_name' ,'users.name as driver_name')
    ->orderBy('trips.id', 'DESC')->get();
   // dd($details);

    foreach ($details as $key => $value) {
      $response_data [] =  array(

        "trip_details" => [

          'ride_id' => $value->ride_id,
          "booking_no" => $value->booking_no,
          "destination_from" => $value->destination_from,
          "destination_to" => $value->destination_to,
          "destination_from_lat" => $value->destination_from_lat,
          "destination_from_lng" => $value->ridedestination_from_lng_id,
          "destination_to_lat" => $value->destination_to_lat,
          "destination_to_lng" => $value->destination_to_lng,
          "trip_date" => $value->trip_date,
          "trip_started_at" =>  $value->trip_started_at,
          "trip_ended_at" =>  $value->trip_ended_at,
          "fare" =>  $value->fare,
          "trip_status" =>  $value->trip_status,
          "trip_category" =>  $value->trip_category,

          "trip_rating" => [
            "rating" => "4",
            "review" => "Nice ride, value for money."
          ]

        ],

        "driver_details"=> [
          "id"=> $value->driver_id,
          "name"=> $value->driver_name,
          "email"=> $value->email,
          "phone"=> $value->phone_no,
          "picture"=> ($value->image_url !== null) ?  url('/').'/uploads/drivers/'.$value->id.'/'.$value->image_url : "",
          "rating" => "4",

          "vehicle_details"=> [
            "vehicle_no" =>($value->car_number !== null)? $value->car_number:"",
            "vehicle_model" => ($value->car_model_name !== null)? $value->car_model_name:"",
            "rider_space" => $value->rider_space,
            "cab_service" => $value->name,
            "car_img" => "https://pp.netclipart.com/pp/s/6-63856_car-clip-art-png-car-clipart-png.png"

          ] 

        ],

      );
    }

    if(sizeof($response_data) > 0){
     $response->status = true;
     $response->code = 200;
     $response->message = 'Trips history feteched successfully';
     $response->data = $response_data;
     return response()->json($response);
   }

   else{
     $response->status = true;
     $response->code = 401;
     $response->message = 'No trip history found';
     $response->data = $response_data;
     return response()->json($response);
   }

 }
}



/*
*API to get list of promos
*/


public function getPromos(Request $request)
{
  $response = new \stdClass();
  $response_data = [];
  $pluck_codes = Promo::where('expiry_date', '>' ,date('Y-m-d'))
  ->get(['id', 'usage_count']);

  foreach ($pluck_codes as  $value) {
   $check_usage_counter = Trip::where('fk_promo_id' ,$value->id)->get();

   if($check_usage_counter->count() !== $value->usage_count):
     $response_data[] = Promo::where('id', $value->id)->first();
   endif;
 }

 if (sizeof($response_data) > 0 ):
   $response->code = 200;
   $response->message = 'Promos fetched successfully';
   $response->data = $response_data;
   return response()->json($response);

 else:
  $response->code = 400;
  $response->message = 'No promos found';
  $response->data = $response_data;
  return response()->json($response);
endif;

}



/*Function to check user cards count on stripe*/

function CheckCustomerCards($id)
{
        $user = User::where('id', $id)->first();

      if($user->stripe_customer_id == NULL):
         return 0;

      else:
       
        $stripe = new \Stripe\StripeClient($this->stripe_secret);
        $card_details = $stripe->customers->allSources($user->stripe_customer_id,['object' => 'card']);

       // print_r($card_details); die;
        if(sizeof($card_details->data) == 0):

           return 0;

        else:

          return 1;

        endif;

      endif;

}



/*API to cancel ride by rider*/


public function CancelRideByRider(Request  $request)
{
   $response = new \stdClass();
    $validator = Validator::make($request->all(), [
      'ride_id' => 'required',
      'id' => 'required'
    ]);

  if($validator->fails()):
    $response->status = false;
    $response->code = 400;
    $response->message = 'validation failed';
    $response->data = $validator->errors();
    return response()->json($response);

    else:
      $check_trip_status = Trip::where('id', $request->ride_id)->first();
      $devices_ = Devices::where('user_id', $check_trip_status->fk_rider_id)->first();
      $riderDetails = User::where('id',  $request->id)->first();

      if ($check_trip_status->trip_status == 'accepted'):

              $check_trip_status->trip_status = 'cancelled';
              $check_trip_status->cancelled_by = 1;
              $check_trip_status->save();


              $notiArray= array(
              "reciever_id" =>$check_trip_status->fk_driver_id,
              "sender_id" => 0,
              "title" => 'Request cancelled.',
              "message" => "Your ride has been cancelled by rider"." ".$riderDetails->name." "." please look another ride.",
              "status" => "sent",
              "sent_on" => date('Y-m-d'),
              "sent_at" =>date('h:i:s a'),
            );


            $msg = array('message'=>$notiArray['message'],
              "start_lat_long" =>[
                "start_latitude" =>$check_trip_status->destination_from_lat,
                "start_longitude"=>$check_trip_status->destination_from_lng] ,
                "end_lat_long"=>[
                  "latitude"=>  $check_trip_status->destination_to_lat,
                  "longitude"=>$check_trip_status->destination_to_lng ],
                  "rider_details"=> [
                    "id"=> $riderDetails->id,
                    "name"=> $riderDetails->name,
                    "email"=> $riderDetails->email,
                    "phone"=> $riderDetails->phone_no,
                    "picture"=>($riderDetails->image_url !== NULL ) ?  url('/').'/uploads/passengers/'.$riderDetails->id.'/'.$riderDetails->image_url : "https://cdn-makehimyours.pressidium.com/wp-content/uploads/2016/11/Depositphotos_9830876_l-2015Optimised.jpg",
                                "rating" =>"4",

                             //    "vehicle_details"=> [
                             //     "vehicle_no" =>($driverDetails->car_number !== null)? $driverDetails->car_number:"",
                             //     "vehicle_model" => ($driverDetails->car_model_name !== null)? $driverDetails->car_model_name:"",
                             //     "latitude"=>($driverDetails->latitude !== null)?  $driverDetails->latitude:"",
                             //     "longitude"=>($driverDetails->longitude !== null)? $driverDetails->longitude:"",
                             //     "rider_space" => $cab_service->rider_space,
                             //     "cab_service" => $cab_service->name,
                             //     "car_img" => "https://pp.netclipart.com/pp/s/6-63856_car-clip-art-png-car-clipart-png.png"
                             // ] 
                         ],
                         "fare" => $check_trip_status->fare,
                         'pickup_address' => $check_trip_status->destination_from,
                         'drop_address' => $check_trip_status->destination_to,
                         "status"=>"Sent success",
                         'type'=>'cancelled',
                     );

              //notification block

              if($devices_->device_type == "android" && $devices_->device_token !== null):

                $notifymsg=array(
                  "title" =>$notiArray['title'],
                  'data'=>$msg,
                  'to'  =>$devices_->device_token,
                  'sent_on'=>$notiArray['sent_on'],
                  'sent_at'=>$notiArray['sent_at'],
                  'type'=>'cancelled',
                );

                $this->android_deviceNotification($notifymsg);

                //  android noti

                  //Ios noti
                  elseif($devices_->device_type =="ios" && $devices_->device_token !== null) :

                     $body['aps'] = array(
                      'alert' => $notiArray['message'],
                      'sound' => 'default',
                      'badge' => 1,
                      'content-available' => 1,
                      "title" =>$notiArray['title'],
                      'data'=>$msg,
                      'type'=>'cancelled',
                    );
                    $this->ios_deviceNotification($devices_->device_token,$body);
                    
                            

            endif; 



              //End of notification block

             $response->status = true;
                            $response->code = 200;
                            $response->message = 'Ride cancel';
                            $response->data = $check_trip_status;
                            return response()->json($response);

      else:

            $response->status = false;
            $response->code = 401;
            $response->message = 'This ride is un-available for cancel';
            $response->data = $check_trip_status;
            return response()->json($response);

      endif;


  endif;
  
}



} //End of controller
