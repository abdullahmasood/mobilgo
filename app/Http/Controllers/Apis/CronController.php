<?php
namespace App\Http\Controllers\Apis;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use File;
use Auth;
use App\User;
use App\Company;
use App\Trip;
use App\CabService;
use App\Notification;
use App\Devices;
use App\CarModels;
use \Illuminate\Support\Facades\Validator;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use  App\Http\Controllers\Apis\BaseController as apiBaseController;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Credentials: true');

class CronController extends apiBaseController
{


public function RideReminderCron(Request $request)
{

		$response = new \stdClass();
		$scheduled_trips = Trip::where('trip_category','schedule_trip')
		->where('trip_status', 'accepted')
		->get();

		if(sizeof($scheduled_trips) > 0 ):
			$today_date_time = date('Y-m-d H:i:s');
			//dd($scheduled_trips);

			foreach ($scheduled_trips as $key => $value):
					
					// 30 mins earlier time
				 $earlier_time = date('Y-m-d H:i:s',strtotime("-30 minutes",strtotime($value->trip_schedule_time)));
				 $devices_ = Devices::where('user_id', $value->fk_rider_id)->where('user_id',$value->fk_driver_id)->get();
				  $riderDetails = User::where('id', $value->fk_rider_id)->first();
				  $driverDetails = User::where('users.id',  $value->fk_driver_id)
									->leftJoin('car_models', 'users.car_model_id' , '=' , 'car_models.id')
									->first();
				  $cab_service = CabService::where('id', $driverDetails->fk_cab_service_id)->first();
					$checkRide = Trip::where('id', $value->id)->first();

				
				if(strtotime($today_date_time) == strtotime($earlier_time)):

					 $notiArray= array(
			             "reciever_id" =>$devices_->user_id,
			             "sender_id" => 0,
			             "title" => 'Scheduled Ride reminder ride.',
			             "message" => "You have a ride scheduled at ".date('H:i:s a', strtotime($value->trip_schedule_time)),
			             "status" => "sent",
			             "is_read" => 1,
			             "sent_on" => date('Y-m-d'),
			             "sent_at" =>date('h:i:s a'),
			           );

					//Message to payload
						$msg = array('message'=>$notiArray['message'],
					"start_lat_long" =>[
					"start_latitude" =>$checkRide->destination_from_lat,
					"start_longitude"=>$checkRide->destination_from_lng] ,
					"end_lat_long"=>[
						"latitude"=>  $checkRide->destination_to_lat,
						"longitude"=>$checkRide->destination_to_lng ],
						"driver_details"=> [
							"id"=> $driverDetails->id,
							"name"=> $driverDetails->name,
							"email"=> $driverDetails->email,
							"phone"=> $driverDetails->phone_no,
							"picture"=> ($driverDetails->image_url !== NULL)?  url('/').'/uploads/drivers/'.$driverDetails->id.'/'.$driverDetails->image_url : "https://cdn-makehimyours.pressidium.com/wp-content/uploads/2016/11/Depositphotos_9830876_l-2015Optimised.jpg",
							"rating" => "4",

							"vehicle_details"=> [
								"vehicle_no" =>($driverDetails->car_number !== null)? $driverDetails->car_number:"",
								"vehicle_model" => ($driverDetails->car_model_name !== null)? $driverDetails->car_model_name:"",
								"latitude"=>($driverDetails->latitude !== null)?  $driverDetails->latitude:"",
								"longitude"=>($driverDetails->longitude !== null)? $driverDetails->longitude:"",
								"rider_space" => $cab_service->rider_space,
								"cab_service" => $cab_service->name,
								"car_img" => "https://pp.netclipart.com/pp/s/6-63856_car-clip-art-png-car-clipart-png.png"

							] 
						],

						 "rider_details"=>[
				               "rider_id"=> $riderDetails->id,
				               "email" => $riderDetails->email,
				               "name" =>$riderDetails->name,
				               "phone" =>$riderDetails->phone_no,
				               "picture" =>$riderDetails->image_url ? url('/').'/uploads/passengers/'.$riderDetails->id.'/'.$riderDetails->image_url :"",

                			] ,
						"fare" => $checkRide->fare,
						'pickup_address' => $checkRide->destination_from,
						'drop_address' => $checkRide->destination_to,
						"status"=>"Sent success",
						'type'=>'cron_reminder',
						

					);

					//End payload message
						if($devices_->device_type == "android" && $devices_->device_token !== null):

							    $notifymsg=array(
					              "title" =>$notiArray['title'],
					              'data'=>$msg,
					              'to'  =>$devices_->device_token,
					              'sent_on'=>$notiArray['sent_on'],
					              'sent_at'=>$notiArray['sent_at'],
					             'type'=> 'cron_reminder',
					            );
					             $this->android_deviceNotification($notifymsg);

						 elseif($devices_->device_type =="ios" && $devices_->device_token !== null):

						 	$body['aps'] = array(
					              'alert' => $msg['message'],
					              'sound' => 'default',
					              'badge' => 1,
					              'content-available' => 1,
					              "title" =>$notiArray['title'],
					              'data'=>$msg,
					              'type'=> 'cron_reminder',

					            );
             				$this->ios_deviceNotification( $devices_->device_token,$body);

						 endif;

				endif;



			endforeach;
			
		endif;
}


}//End of controller