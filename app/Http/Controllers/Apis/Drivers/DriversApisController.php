<?php
namespace App\Http\Controllers\Apis\Drivers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use File;
use Auth;
use App\User;
use App\Company;
use App\Trip;
use App\CabService;
use App\Notification;
use App\Devices;
use App\CarModels;
use \Illuminate\Support\Facades\Validator;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use  App\Http\Controllers\Apis\BaseController as apiBaseController;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Credentials: true');

class DriversApisController extends apiBaseController
{


	public function driverLogin(Request $request){
		$response = new \stdClass();
		$validator = Validator::make($request->all(), [
			'email' => 'required',
			'password' => 'required',
			'device_type' => 'required',
			'device_token' => 'required'
		]);

		if($validator->fails()){
			$response->status = false;
			$response->code = 400;
			$response->message = 'validation failed';
			$response->data = $validator->errors();
			return response()->json($response);
		} else {
			$user = User::where('email', $request->email)->where('user_type', 'driver')->first();
			if($user){

                //  $device =  Devices::where('user_id', $user->id)->where([
                //     ['device_type', '=', $request->device_type],
                //     ['device_token', '=', $request->device_token]

                // ])->first();
				if (Hash::check($request->password, $user->password)) {
					$user->remember_token = md5(time());
					$user->latitude = $request->lat;
					$user->longitude = $request->long;
					$user->save();
					$login_user = new \stdClass();
					$login_user->id = $user->id;
					$login_user->name = $user->name;
					$login_user->email = $user->email;
					$login_user->_token = $user->remember_token;
                   // $login_user->trips = $user->customer_trips;
					$user->password = $request->password;
					$user_device = new Devices();
					$user_device->user_id = $user->id;
					$user_device->device_type = $request->device_type;
					$user_device->device_token = $request->device_token;
					$user_device->save();

					$response->status = true;
					$response->code = 200;
					$response->message = 'login successful';
                    $response->data = $user;// $login_user;
                    return response()->json($response);

                } 
                else {

                	$response->status = true;
                	$response->code = 401;
                	$response->message = 'password does not match';
                	$response->data = [];
                	return response()->json($response);
                }
            }
            else{
            	$response->status = false;
            	$response->code = 404;
            	$response->message = 'Rider not found';
            	$response->data = [];
            	return response()->json($response);
            }
        }
    }

    public function actionRideRequest(Request $request){
    	$response = new \stdClass();
    	$validator = Validator::make($request->all(), [
    		'ride_id' => 'required',
    		'id' => 'required',
    		'request_type' => 'required',
    		//'current_device' => 'required'

    	]);

    	if($validator->fails()){
    		$response->status = false;
    		$response->code = 400;
    		$response->message = 'validation failed';
    		$response->data = $validator->errors();
    		return response()->json($response);
    	} else {
    		$check_trip_status = Trip::where('id', $request->ride_id)->first();
    		$devices_ = Devices::where('user_id', $check_trip_status->fk_rider_id)->first();
    		$driverDetails = User::where('users.id',  $request->id)
    		->leftJoin('car_models', 'users.car_model_id' , '=' , 'car_models.id')
    		->first();
    		$cab_service = CabService::where('id', $driverDetails->fk_cab_service_id)->first();



    		$ride_details = array(
    			"vehicle" => [
    				"vehicle_no" =>$driverDetails->car_number,
    				"vehicle_model" => $driverDetails->car_model_name,
    				"service" => $cab_service->name,
    			],
    			"driver" => [
    				"id"=> $driverDetails->id,
    				"driver_name" =>$driverDetails->name,
    				"driver_rating" =>"",
    				//"driver_gender" =>$driverDetails->v_gender,
    				//"total_rides" =>$completedRides,
    			],
    			"location" => [
    				"driver_lat_long" =>[
    					$driverDetails->latitude,
    					$driverDetails->longitude
    				],
    				"end_lat_long" =>[
    					$check_trip_status->destination_from_lat,
    					$check_trip_status->destination_from_lng
    				],
    				
    			],
    			"fare" => $check_trip_status->fare,
    			"trip_status" => $check_trip_status->trip_status,
    			"ride_id" =>$check_trip_status->id
    		);



    		if ($request->request_type == 'accept') {

            $noti_type =  ($check_trip_status->trip_category == 'schedule_trip') ? 'scheduled_ride_accepted' : 'accepted';

    			$notiArray= array(
    				"reciever_id" =>$check_trip_status->fk_rider_id,
    				"sender_id" => 0,
    				"title" => 'Request accepted.',
    				"message" => "Your ride has been confirmed by"." ".$driverDetails->name." "."will be at your loction soon.",
    				"status" => "sent",
    				"sent_on" => date('Y-m-d'),
    				"sent_at" =>date('h:i:s a'),
    			);


    			$msg = array('message'=>$notiArray['message'],
    				"start_lat_long" =>[
    					"start_latitude" =>$check_trip_status->destination_from_lat,
    					"start_longitude"=>$check_trip_status->destination_from_lng] ,
    					"end_lat_long"=>[
    						"latitude"=>  $check_trip_status->destination_to_lat,
    						"longitude"=>$check_trip_status->destination_to_lng ],
    						"driver_details"=> [
    							"id"=> $driverDetails->id,
    							"name"=> $driverDetails->name,
    							"email"=> $driverDetails->email,
    							"phone"=> $driverDetails->phone_no,
    							"picture"=>($driverDetails->image_url !== NULL) ?  url('/').'/uploads/drivers/'.$driverDetails->id.'/'.$driverDetails->image_url : "https://cdn-makehimyours.pressidium.com/wp-content/uploads/2016/11/Depositphotos_9830876_l-2015Optimised.jpg",
                                "rating" => "4",

                                "vehicle_details"=> [
                                    "vehicle_no" =>($driverDetails->car_number !== null)? $driverDetails->car_number:"",
                                    "vehicle_model" => ($driverDetails->car_model_name !== null)? $driverDetails->car_model_name:"",
                                    "latitude"=>($driverDetails->latitude !== null)?  $driverDetails->latitude:"",
                                    "longitude"=>($driverDetails->longitude !== null)? $driverDetails->longitude:"",
                                    "rider_space" => $cab_service->rider_space,
                                    "cab_service" => $cab_service->name,
                                    "car_img" => "https://pp.netclipart.com/pp/s/6-63856_car-clip-art-png-car-clipart-png.png"
                                ] 
                            ],
                            "fare" => $check_trip_status->fare,
                            'pickup_address' => $check_trip_status->destination_from,
                            'drop_address' => $check_trip_status->destination_to,
                            "status"=>"Sent success",
                            'type'=>$noti_type,
                        );
    			

    			if ($check_trip_status->trip_status !== 'requested') {
    				$response->status = false;
    				$response->code = 401;
    				$response->message = 'This ride is un-available or already has been assigned to someone else.';
    				return response()->json($response);
    			}
    			else {
    				$check_trip_status->trip_status = 'accepted';
    				$check_trip_status->fk_driver_id = $request->id;
    				//$check_trip_status->trip_date = date('Y-m-d');
    				$check_trip_status->save();

    				if($devices_->device_type == "android" && $devices_->device_token !== null){

    					

    					$notifymsg=array(
    						"title" =>$notiArray['title'],
    						'data'=>$msg,
    						'to'  =>$devices_->device_token,
    						'sent_on'=>$notiArray['sent_on'],
    						'sent_at'=>$notiArray['sent_at'],
    						'type'=>$noti_type,
    					);

    					$this->android_deviceNotification($notifymsg);


    				} //  android noti
    				elseif($devices_->device_type =="ios" && $devices_->device_token !== null){


    					$body['aps'] = array(
    						'alert' => $notiArray['message'],
    						'sound' => 'default',
    						'badge' => 1,
    						'content-available' => 1,
    						"title" =>$notiArray['title'],
    						'data'=>$msg,
    						'type'=>$noti_type,
    					);
    					$this->ios_deviceNotification($devices_->device_token,$body);


    				} // ios noti
    				$response->status = true;
    				$response->code = 200;
    				$response->message = 'Ride accepted';
    				$response->data = $ride_details;
    				return response()->json($response);

    			}

        	} // accept


        	if ($request->request_type == 'cancel') {
        		$notiArray= array(
        			"reciever_id" =>$check_trip_status->fk_rider_id,
        			"sender_id" => 0,
        			"title" => 'Request cancelled.',
        			"message" => "Your ride has been cancelled by"." ".$driverDetails->name." "." please book another ride.",
        			"status" => "sent",
        			"sent_on" => date('Y-m-d'),
        			"sent_at" =>date('h:i:s a'),
        		);


        		$msg = array('message'=>$notiArray['message'],
        			"start_lat_long" =>[
        				"start_latitude" =>$check_trip_status->destination_from_lat,
        				"start_longitude"=>$check_trip_status->destination_from_lng] ,
        				"end_lat_long"=>[
        					"latitude"=>  $check_trip_status->destination_to_lat,
        					"longitude"=>$check_trip_status->destination_to_lng ],
        					"driver_details"=> [
        						"id"=> $driverDetails->id,
        						"name"=> $driverDetails->name,
        						"email"=> $driverDetails->email,
        						"phone"=> $driverDetails->phone_no,
        						"picture"=>($driverDetails->image_url !== NULL ) ?  url('/').'/uploads/drivers/'.$driverDetails->id.'/'.$driverDetails->image_url : "https://cdn-makehimyours.pressidium.com/wp-content/uploads/2016/11/Depositphotos_9830876_l-2015Optimised.jpg",
                                "rating" =>"4",

                                "vehicle_details"=> [
                                 "vehicle_no" =>($driverDetails->car_number !== null)? $driverDetails->car_number:"",
                                 "vehicle_model" => ($driverDetails->car_model_name !== null)? $driverDetails->car_model_name:"",
                                 "latitude"=>($driverDetails->latitude !== null)?  $driverDetails->latitude:"",
                                 "longitude"=>($driverDetails->longitude !== null)? $driverDetails->longitude:"",
                                 "rider_space" => $cab_service->rider_space,
                                 "cab_service" => $cab_service->name,
                                 "car_img" => "https://pp.netclipart.com/pp/s/6-63856_car-clip-art-png-car-clipart-png.png"
                             ] 
                         ],
                         "fare" => $check_trip_status->fare,
                         'pickup_address' => $check_trip_status->destination_from,
                         'drop_address' => $check_trip_status->destination_to,
                         "status"=>"Sent success",
                         'type'=>'cancelled',
                     );

        		if ($check_trip_status->trip_status == 'accepted') {
        			$check_trip_status->trip_status = 'cancelled';
        			$check_trip_status->cancelled_by = 2;
        			$check_trip_status->save();
        			if($devices_->device_type == "android" && $devices_->device_token !== null){

        				$notifymsg=array(
        					"title" =>$notiArray['title'],
        					'data'=>$msg,
        					'to'  =>$devices_->device_token,
        					'sent_on'=>$notiArray['sent_on'],
        					'sent_at'=>$notiArray['sent_at'],
        					'type'=>'cancelled',
        				);

        				$this->android_deviceNotification($notifymsg);


    				} //  android noti
    				elseif($devices_->device_type =="ios" && $devices_->device_token !== null){


    					$body['aps'] = array(
    						'alert' => $notiArray['message'],
    						'sound' => 'default',
    						'badge' => 1,
    						'content-available' => 1,
    						"title" =>$notiArray['title'],
    						'data'=>$msg,
    						'type'=>'cancelled',
    					);
    					$this->ios_deviceNotification($devices_->device_token,$body);


    				} // ios noti

    				$response->status = true;
    				$response->code = 200;
    				$response->message = 'Ride cancel';
    				$response->data = $ride_details;
    				return response()->json($response);
    			}

    			else{
    				$response->status = false;
    				$response->code = 401;
    				$response->message = 'This ride is un-available for cancel';
    				$response->data = $ride_details;
    				return response()->json($response);

    			}
    		}




        } //end of else part
    } // end of request action api


/*
*Api to start ride
*@ride_id
*/

public function startRide(Request $request)
{
	$response = new \stdClass();
	$validator = Validator::make($request->all(), [
		'ride_id' => 'required',
	]);

	if($validator->fails()){
		$response->status = false;
		$response->code = 400;
		$response->message = 'validation failed';
		$response->data = $validator->errors();
		return response()->json($response);
	} else {
			//check if ride exist and accepted
		$matchThese = ['id' => $request->ride_id, 'trip_status' => 'accepted'];
		$checkRide = Trip::where($matchThese)->first();

		if (!$checkRide) {
			$response->status = false;
			$response->code = 401;
			$response->message = 'Ride id does not exit or trip status is not accepted.';
			return response()->json($response);
		}else{
			$checkRide->trip_status = 'inprogress';
			$checkRide->trip_started_at = now();
			$checkRide->save();
			$devices_ = Devices::where('user_id', $checkRide->fk_rider_id)->first();
			$driverDetails = User::where('users.id',  $checkRide->fk_driver_id)
			->leftJoin('car_models', 'users.car_model_id' , '=' , 'car_models.id')
			->first();
			$cab_service = CabService::where('id', $driverDetails->fk_cab_service_id)->first();

			$notiArray= array(
				"reciever_id" =>$checkRide->fk_rider_id,
				"sender_id" => 0,
				"title" => 'Ride started.',
				"message" => "Your ride has started",
				"status" => "sent",
				"sent_on" => date('Y-m-d'),
				"sent_at" =>date('h:i:s a'),
			);

				//noti array

			$msg = array('message'=>$notiArray['message'],
				"start_lat_long" =>[
					"start_latitude" =>$checkRide->destination_from_lat,
					"start_longitude"=>$checkRide->destination_from_lng] ,
					"end_lat_long"=>[
						"latitude"=>  $checkRide->destination_to_lat,
						"longitude"=>$checkRide->destination_to_lng ],
						"driver_details"=> [
							"id"=> $driverDetails->id,
							"name"=> $driverDetails->name,
							"email"=> $driverDetails->email,
							"phone"=> $driverDetails->phone_no,
							"picture"=> ($driverDetails->image_url !== NULL)?  url('/').'/uploads/drivers/'.$driverDetails->id.'/'.$driverDetails->image_url : "https://cdn-makehimyours.pressidium.com/wp-content/uploads/2016/11/Depositphotos_9830876_l-2015Optimised.jpg",
							"rating" => "4",

							"vehicle_details"=> [
								"vehicle_no" =>($driverDetails->car_number !== null)? $driverDetails->car_number:"",
								"vehicle_model" => ($driverDetails->car_model_name !== null)? $driverDetails->car_model_name:"",
								"latitude"=>($driverDetails->latitude !== null)?  $driverDetails->latitude:"",
								"longitude"=>($driverDetails->longitude !== null)? $driverDetails->longitude:"",
								"rider_space" => $cab_service->rider_space,
								"cab_service" => $cab_service->name,
								"car_img" => "https://pp.netclipart.com/pp/s/6-63856_car-clip-art-png-car-clipart-png.png"

							] 
						],
						"fare" => $checkRide->fare,
						'pickup_address' => $checkRide->destination_from,
						'drop_address' => $checkRide->destination_to,
						"status"=>"Sent success",
						'type'=>'started',
						

					);


			if($devices_->device_type == "android" && $devices_->device_token !== null){

				$notifymsg=array(
					"title" =>$notiArray['title'],
					'data'=>$msg,
					'to'  =>$devices_->device_token,
					'sent_on'=>$notiArray['sent_on'],
					'sent_at'=>$notiArray['sent_at'],
					'type'=>'started',
				);

				$this->android_deviceNotification($notifymsg);


    				} //  android noti
    				elseif($devices_->device_type =="ios" && $devices_->device_token !== null){


    					$body['aps'] = array(
    						'alert' => $notiArray['message'],
    						'sound' => 'default',
    						'badge' => 1,
    						'content-available' => 1,
    						"title" =>$notiArray['title'],
    						'data'=>$msg,
    						'type'=>'started',
    					);
    					$this->ios_deviceNotification($devices_->device_token,$body);


    				} // ios noti


    				$ride_details = array(
    					"vehicle" => [
    						"vehicle_no" =>$driverDetails->car_number,
    						"vehicle_model" => $driverDetails->car_model_name,
    						"service" => $cab_service->name,
    						"rider_space" => $cab_service->rider_space,
    					],
    					"driver" => [
    						"id"=> $driverDetails->id,
    						"driver_name" =>$driverDetails->name,
    						"driver_rating" =>"",
    				//"driver_gender" =>$driverDetails->v_gender,
    				//"total_rides" =>$completedRides,
    					],
    					"location" => [
    						"driver_lat_long" =>[
    							$driverDetails->latitude,
    							$driverDetails->longitude
    						],
    						"start_lat_long" =>[
    							$checkRide->destination_from_lat,
    							$checkRide->destination_from_lng
    						],
    						"end_lat_long" =>[
    							$checkRide->destination_to_lat,
    							$checkRide->destination_to_lng
    						],

    					],

    					"fare" => $checkRide->fare,
    					"trip_status" => $checkRide->trip_status,
    					"ride_id" =>$checkRide->id,
    					"rating" => "4",

                        "notification_data" => $msg
    				);

    				$response->status = true;
    				$response->code = 200;
    				$response->message = 'Your ride has been started.';
    				$response->data = $ride_details;
    				return response()->json($response);

    			}

    		}
} // end of start ride api






/*
*Api to complete ride
*@ride_id
*/

public function completeRide(Request $request)
{
	$response = new \stdClass();
	$validator = Validator::make($request->all(), [
		'ride_id' => 'required',
	]);

	if($validator->fails()){
		$response->status = false;
		$response->code = 400;
		$response->message = 'validation failed';
		$response->data = $validator->errors();
		return response()->json($response);
	} else {
			//check if ride exist and accepted
		$matchThese = ['id' => $request->ride_id, 'trip_status' => 'inprogress'];
		$checkRide = Trip::where($matchThese)->first();

		if (!$checkRide) {
			$response->status = false;
			$response->code = 401;
			$response->message = 'Ride id does not exit or trip status is not inprogress.';
			return response()->json($response);
		}else{
			$checkRide->trip_status = 'completed';
			$checkRide->trip_ended_at = now();
			$checkRide->save();
			$devices_ = Devices::where('user_id', $checkRide->fk_rider_id)->first();
			$driverDetails = User::where('users.id',  $checkRide->fk_driver_id)
			->leftJoin('car_models', 'users.car_model_id' , '=' , 'car_models.id')
			->first();
			$cab_service = CabService::where('id', $driverDetails->fk_cab_service_id)->first();
            $riderDetails = User::where('id', $checkRide->fk_rider_id)->first();

			$notiArray= array(
				"reciever_id" =>$checkRide->fk_rider_id,
				"sender_id" => 0,
				"title" => 'Ride completed.',
				"message" => "Your ride is completed.",
				"status" => "sent",
				"sent_on" => date('Y-m-d'),
				"sent_at" =>date('h:i:s a'),
			);

				//noti array

			$msg = array('message'=>$notiArray['message'],
				"start_lat_long" =>[
					"start_latitude" =>$checkRide->destination_from_lat,
					"start_longitude"=>$checkRide->destination_from_lng] ,
					"end_lat_long"=>[
						"latitude"=>  $checkRide->destination_to_lat,
						"longitude"=>$checkRide->destination_to_lng ],
						"driver_details"=> [
							"id"=> $driverDetails->id,
							"name"=> $driverDetails->name,
							"email"=> $driverDetails->email,
							"phone"=> $driverDetails->phone_no,
							"picture"=> ($driverDetails->image_url !== NULL ) ? url('/').'/uploads/drivers/'.$driverDetails->id.'/'.$driverDetails->image_url : "https://cdn-makehimyours.pressidium.com/wp-content/uploads/2016/11/Depositphotos_9830876_l-2015Optimised.jpg",
							"rating" => "4",

							"vehicle_details"=> [
								"vehicle_no" =>($driverDetails->car_number !== null)? $driverDetails->car_number:"",
								"vehicle_model" => ($driverDetails->car_model_name !== null)? $driverDetails->car_model_name:"",
								"latitude"=>($driverDetails->latitude !== null)?  $driverDetails->latitude:"",
								"longitude"=>($driverDetails->longitude !== null)? $driverDetails->longitude:"",
								"rider_space" => $cab_service->rider_space,
								"cab_service" => $cab_service->name,
								"car_img" => "https://pp.netclipart.com/pp/s/6-63856_car-clip-art-png-car-clipart-png.png",
							] 
						],
						"fare" => $checkRide->fare,
						'pickup_address' => $checkRide->destination_from,
						'drop_address' => $checkRide->destination_to,
						"status"=>"Sent success",
						'type'=>'completed',
					);


			if($devices_->device_type == "android" && $devices_->device_token !== null){

				$notifymsg=array(
					"title" =>$notiArray['title'],
					'data'=>$msg,
					'to'  =>$devices_->device_token,
					'sent_on'=>$notiArray['sent_on'],
					'sent_at'=>$notiArray['sent_at'],
					'type'=>'completed',
				);

				$this->android_deviceNotification($notifymsg);


    				} //  android noti
    				elseif($devices_->device_type =="ios" && $devices_->device_token !== null){


    					$body['aps'] = array(
    						'alert' => $notiArray['message'],
    						'sound' => 'default',
    						'badge' => 1,
    						'content-available' => 1,
    						"title" =>$notiArray['title'],
    						'data'=>$msg,
    						'type'=>'completed',
    					);
    					$this->ios_deviceNotification($devices_->device_token,$body);


    				} // ios noti


    				$ride_details = array(
    					"vehicle" => [
    						"vehicle_no" =>$driverDetails->car_number,
    						"vehicle_model" => $driverDetails->car_model_name,
    						"service" => $cab_service->name,
    						"rider_space" => $cab_service->rider_space,
    					],
    					"driver" => [
    						"id"=> $driverDetails->id,
    						"driver_name" =>$driverDetails->name,
    						"driver_rating" =>"4",
    				//"driver_gender" =>$driverDetails->v_gender,
    				//"total_rides" =>$completedRides,
    					],
    					"location" => [
    						"driver_lat_long" =>[
    							$driverDetails->latitude,
    							$driverDetails->longitude
    						],
    						"start_lat_long" =>[
    							$checkRide->destination_from_lat,
    							$checkRide->destination_from_lng
    						],
    						"end_lat_long" =>[
    							$checkRide->destination_to_lat,
    							$checkRide->destination_to_lng
    						],

    					],

    					"fare" => $checkRide->fare,
    					"trip_status" => $checkRide->trip_status,
    					"ride_id" =>$checkRide->id,
                        "rider_details" => $riderDetails,
    				);

    				$response->status = true;
    				$response->code = 200;
    				$response->message = 'Your ride is completed.';
    				$response->data = $ride_details;
    				return response()->json($response);

    			}

    		}
} // end of complete ride api




/*
*Api to get trip history
*id(driver id)
*/

public function driverTripsHistory(Request $request)
{
  $response = new \stdClass();
  $validator = Validator::make($request->all(), [
    'id' => 'required'
]);

  if($validator->fails()){
    $response->status = false;
    $response->code = 400;
    $response->message = 'validation failed';
    $response->data = $validator->errors();
    return response()->json($response);
}
else 
{
    $response_data = [];
    $details = Trip::where('trips.fk_driver_id' ,$request->id)
    ->where('trip_status','=','completed')
    ->orWhere('trip_status','=','cancelled')
    ->leftJoin('users', 'trips.fk_rider_id' ,'=' ,'users.id')
    ->leftJoin('cab_services' , 'cab_services.id'  ,'=', 'users.fk_cab_service_id')
    ->leftJoin('car_models' ,'users.car_model_id' ,'=', 'car_models.id')
    ->select('*', 'trips.id as ride_id', 'users.id as rider_id','cab_services.name as cab_service_name' ,'users.name as rider_name')
    ->orderBy('trips.id', 'DESC')->get();
    //dd($details);

    foreach ($details as $key => $value) {
      $response_data [] =  array(

        "trip_details" => [

          'ride_id' => $value->ride_id,
          "booking_no" => $value->booking_no,
          "destination_from" => $value->destination_from,
          "destination_to" => $value->destination_to,
          "destination_from_lat" => $value->destination_from_lat,
          "destination_from_lng" => $value->ridedestination_from_lng_id,
          "destination_to_lat" => $value->destination_to_lat,
          "destination_to_lng" => $value->destination_to_lng,
          "trip_date" => $value->trip_date,
          "trip_started_at" =>  $value->trip_started_at,
          "trip_ended_at" =>  $value->trip_ended_at,
          "fare" =>  $value->fare,
          "trip_status" =>  $value->trip_status,
          "trip_category" =>  $value->trip_category,

          "trip_rating" => [
              "rating" => "4",
              "review" => "Nice ride, value for money."
          ]

      ],

      "rider_details"=> [
          "id"=> $value->rider_id,
          "name"=> $value->rider_name,
          "email"=> $value->email,
          "phone"=> $value->phone_no,
          "picture"=> ($value->image_url !== null) ?  url('/').'/uploads/passengers/'.$value->rider_id.'/'.$value->image_url : "",
          "rating" => "4",

      ],


      "vehicle_details"=> [
        "vehicle_no" =>($value->car_number !== null)? $value->car_number:"",
        "vehicle_model" => ($value->car_model_name !== null)? $value->car_model_name:"",
        "rider_space" => $value->rider_space,
        "cab_service" => $value->name,
        "car_img" => "https://pp.netclipart.com/pp/s/6-63856_car-clip-art-png-car-clipart-png.png"

    ] 

);
  }

  if(sizeof($response_data) > 0){
   $response->status = true;
   $response->code = 200;
   $response->message = 'Trips history feteched successfully';
   $response->data = $response_data;
   return response()->json($response);
}

else{
   $response->status = true;
   $response->code = 401;
   $response->message = 'No trip history found';
   $response->data = $response_data;
   return response()->json($response);
}

}
}




/*
API FOR EARNIG SCREEN
*/

public function EarningDetails(Request $request)
{
    $response = new \stdClass();
    $validator = Validator::make($request->all(), [
        'driver_id' => 'required'
    ]);

    if($validator->fails()):
        $response->status = false;
        $response->code = 400;
        $response->message = 'validation failed';
        $response->data = $validator->errors();
        return response()->json($response);

    else:

        $check_if_driver_exist = User::where('id', $request->driver_id)->where('user_type','driver')->first();

        if($check_if_driver_exist === null):
           $response->status = false;
           $response->code = 400;
           $response->message = 'Invalid driver id';
           return response()->json($response);

       else:
        $current_month = date('m');
        $current_year = date('Y');
            $current_month_earning = Trip::whereYear('created_at', '=', $current_year)
                                      ->whereMonth('created_at', '=', $current_month)
                                      ->where('fk_driver_id', $request->driver_id)
                                      ->sum('trips.fare');
              $current_month_completed_trips = Trip::whereYear('created_at', '=', $current_year)
                                                  ->whereMonth('created_at', '=', $current_month)
                                                  ->where('fk_driver_id', $request->driver_id)
                                                   ->where('trip_status', 'completed')
                                                    ->count();
                 $current_month_canceled_trips = Trip::whereYear('created_at', '=', $current_year)
                                                  ->whereMonth('created_at', '=', $current_month)
                                                  ->where('fk_driver_id', $request->driver_id)
                                                   ->where('trip_status', 'cancelled')
                                                   ->where('cancelled_by', '2')
                                                    ->count();

                $total_completed_trips =  Trip::where('fk_driver_id', $request->driver_id)
                                            ->where('trip_status', 'completed')
                                            ->count();
             
                 $current_month_analytics = ["Personal_balance" => "$".$check_if_driver_exist->wallet_balance ,"earning" => "$".(int)$current_month_earning , "completed_trips" => (int)$current_month_completed_trips, "cancelled_trips" => (int)$current_month_canceled_trips];
                 $revenue_arr =  ["withdraw" => 0 , "pending_clearance" =>0 , "total_completed_trips" => $total_completed_trips];
            //  dd($current_month_analytics);

              $full_details = ["current_month_analytics" => $current_month_analytics,
                                "revenue"=> $revenue_arr];

               $response->status = true;
           $response->code = 200;
           $response->message = 'Details fetched successfully';
           $response->data = $full_details;
           return response()->json($response);
       endif;



   endif;
}

} //End of controller
