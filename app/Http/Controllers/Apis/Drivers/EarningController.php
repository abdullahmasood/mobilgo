<?php
namespace App\Http\Controllers\Apis\Drivers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use File;
use Auth;
use App\User;
use App\Company;
use App\Trip;
use App\CabService;
use App\Notification;
use App\Devices;
use App\CarModels;
use \Illuminate\Support\Facades\Validator;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use  App\Http\Controllers\Apis\BaseController as apiBaseController;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Credentials: true');

class EarningController extends apiBaseController
{

	public function CompletedTripsAnalytics(Request $request)
	{
		$response = new \stdClass();
		$validator = Validator::make($request->all(), [
			"id" => 'required',
			"tab_type" => 'required',
		]);

		if($validator->fails()):

			$response->status = false;
			$response->code = 400;
			$response->message = 'validation failed';
			$response->data = $validator->errors();
			return response()->json($response);

		else:
			$monday = strtotime('next Monday -1 week');
			$monday = date('w', $monday)==date('w') ? strtotime(date("Y-m-d",$monday)." +7 days") : $monday;
			$sunday = strtotime(date("Y-m-d",$monday)." +6 days");
			$this_week_sd = date("Y-m-d",$monday)."<br>";
			//echo $this_week_ed = date("Y-m-d",$sunday)."<br>";
			

			$today_date = date('Y-m-d');
			$current_month = date('m');

			if( $request->tab_type == 'today' ):
				

				$details = Trip::where('trip_date', $today_date)
				//->join('payments', 'trips.id' ,'=', 'payments.fk_trip_id')
				->where('trip_status','completed')
				->select('*', 'trips.id as trip_id')->get();
				
				if(sizeof($details) > 0 ): 

					$sum_result = Trip::where('trip_date', $today_date)->select(\DB::raw('SUM(fare) as total_fare'),\DB::raw('SUM(trip_distance) as total_distance'))->first();

					//print_r($sum_result); die;

					$result =  ["total_fare" => (int)$sum_result['total_fare'] , "total_distance" => (int)$sum_result['total_distance'] , "total_rides" => count($details)];

					foreach ($details as $key => $value):
						$rider_details = User::where('id', $value->fk_rider_id)->first();
						$rider_details->picture =($rider_details->image_url !== NULL) ?  url('/').'/uploads/passengers/'.$rider_details->id.'/'.$rider_details->image_url : "https://cdn-makehimyours.pressidium.com/wp-content/uploads/2016/11/Depositphotos_9830876_l-2015Optimised.jpg";
						$details[$key]->rider_details = $rider_details;
					endforeach;	

					$response->status = true;
					$response->code = 200;
					$response->message = 'Details fetched successfully';
					$response->data = ["cards" => $result ,  "list" => $details];
					return response()->json($response);

				else:

					$response->status = false;
					$response->code = 401;
					$response->message = 'No details found';
					$response->data = [];
					return response()->json($response);


				endif;

				//End today

			elseif( $request->tab_type == 'weekly'):

				$details = Trip::whereBetween('trip_date', [$this_week_sd , $today_date])
				->where('trip_status','completed')
				//->join('payments', 'trips.id' ,'=', 'payments.fk_trip_id')
				->select('*', 'trips.id as trip_id')->get();

				if(sizeof($details) > 0):
					$ids = Trip::whereBetween('trip_date', [$this_week_sd , $today_date])->pluck('id');
					$sum_result = Trip::whereBetween('trip_date', [$this_week_sd , $today_date])->select(\DB::raw('SUM(fare) as total_fare'),\DB::raw('SUM(trip_distance) as total_distance'))->first();

					//print_r($sum_result); die;

					$result =  ["total_fare" => (int)$sum_result['total_fare'] , "total_distance" => (int)$sum_result['total_distance'] , "total_rides" => count($details)];

					foreach ($details as $key => $value):
						$rider_details = User::where('id', $value->fk_rider_id)->first();
						$rider_details->picture =($rider_details->image_url !== NULL) ?  url('/').'/uploads/passengers/'.$rider_details->id.'/'.$rider_details->image_url : "https://cdn-makehimyours.pressidium.com/wp-content/uploads/2016/11/Depositphotos_9830876_l-2015Optimised.jpg";
						$details[$key]->rider_details = $rider_details;
					endforeach;	

					$response->status = true;
					$response->code = 200;
					$response->message = 'Details fetched successfully';
					$response->data = ["cards" => $result ,  "list" => $details];
					return response()->json($response);	

				else:

					$response->status = false;
					$response->code = 401;
					$response->message = 'No details found';
					$response->data = [];
					return response()->json($response);

				endif;


				//End weekly
			else:
				//Monthly block


				$details = Trip::whereMonth('trip_date', $current_month )
				->where('trip_status','completed')
							//->join('payments', 'trips.id' ,'=', 'payments.fk_trip_id')
				->select('*', 'trips.id as trip_id')->get();

				if(sizeof($details) > 0):

					$ids = Trip::whereMonth('trip_date', $current_month )->pluck('id');
					$sum_result = Trip::whereMonth('trip_date', $current_month )->select(\DB::raw('SUM(fare) as total_fare'),\DB::raw('SUM(trip_distance) as total_distance'))->first();

					//print_r($sum_result); die;

					$result =  ["total_fare" => (int)$sum_result['total_fare'] , "total_distance" => (int)$sum_result['total_distance'] , "total_rides" => count($details)];

					foreach ($details as $key => $value):
						$rider_details = User::where('id', $value->fk_rider_id)->first();
						$rider_details->picture =($rider_details->image_url !== NULL) ?  url('/').'/uploads/passengers/'.$rider_details->id.'/'.$rider_details->image_url : "https://cdn-makehimyours.pressidium.com/wp-content/uploads/2016/11/Depositphotos_9830876_l-2015Optimised.jpg";
						$details[$key]->rider_details = $rider_details;
					endforeach;	

					$response->status = true;
					$response->code = 200;
					$response->message = 'Details fetched successfully';
					$response->data = ["cards" => $result ,  "list" => $details];
					return response()->json($response);	

				else:

					$response->status = false;
					$response->code = 401;
					$response->message = 'No details found';
					$response->data = [];
					return response()->json($response);

				endif;




			endif;



		endif;

	}




	/*Api to get analytics of cancel ride*/


	public function CancelTripsAnalytics(Request $request)
	{
		$response = new \stdClass();
		$validator = Validator::make($request->all(), [
			"id" => 'required',
			"tab_type" => 'required',
		]);

		if($validator->fails()):

			$response->status = false;
			$response->code = 400;
			$response->message = 'validation failed';
			$response->data = $validator->errors();
			return response()->json($response);

		else:
			$monday = strtotime('next Monday -1 week');
			$monday = date('w', $monday)==date('w') ? strtotime(date("Y-m-d",$monday)." +7 days") : $monday;
			$sunday = strtotime(date("Y-m-d",$monday)." +6 days");
			$this_week_sd = date("Y-m-d",$monday)."<br>";
			//echo $this_week_ed = date("Y-m-d",$sunday)."<br>";
			

			$today_date = date('Y-m-d');
			$current_month = date('m');

			if( $request->tab_type == 'today' ):
				

				$details = Trip::where('trip_date', $today_date)
				//->join('payments', 'trips.id' ,'=', 'payments.fk_trip_id')
				->where('trip_status','cancelled')
				->select('*', 'trips.id as trip_id')->get();
				
				if(sizeof($details) > 0 ): 

					$sum_result = Trip::where('trip_date', $today_date)->select(\DB::raw('SUM(fare) as total_fare'),\DB::raw('SUM(trip_distance) as total_distance'))->first();

					//print_r($sum_result); die;

					$result =  ["total_fare" => (int)$sum_result['total_fare'] , "total_distance" => (int)$sum_result['total_distance'] , "total_rides" => count($details)];

					foreach ($details as $key => $value):
						$rider_details = User::where('id', $value->fk_rider_id)->first();
						$rider_details->picture =($rider_details->image_url !== NULL) ?  url('/').'/uploads/passengers/'.$rider_details->id.'/'.$rider_details->image_url : "https://cdn-makehimyours.pressidium.com/wp-content/uploads/2016/11/Depositphotos_9830876_l-2015Optimised.jpg";
						$details[$key]->rider_details = $rider_details;
					endforeach;	

					$response->status = true;
					$response->code = 200;
					$response->message = 'Details fetched successfully';
					$response->data = ["cards" => $result ,  "list" => $details];
					return response()->json($response);

				else:

					$response->status = false;
					$response->code = 401;
					$response->message = 'No details found';
					$response->data = [];
					return response()->json($response);


				endif;

				//End today

			elseif( $request->tab_type == 'weekly'):

				$details = Trip::whereBetween('trip_date', [$this_week_sd , $today_date])
				->where('trip_status','cancelled')
				//->join('payments', 'trips.id' ,'=', 'payments.fk_trip_id')
				->select('*', 'trips.id as trip_id')->get();

				if(sizeof($details) > 0):
					$ids = Trip::whereBetween('trip_date', [$this_week_sd , $today_date])->pluck('id');
					$sum_result = Trip::whereBetween('trip_date', [$this_week_sd , $today_date])->select(\DB::raw('SUM(fare) as total_fare'),\DB::raw('SUM(trip_distance) as total_distance'))->first();

					//print_r($sum_result); die;

					$result =  ["total_fare" => (int)$sum_result['total_fare'] , "total_distance" => (int)$sum_result['total_distance'] , "total_rides" => count($details)];

					foreach ($details as $key => $value):
						$rider_details = User::where('id', $value->fk_rider_id)->first();
						$rider_details->picture =($rider_details->image_url !== NULL) ?  url('/').'/uploads/passengers/'.$rider_details->id.'/'.$rider_details->image_url : "https://cdn-makehimyours.pressidium.com/wp-content/uploads/2016/11/Depositphotos_9830876_l-2015Optimised.jpg";
						$details[$key]->rider_details = $rider_details;
					endforeach;	

					$response->status = true;
					$response->code = 200;
					$response->message = 'Details fetched successfully';
					$response->data = ["cards" => $result ,  "list" => $details];
					return response()->json($response);	

				else:

					$response->status = false;
					$response->code = 401;
					$response->message = 'No details found';
					$response->data = [];
					return response()->json($response);

				endif;


				//End weekly
			else:
				//Monthly block


				$details = Trip::whereMonth('trip_date', $current_month )
				->where('trip_status','cancelled')
							//->join('payments', 'trips.id' ,'=', 'payments.fk_trip_id')
				->select('*', 'trips.id as trip_id')->get();

				if(sizeof($details) > 0):

					$ids = Trip::whereMonth('trip_date', $current_month )->pluck('id');
					$sum_result = Trip::whereMonth('trip_date', $current_month )->select(\DB::raw('SUM(fare) as total_fare'),\DB::raw('SUM(trip_distance) as total_distance'))->first();

					//print_r($sum_result); die;

					$result =  ["total_fare" => (int)$sum_result['total_fare'] , "total_distance" => (int)$sum_result['total_distance'] , "total_rides" => count($details)];

					foreach ($details as $key => $value):
						$rider_details = User::where('id', $value->fk_rider_id)->first();
						$rider_details->picture =($rider_details->image_url !== NULL) ?  url('/').'/uploads/passengers/'.$rider_details->id.'/'.$rider_details->image_url : "https://cdn-makehimyours.pressidium.com/wp-content/uploads/2016/11/Depositphotos_9830876_l-2015Optimised.jpg";
						$details[$key]->rider_details = $rider_details;
					endforeach;	

					$response->status = true;
					$response->code = 200;
					$response->message = 'Details fetched successfully';
					$response->data = ["cards" => $result ,  "list" => $details];
					return response()->json($response);	

				else:

					$response->status = false;
					$response->code = 401;
					$response->message = 'No details found';
					$response->data = [];
					return response()->json($response);

				endif;




			endif;



		endif;

	}


	/*Api total completed trips list*/
	public function totalCompletedTrips(Request $request)
	{
		$response = new \stdClass();
		$validator = Validator::make($request->all(), [
			'id' => 'required'
		]);

		if($validator->fails()){
			$response->status = false;
			$response->code = 400;
			$response->message = 'validation failed';
			$response->data = $validator->errors();
			return response()->json($response);
		}
		else 
		{
			$response_data = [];
			$details = Trip::where('trips.fk_driver_id' ,$request->id)
			->where('trip_status','=','completed')
    //->orWhere('trip_status','=','cancelled')
			->leftJoin('users', 'trips.fk_rider_id' ,'=' ,'users.id')
			->leftJoin('cab_services' , 'cab_services.id'  ,'=', 'users.fk_cab_service_id')
			->leftJoin('car_models' ,'users.car_model_id' ,'=', 'car_models.id')
			->select('*', 'trips.id as ride_id', 'users.id as rider_id','cab_services.name as cab_service_name' ,'users.name as rider_name')
			->orderBy('trips.id', 'DESC')->get();
    //dd($details);

			foreach ($details as $key => $value) {
				$response_data [] =  array(

					"trip_details" => [

						'ride_id' => $value->ride_id,
						"booking_no" => $value->booking_no,
						"destination_from" => $value->destination_from,
						"destination_to" => $value->destination_to,
						"destination_from_lat" => $value->destination_from_lat,
						"destination_from_lng" => $value->ridedestination_from_lng_id,
						"destination_to_lat" => $value->destination_to_lat,
						"destination_to_lng" => $value->destination_to_lng,
						"trip_date" => $value->trip_date,
						"trip_started_at" =>  $value->trip_started_at,
						"trip_ended_at" =>  $value->trip_ended_at,
						"fare" =>  $value->fare,
						"trip_status" =>  $value->trip_status,
						"trip_category" =>  $value->trip_category,

						"trip_rating" => [
							"rating" => "4",
							"review" => "Nice ride, value for money."
						]

					],

					"rider_details"=> [
						"id"=> $value->rider_id,
						"name"=> $value->rider_name,
						"email"=> $value->email,
						"phone"=> $value->phone_no,
						"picture"=> ($value->image_url !== null) ?  url('/').'/uploads/passengers/'.$value->rider_id.'/'.$value->image_url : "",
						"rating" => "4",

					],


					"vehicle_details"=> [
						"vehicle_no" =>($value->car_number !== null)? $value->car_number:"",
						"vehicle_model" => ($value->car_model_name !== null)? $value->car_model_name:"",
						"rider_space" => $value->rider_space,
						"cab_service" => $value->name,
						"car_img" => "https://pp.netclipart.com/pp/s/6-63856_car-clip-art-png-car-clipart-png.png"

					] 

				);
			}

			if(sizeof($response_data) > 0){
				$response->status = true;
				$response->code = 200;
				$response->message = 'Completed trips history feteched successfully';
				$response->data = $response_data;
				return response()->json($response);
			}

			else{
				$response->status = true;
				$response->code = 401;
				$response->message = 'No data found';
				$response->data = $response_data;
				return response()->json($response);
			}

		}
	}


}//End of controller