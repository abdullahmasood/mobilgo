<?php
namespace App\Http\Controllers\Apis;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use File;
use Auth;
use App\User;
use App\Company;
use App\Trip;
use App\CabService;
use App\Notification;
use App\CarModels;
use App\Devices;
use App\Review;
use DB;
use \Illuminate\Support\Facades\Validator;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use  App\Http\Controllers\Apis\BaseController as apiBaseController;
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Credentials: true');

class CommonApisController extends apiBaseController
{
   public function get_register(Request $request){
    $response = new \stdClass();
    $validator = Validator::make($request->all(), [
        'password' => 'required|min:6',
        'email' => 'required',
        'user_type' => 'required'

    ]);

    if($validator->fails()){
        $response->status = false;
        $response->code = 400;
        $response->message = 'validation failed';
        $response->data = $validator->errors();
        return response()->json($response); 

    } else {
        $exist = User::where('email',$request->email)->first();

        if(!$exist)
        {

            if($request->user_type === 'driver')
            {
                $driver = new User();
                $driver->name = $request->name;
                $driver->email = $request->email;
                $driver->car_model_id = $request->car_model_id;
                $driver->car_number = $request->car_number;
                $driver->fk_cab_service_id = $request->cab_service_id;
                $driver->phone_no = $request->phone_no;
                           // $driver->fk_company_id = $request->fk_company_id;//$company_exist->id;
                $driver->user_type = $request->user_type;
                $driver->password = Hash::make($request->password);
                $driver->status = "Active";
                $driver->oauth_provider = "standard";
                $driver->latitude = $request->lat;
                $driver->longitude = $request->long;
                $driver->save(); 
                            //Img upload

                $destinationPath =  public_path( 'uploads/drivers/'.$driver->id);              
                if(!File::isDirectory($destinationPath)) {
                  mkdir($destinationPath, 0777, true);
                  chmod($destinationPath,0777);
              }

              $image = $request->file('license_img');
              if ($image) {
                $imageName = 'license_'.$driver->id .'.'.$image->getClientOriginalExtension();
                $image->move($destinationPath,$imageName);
                $driver->license_img = $imageName;
                $driver->save();
              }



                            //End img upload
              $user = User::where('id',$driver->id)->first();  
                           // $user->password = $request->password;  
              $user->license_img = url('/').'/uploads/drivers/'.$driver->id.'/'.$user->license_img;
              $response->status = true;
              $response->code = 200;
              $response->message = 'Driver added successfully';
              $response->data = $user;
              return response()->json($response);
          }
          else if($request->user_type === 'rider')
          {
            $rider = new User();
            $rider->name = $request->name;
            $rider->email = $request->email;
                            //$driver->image_url = $path;//$filenameToStore;
            $rider->phone_no = $request->phone_no;
            $rider->user_type = $request->user_type;
            $rider->password = Hash::make($request->password);
            $rider->status = "Active";
            $rider->oauth_provider = "standard";
            $rider->latitude = $request->lat;
            $rider->longitude = $request->long;
            $rider->save();
            $user = User::where('id',$rider->id)->first();    
                          //  $user->password = $request->password;  
            $response->status = true;
            $response->code = 200;
            $response->message = 'Rider added successfully';
            $response->data = $user;
            return response()->json($response);
        }



    }
    else{
        $response->status = false;
        $response->code = 409;
        $response->message = 'email already exist';
        $response->data = [];
        return response()->json($response);
    }
}
}


        /*
    	* Log out api
        */

        public function commonLogout(Request $request){
            $response = new \stdClass();

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                //'email' => 'required',
               // 'user_type' => 'required'

            ]);

            if($validator->fails()){
            	$response->status = false;
                $response->code = 400;
                $response->message = 'validation failed';
                $response->data = $validator->errors();
                return response()->json($response); 
            }
            else{
                $user = User::find($request->id);

                if($user){
                    $device = Devices::where('user_id',$user->id);    
                    $device->delete();
                    $user->remember_token = null;
                    $user->save();
                    $response->status = true;
                    $response->code = 200;
                    $response->message = 'logout Successfully';
                    $response->data = [];
                    return response()->json($response);
                }else{
                    $response->status = false;
                    $response->code = 404;
                    $response->message = 'No User Found';
                    $response->data = [];
                    return response()->json($response);
                }
            }
        }


        /*
        * Api to upload profile picture
        */


        public function updateProfilePicture(Request $request){
         $response = new \stdClass();
         $validator = Validator::make($request->all(), [
            'id' => 'required',
            'user_type' => 'required',
            'profile_picture' => 'image|mimes:jpeg,png,jpg',

        ]);

         if($validator->fails()){
            $response->status = false;
            $response->code = 400;
            $response->message = 'validation failed';
            $response->data = $validator->errors();
            return response()->json($response); 

        }
        else{

           $user = User::find($request->id);

           if($user){

              if($request->hasFile('profile_picture')){

                if ($request->user_type == "rider") {
                    $destinationPath = public_path( 'uploads/passengers/'.$request->id);
                }
                else{
                    $destinationPath =  public_path( 'uploads/drivers/'.$request->id) ;
                }                      

                if(!File::isDirectory($destinationPath)) {
                  mkdir($destinationPath, 0777, true);
                  chmod($destinationPath,0777);
              }

              $image = $request->file('profile_picture');
              $imageName = $request->id .'.'.$image->getClientOriginalExtension();
              $image->move($destinationPath,$imageName);
              $user->image_url = $imageName;
              $user->save();
              $user->image_url = ($request->user_type == 'rider') ? url('/').'/uploads/passengers/'.$request->id.'/'.$imageName :  url('/').'/uploads/drivers/'.$request->id.'/'.$imageName;


              $response->status = true;
              $response->code = 200;
              $response->message = 'Profile picture updated successfully';
              $response->data = $user;
              return response()->json($response);

          }else{
            $response->status = false;
            $response->code = 404;
            $response->message = 'No image Found';
            $response->data = [];
            return response()->json($response);
        }

    }else{
        $response->status = false;
        $response->code = 404;
        $response->message = 'No User Found';
        $response->data = [];
        return response()->json($response);
    }



}

}




    /*
    * Api to get profile details
    */


    public function getProfileDetails(Request $request)
    {
     $response = new \stdClass();
     $validator = Validator::make($request->all(), [
        'id' => 'required',
    ]);

     if($validator->fails()){
        $response->status = false;
        $response->code = 400;
        $response->message = 'validation failed';
        $response->data = $validator->errors();
        return response()->json($response); 

    }
    else{
     $user = User::find($request->id);

     if($user){
      $user->image_url = ($user->user_type == 'rider') ? url('/').'/uploads/passengers/'.$request->id.'/'.$user->image_url :  url('/').'/uploads/drivers/'.$request->id.'/'.$user->image_url;

      $response->status = true;
      $response->code = 200;
      $response->message = 'Details fetched successfully';
      $response->data = $user;
      return response()->json($response);
  }
  else{
     $response->status = false;
     $response->code = 404;
     $response->message = 'No User Found';
     $response->data = [];
     return response()->json($response);
 }

}
}


    /*
    * Api to get car models list
    */

    public function getCarModelsList(){
        $response = new \stdClass();
        $car_models = CarModels::orderBy("car_model_name")->get();
        $response->status = true;
        $response->code = 200;
        $response->message = 'Cars list fetched successfully';
        $response->data = $car_models;
        return response()->json($response);

    }


        /*
    * Api to get car models list
    */

        public function cabServices(){
            $response = new \stdClass();
            $car_services = \DB::table('cab_services')->orderBy("name")->get();
            $response->status = true;
            $response->code = 200;
            $response->message = 'Cars services list fetched successfully';
            $response->data = $car_services;
            return response()->json($response);

        }


    /*
    *Api to reset ride request
    */

    public function resetRideRequest(Request $request)
    {
     $response = new \stdClass();
     $validator = Validator::make($request->all(), [
        'id' => 'required',
    ]);

     if($validator->fails()){
        $response->status = false;
        $response->code = 400;
        $response->message = 'validation failed';
        $response->data = $validator->errors();
        return response()->json($response); 

    }
    else{

        $checkIfRequested = Trip::where('fk_rider_id', $request->id)->where('trip_status','requested');
        if($checkIfRequested){
            $checkIfRequested->delete();
            $response->status = true;
            $response->code = 200;
            $response->message = 'success';
            return response()->json($response);

        }else{
             $response->status = false;
            $response->code = 400;
            $response->message = 'No ride request found';
            return response()->json($response);
        }
    }
}



public function AddRatingReviews(Request $request)
{
	  $response = new \stdClass();
     $validator = Validator::make($request->all(), [
        'rider_id' => 'required',
        'driver_id' => 'required',
        'rating' => 'required',
        'comment' => 'required',
        'trip_id' => 'required'
    ]);

     if($validator->fails()):
     	$response->status = false;
        $response->code = 400;
        $response->message = 'validation failed';
        $response->data = $validator->errors();
        return response()->json($response); 

    else:

    	$check_if_rider_exist = User::where('id',$request->rider_id)->where('user_type','rider')->first();
    	$check_if_driver_exist = User::where('id',$request->driver_id)->where('user_type','driver')->first();

    	if($check_if_rider_exist === null):
    			$response->status = false;
		        $response->code = 401;
		        $response->message = 'Invalid rider Id';
		        return response()->json($response); 

    		else:

    			if($check_if_driver_exist === null):
	    			$response->status = false;
			        $response->code = 401;
			        $response->message = 'Invalid driver Id';
			        return response()->json($response); 

			    else:
			    	$check_if_review_exist = Review::where('fk_trip_id', $request->trip_id)->first();

			    	if($check_if_review_exist === null):
			    	$review  = new Review();
			    	$review->fk_rider_id = $request->rider_id;
			    	$review->fk_driver_id = $request->driver_id;
			    	$review->rider_rating = $request->rating;
			    	$review->comment = $request->comment;
			    	$review->fk_trip_id = $request->trip_id;
			    	$review->save();
            $this->ReviewGivenNotification($request->trip_id, $review->id);

				    $response->status = true;
			        $response->code = 200;
			        $response->message = 'Rating added successfully';
              $response->data = $review;
			        return response()->json($response);

			    	else:

			    	$response->status = false;
			        $response->code = 401;
			        $response->message = 'Review for this trip already exist';
			        return response()->json($response); 

			    	endif;

			    	 


    			endif;



    	endif;


     endif;
}



/*
*Api to get upcoming rides
*/

public function UpcomingRides(Request $request)
{
    $response = new \stdClass();
     $validator = Validator::make($request->all(), [
        'id' => 'required',
        'user_type' => 'required',
    ]);

     if($validator->fails()):

        $response->status = false;
        $response->code = 400;
        $response->message = 'validation failed';
        $response->data = $validator->errors();
        return response()->json($response); 

      else:
        $checkIfIdExist = User::where('id', $request->id)->where('user_type', $request->user_type)->first();

        if($checkIfIdExist === null):
            $response->status = false;
            $response->code = 401;
            $response->message = 'User does not exist, invalid ID';
            return response()->json($response); 

             else:


                 
                  if($request->user_type == 'rider'):

                      $details =  Trip::where('trips.fk_rider_id' ,$request->id)
                       ->where('trip_category','=','schedule_trip')
                      ->Where('trip_status','=','accepted')
                      ->whereDate('trip_date','>=', date('Y-m-d'))
                      // ->leftJoin('users', 'trips.fk_rider_id' ,'=' ,'users.id')
                      // ->leftJoin('cab_services' , 'cab_services.id'  ,'=', 'users.fk_cab_service_id')
                      // ->leftJoin('car_models' ,'users.car_model_id' ,'=', 'car_models.id')
                      // ->select('*', 'trips.id as ride_id', 'users.id as rider_id','cab_services.name as cab_service_name' ,'users.name as rider_name' )
                      ->orderBy('trips.id', 'DESC')->get();
                    else:
                       $details =  Trip::where('trips.fk_driver_id' ,$request->id)
                        ->where('trip_category','=','schedule_trip')
                        ->Where('trip_status','=','accepted')
                        ->whereDate('trip_date','>=', date('Y-m-d'))
                        // ->leftJoin('users', 'trips.fk_rider_id' ,'=' ,'users.id')
                        // ->leftJoin('cab_services' , 'cab_services.id'  ,'=', 'users.fk_cab_service_id')
                        // ->leftJoin('car_models' ,'users.car_model_id' ,'=', 'car_models.id')
                        // ->select('*', 'trips.id as ride_id', 'users.id as rider_id','cab_services.name as cab_service_name' ,'users.name as rider_name')
                        ->orderBy('trips.id', 'DESC')->get();
                endif;


                  //dd($details);

                  if($details->isEmpty()):
                              $response->status = false;
                              $response->code = 401;
                              $response->message = 'No Upcoming rides';
                              $response->data = $details;
                              return response()->json($response);

                              else:

                              	 $today_date_time =  date('Y-m-d H:i:s');
			     
                                
                                foreach ($details as $key => $value):
                                	 $trip_schedule_time = date('Y-m-d H:i:s',strtotime("-30 minutes",strtotime($value->trip_schedule_time)));

                                	   $check_schedule_date = (strtotime($today_date_time) ==  strtotime($trip_schedule_time)) ? true : false;

                                	   $set_btn_flag = ($check_schedule_date == true) ? 1 :0;
                                	   
                                	   $details[$key]->button_flag = $set_btn_flag;
          
                                  $details[$key]->driver_details = User::where('users.id', $value->fk_driver_id)
                                   ->leftJoin('cab_services' , 'cab_services.id'  ,'=', 'users.fk_cab_service_id')
                                    ->leftJoin('car_models' ,'users.car_model_id' ,'=', 'car_models.id')
                                   ->select('*', 'users.name as name', 'users.id as driver_id','cab_services.name as cab_service_name')->first();
                                  $details[$key]->rider_details = User::where('id', $value->fk_rider_id)->first();
                               endforeach;

                               

                                      $response->status = true;
                                      $response->code = 200;
                                      $response->message = 'Upcoming rides details fetched successfully';
                                      $response->data = $details;
                                      return response()->json($response);  
                  endif;

        endif;

     


    endif;
}

/*
End of upcoming api
*/




/*Function to send notification on payment complete to driver*/

public function ReviewGivenNotification($trip_id, $review_id)
{
  $checkRide = Trip::where('id', $trip_id)->first();
  $review_details = Review::where('id', $review_id)->first();
  $rev_details = ($review_details === null) ? array(): $review_details;

  if($checkRide):

      $devices_ = Devices::where('user_id', $checkRide->fk_driver_id)->first();
      $driverDetails = User::where('users.id',  $checkRide->fk_driver_id)
      ->leftJoin('car_models', 'users.car_model_id' , '=' , 'car_models.id')
      ->first();
      $cab_service = CabService::where('id', $driverDetails->fk_cab_service_id)->first();
      $riderDetails = User::where('id',$checkRide->fk_rider_id)->first();

      $notiArray= array(
        "reciever_id" =>$checkRide->fk_rider_id,
        "sender_id" => 0,
        "title" => 'Ride Payment has been completed.',
        "message" => "Your rider has completed his ride payment.",
        "status" => "sent",
        "sent_on" => date('Y-m-d'),
        "sent_at" =>date('h:i:s a'),
      );

      $msg = array('message'=>$notiArray['message'],
        "start_lat_long" =>[
          "start_latitude" =>$checkRide->destination_from_lat,
          "start_longitude"=>$checkRide->destination_from_lng] ,
          "end_lat_long"=>[
            "latitude"=>  $checkRide->destination_to_lat,
            "longitude"=>$checkRide->destination_to_lng ],
            "driver_details"=> [
              "id"=> $driverDetails->id,
              "name"=> $driverDetails->name,
              "email"=> $driverDetails->email,
              "phone"=> $driverDetails->phone_no,
              "picture"=> ($driverDetails->image_url !== NULL ) ? url('/').'/uploads/drivers/'.$driverDetails->id.'/'.$driverDetails->image_url : "https://cdn-makehimyours.pressidium.com/wp-content/uploads/2016/11/Depositphotos_9830876_l-2015Optimised.jpg",
              "rating" => "4",

              "vehicle_details"=> [
                "vehicle_no" =>($driverDetails->car_number !== null)? $driverDetails->car_number:"",
                "vehicle_model" => ($driverDetails->car_model_name !== null)? $driverDetails->car_model_name:"",
                "latitude"=>($driverDetails->latitude !== null)?  $driverDetails->latitude:"",
                "longitude"=>($driverDetails->longitude !== null)? $driverDetails->longitude:"",
                "rider_space" => $cab_service->rider_space,
                "cab_service" => $cab_service->name,
                "car_img" => "https://pp.netclipart.com/pp/s/6-63856_car-clip-art-png-car-clipart-png.png",
              ] 
            ],
            "review_details" =>[$rev_details],
            "rider_details"=>[
                         "rider_id"=> $riderDetails->id,
                         "email" => $riderDetails->email,
                         "name" =>$riderDetails->name,
                         "phone" =>$riderDetails->phone_no,
                         "picture" =>$riderDetails->image_url ? url('/').'/uploads/passengers/'.$request->id.'/'.$riderDetails->image_url :""],
            "fare" => $checkRide->fare,
            'pickup_address' => $checkRide->destination_from,
            'drop_address' => $checkRide->destination_to,
            "status"=>"Sent success",
            'type'=>'review',
          );

            if($devices_->device_type == "android" && $devices_->device_token !== null){

        $notifymsg=array(
          "title" =>$notiArray['title'],
          'data'=>$msg,
          'to'  =>$devices_->device_token,
          'sent_on'=>$notiArray['sent_on'],
          'sent_at'=>$notiArray['sent_at'],
          'type'=>'completed',
        );

        $this->android_deviceNotification($notifymsg);


            } //  android noti
            elseif($devices_->device_type =="ios" && $devices_->device_token !== null){


              $body['aps'] = array(
                'alert' => $notiArray['message'],
                'sound' => 'default',
                'badge' => 1,
                'content-available' => 1,
                "title" =>$notiArray['title'],
                'data'=>$msg,
                'type'=>'completed',
              );
              $this->ios_deviceNotification($devices_->device_token,$body);


            } // ios noti

    endif;
}


    } // End of controller


