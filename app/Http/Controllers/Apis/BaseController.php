<?php

namespace App\Http\Controllers\Apis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Credentials: true');
//define('ANDRIOD_PUSH_AUTH_KEY','AAAAh0vX6sI:APA91bHpqOLROKTtja0SpJx9W83FjsnV8HawyeqdKpfDxE9-eHbKBrB_OCS3ywVvqeOMzpj4tzUVG3xyKledVIkvnHecMHZ-CWV_V7dtOcW__iGKKTnEBDQg0ytKhX9FXO-i-SSoXjtD');


define('ANDRIOD_PUSH_AUTH_KEY','AAAAh0vX6sI:APA91bHpqOLROKTtja0SpJx9W83FjsnV8HawyeqdKpfDxE9-eHbKBrB_OCS3ywVvqeOMzpj4tzUVG3xyKledVIkvnHecMHZ-CWV_V7dtOcW__iGKKTnEBDQg0ytKhX9FXO-i-SSoXjtD');
// define('IOS_PUSH_AUTH_KEY','AIzaSyAaQBwLR3MlZRNUHNMDAeAxBu2O2skDO2M');
// define('IOS_CERT',public_path("ios_cert/1.pem"));
// define('IOS_CERT_USER',public_path("ios_cert/12.pem"));

class BaseController extends Controller
{
	public  function getDriverLocationByLatLong($orig_lat, $orig_lon,$service_id,$distance)
	{
		$query = \DB::Select("SELECT *, round (6353 * 2 * ASIN(SQRT( POWER(SIN(($orig_lat -abs(latitude)) * pi()/180 / 2),2) + COS($orig_lat * pi()/180 ) * COS( abs(latitude) *  pi()/180) * POWER(SIN(($orig_lon - longitude) *  pi()/180 / 2), 2) ))) as distance FROM users WHERE status ='Active' AND fk_cab_service_id = $service_id AND on_trip_status = 0 AND user_type ='driver'  having distance <= $distance ORDER BY distance");
		
		if($query == true){
			return $query;
		}
		else{
			return array();
		}
	}


	 /*
  * function for push notification for android devices of drivers
  */
	 function android_deviceNotification($data1){
	 	$payload_data = json_encode($data1);
	 	//dd($payload_data);
	 	$url = 'https://fcm.googleapis.com/fcm/send';
	 	$serverApiKey = ANDRIOD_PUSH_AUTH_KEY;
	 	$headers = array('Content-Type:application/json',
	 		'Authorization:key=' .$serverApiKey,
	 		'Content-Type:application/json'
	 	);
	 	$ch = curl_init();
	 	curl_setopt($ch, CURLOPT_URL, $url);
	 	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	 	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	 	curl_setopt($ch, CURLOPT_POST, true);
	 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	 	curl_setopt($ch, CURLOPT_POSTFIELDS,$payload_data);
	 	$response = curl_exec($ch);
	 	if (curl_errno($ch)) {
	 		$error_msg = curl_error($ch);
	 		dd($error_msg);
	 	}
	 	curl_close($ch);
	 	$result_ = json_decode($response,true);
	 	//dd($result_);
	 	return $result_;
	 }
  /*
  * function for Ios push notification of drivers devices
  */

  function ios_deviceNotification($device,$body){
  	$ctx = stream_context_create();
  	stream_context_set_option($ctx, 'ssl', 'local_cert', IOS_CERT_USER);
  	stream_context_set_option($ctx, 'ssl', 'passphrase', IOS_PUSH_AUTH_KEY);
  	$fp = stream_socket_client(
      //'ssl://feedback.push.apple.com:2196', $err,
      //'ssl://gateway.push.apple.com:2195',
  		'ssl://gateway.sandbox.push.apple.com:2195',
  		$err,
  		$errstr,
  		60,
  		STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT,
  		$ctx
  	);
  	if (!$fp)
  		exit("Failed to connect: $err $errstr" . PHP_EOL);
  	$payload = json_encode($body);
    //  print_r($device);
    //print_r($payload);die;
  	$msg = chr(0) . pack('n', 32) . pack('H*', $device) . pack('n', strlen($payload)) .  $payload;
  	$result = fwrite($fp, $msg, strlen($msg));
  	fclose($fp);
  	return $result;
  }


  function getDrivingDistance($lat1, $lat2, $long1, $long2)
  {
  	$url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&key=AIzaSyAxyiikmyMa2fPBy-Y9veUwHNos1i8OTz0";

    //print_r($url);
  	$ch = curl_init();
  	curl_setopt($ch, CURLOPT_URL, $url);
  	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  	curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
  	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  	$response = curl_exec($ch);
  	curl_close($ch);
  	$response_a = json_decode($response, true);
  //  print_r($response_a['status']);die;
  	if ($response_a['status'] !== 'OK') {
  		return array();
  	}
  	else{
  		$dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
  		$time = $response_a['rows'][0]['elements'][0]['duration']['text'];
  		if (!empty($dist)) {
  			return array('distance' => $dist, 'time' => $time);
  		}
  		else{
  			return array();
  		}
  	}


  }


} //End of controller
