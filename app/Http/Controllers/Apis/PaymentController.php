<?php
namespace App\Http\Controllers\Apis;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use File;
use Auth;
use App\User;
use App\Company;
use App\Trip;
use App\CabService;
use App\Notification;
use App\Devices;
use App\CarModels;
use App\Payments;
use \Illuminate\Support\Facades\Validator;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use  App\Http\Controllers\Apis\BaseController as apiBaseController;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Credentials: true');

class PaymentController extends apiBaseController
{
	private $stripe_secret; 

	public function __construct()
	{
		$this->stripe_secret = 'sk_test_qxcWlawAwx2qqqjFvshiHVlB';

	}

	public function addStripeCustomer($customerDetailsAry)
	{
		$stripe = new \Stripe\StripeClient($this->stripe_secret);
		try{
			$customerDetails =  $stripe->customers->create($customerDetailsAry);
			return $customerDetails;

		}
		catch(\Exception $e){
			return [];
		}


		
	}



	public function GenerateStripeCustomer (Request $request) {
		$response = new \stdClass();
		$validator = Validator::make($request->all(), [
			'token' => 'required',
			'id' => 'required'
			
		]);
		if($validator->fails()):
			$response->status = false;
			$response->code = 400;
			$response->message = 'validation failed';
			$response->data = $validator->errors();
			return response()->json($response);

		else:

			try{

				$user_details = User::where('id', $request->id)->first();

				if($user_details->stripe_customer_id == NULL):
					$customerDetailsAry = array(
						'email' => $user_details->email,
						'description' =>  'Customer created named as '.ucfirst($user_details->name).' for payments.',
  					//'source' => 'tok_1HePqIDXWlOwUyJBTCRAZtyY'
						'source' => 'tok_visa'
					);

					$customerResult = $this->addStripeCustomer($customerDetailsAry);

			//CHECK IF CUSTOMER IS CREATED
					if(sizeof($customerResult) == 0 ):

						$response->status = false;
						$response->code = 402;
						$response->message = 'Failed to add customer to stripe';
						$response->data = [];
						return response()->json($response);

					else:
					
					//SAVE CUSTOMER ID						
						$user_details->stripe_customer_id = $customerResult->id;
						$user_details->save();

						$user = User::where('id',$request->id)->first();
						$response->status = true;
						$response->code = 200;
						$response->message = 'Stripe customer ID saved successfully';
						$response->data = $user;
						return response()->json($response);

				endif;

			else:
						$response->status = true;
						$response->code = 200;
						$response->message = 'Stripe customer ID saved successfully';
						$response->data = $user_details;
						return response()->json($response);

			endif;

			}catch (\Exception $e) {

				$exceptions =  $e->getMessage();
				$response->status = false;
				$response->code = 403;
				//$response->message = 'Exceptions encountered';
				$response->message = $exceptions;
				return response()->json($response);
			}
		endif;
	}



	public function chargeAmountFromCard(Request $request)
	{
		$response = new \stdClass();
		$validator = Validator::make($request->all(), [
			//'token' => 'required',
			'amount' => 'required',
			'id' => 'required',
			'trip_id' => 'required',
			'card_id' => 'required',
		]);

		if($validator->fails()):
			$response->status = false;
			$response->code = 400;
			$response->message = 'validation failed';
			$response->data = $validator->errors();
			return response()->json($response);

		else:

			try {

				$user_details = User::where('id', $request->id)->first();

				if($user_details->stripe_customer_id == NULL):

							$response->status = false;
							$response->code = 402;
							$response->message = ' Unable to make payment as customer stripe ID is not generated for payments';
							$response->data = [];
							return response()->json($response);

					//CREATE STRIPE CUSTOMER

					// $customerDetailsAry = array(
					// 	'email' => $user_details->email,
					// 	'description' =>  'Customer created named as '.ucfirst($user_details->name).' for payments.',
  			// 		//'source' => 'tok_1HePqIDXWlOwUyJBTCRAZtyY'
					// 	'source' => 'tok_visa'
					// );

					// $customerResult = $this->addStripeCustomer($customerDetailsAry);

			//CHECK IF CUSTOMER IS CREATED
					// if(sizeof($customerResult) == 0 ):

					// 	$response->status = false;
					// 	$response->code = 402;
					// 	$response->message = 'Failed to add customer to stripe';
					// 	$response->data = [];
					// 	return response()->json($response);

					// else:
					//CHARGE CUSTOMER
					//SAVE CUSTOMER ID						
					// 	$user_details->stripe_customer_id = $customerResult->id;
					// 	$user_details->save();

					// 	$stripe = new \Stripe\StripeClient($this->stripe_secret);

					// 	$cardDetailsAry = array(
					// 		'customer' => $customerResult->id,
					// 		'amount' => round($request->amount, 2) * 100 ,
					// 		'currency' => 'usd',
					// 		'description' => 'Mobile Go Ride on'.' '.date('Y-m-d H:i:s'),
					// 	);
					// 	$result = $stripe->charges->create($cardDetailsAry);

					// 	$stripeResponse =  $result->jsonSerialize();

					// 	if ($stripeResponse['amount_refunded'] == 0 && empty($stripeResponse['failure_code']) && $stripeResponse['paid'] == 1 && $stripeResponse['captured'] == 1 && $stripeResponse['status'] == 'succeeded'): 

					// 		$payment = new Payments();
					// 		$payment->txn_id = $stripeResponse['balance_transaction'];
					// 		$payment->fk_rider_id = $request->id;
					// 		$payment->fk_trip_id = $request->trip_id;
					// 		$payment->payment_platform = 'stripe';
					// 		$payment->created_at = date('Y-m-d H:i:s');
					// 		$payment->save();

					// 		$trips_update = Trip::where('id', $request->trip_id)->first();
					// 		$trips_update->payment_status = 'paid';
					// 		$trips_update->save();

					// 		$response->status = true;
					// 		$response->code = 200;
					// 		$response->message = 'Payment success';
					// 		$response->data = $stripeResponse;
					// 		return response()->json($response);

					// 	else:
					// 		$response->status = false;
					// 		$response->code = 402;
					// 		$response->message = 'Payment failed';
					// 		$response->data = [];
					// 		return response()->json($response);

					// 	endif;

					// endif;
				//STRIPE CUSTOMER ALREADY EXIST
				else:
					

					$stripe = new \Stripe\StripeClient($this->stripe_secret);

					$cardDetailsAry = array(
						'customer' => $user_details->stripe_customer_id,
						'amount' => round($request->amount, 2) * 100 ,
						'currency' => 'usd',
						'description' => 'Mobile Go Ride on'.' '.date('Y-m-d H:i:s'),
						'card' => $request->card_id,
					);
					$result = $stripe->charges->create($cardDetailsAry);

					$stripeResponse =  $result->jsonSerialize();

					if ($stripeResponse['amount_refunded'] == 0 && empty($stripeResponse['failure_code']) && $stripeResponse['paid'] == 1 && $stripeResponse['captured'] == 1 && $stripeResponse['status'] == 'succeeded'): 
						$payment = new Payments();
							$payment->txn_id = $stripeResponse['balance_transaction'];
							$payment->fk_rider_id = $request->id;
							$payment->fk_trip_id = $request->trip_id;
							$payment->payment_platform = 'stripe';
							$payment->created_at = date('Y-m-d H:i:s');
							$payment->save();
							$trips_update = Trip::where('id', $request->trip_id)->first();
							$trips_update->payment_status = 'paid';
							$trips_update->save();

							$driver_details = User::where('id', $trips_update->fk_driver_id)->first();
							if($driver_details == null):
								$driver_details->wallet_balance = $trips_update->fare;
								$driver_details->save();
							else:
								$driver_details->wallet_balance = $driver_details->wallet_balance + $trips_update->fare;
								$driver_details->save();
							endif;

							$this->PaymentCompleteNotification($request->trip_id, $trips_update->id);
						$response->status = true;
						$response->code = 200;
						$response->message = 'Payment success';
						$response->data = $stripeResponse;
						return response()->json($response);

					else:
						$response->status = false;
						$response->code = 402;
						$response->message = 'Payment failed';
						$response->data = [];
						return response()->json($response);

					endif;


				endif;	



			} catch (\Exception $e) {

				$exceptions =  $e->getMessage();
				$response->status = false;
				$response->code = 403;
				//$response->message = 'Exceptions encountered';
				$response->message = $exceptions;
				return response()->json($response);
			}


		endif;

	}




	/*
	* API TO GET CUSTOMER CARDS
	*/

		public function customerStripeCard(Request $request)
		{
			$response = new \stdClass();
		$validator = Validator::make($request->all(), [
			'id' => 'required',
		]);

		if($validator->fails()):
			$response->status = false;
			$response->code = 400;
			$response->message = 'validation failed';
			$response->data = $validator->errors();
			return response()->json($response);

		else:
			$user = User::where('id', $request->id)->first();

			if($user->stripe_customer_id == NULL):
				$response->status = false;
						$response->code = 402;
						$response->message = 'Customer does not exist on stripe ';
						$response->data = [];
						return response()->json($response);
			else:
				try{
						$stripe = new \Stripe\StripeClient($this->stripe_secret);
				$card_details = $stripe->customers->allSources($user->stripe_customer_id,['object' => 'card']);

				//print_r($card_details); die;
				if(sizeof($card_details->data) == 0):
					$response->status = false;
						$response->code = 402;
						$response->message = 'No card found on stripe';
						$response->data = [];
						return response()->json($response);
				else:
					$response->status = true;
						$response->code = 200;
						$response->message = 'Cards fetched successfully';
						$response->data = $card_details->data;
						return response()->json($response);
				endif;

			} catch (\Exception $e) {

				$exceptions =  $e->getMessage();
				$response->status = false;
				$response->code = 403;
				$response->message = $exceptions;
				//$response->data = $exceptions;
				return response()->json($response);
			}

				


			endif;

		endif;

		}



/*
*Api to remove stripe card
*/

public function RemoveStripCard(Request $request)
{
		$response = new \stdClass();
		$validator = Validator::make($request->all(), [
			'id' => 'required',
			'card_id' => 'required',
		]);

		if($validator->fails()):
			$response->status = false;
			$response->code = 400;
			$response->message = 'validation failed';
			$response->data = $validator->errors();
			return response()->json($response);

		else:
			$user = User::where('id', $request->id)->first();

			if($user === null):
				$response->status = false;
						$response->code = 401;
						$response->message = 'Invalid ID';
						return response()->json($response);

					else:

						if($user->stripe_customer_id === null):
							$response->status = false;
							$response->code = 402;
							$response->message = 'Customer does not exist on stripe ';
							$response->data = [];
							return response()->json($response);

						else:
							try{
									$stripe = new \Stripe\StripeClient($this->stripe_secret);

								$cardDetailsAry = array(
									$user_details->stripe_customer_id,
									$request->card_id,
									[]
								);

								$result = $stripe->customers->deleteSource($cardDetailsAry);
								$stripeResponse =  $result->jsonSerialize();
								if($stripeResponse['deleted'] == 1):
									$response->status = true;
									$response->code = 200;
									$response->message = 'Card removed successfully';
									$response->data = $stripeResponse;
									return response()->json($response);
								else:
									$response->status = false;
									$response->code = 401;
									$response->message = 'Something went wrong, unable to remove card';
									return response()->json($response);

								endif;

							}catch (\Exception $e) {

								$exceptions =  $e->getMessage();
								$response->status = false;
								$response->code = 403;
								$response->message = $exceptions;
								//$response->data = $exceptions;
								return response()->json($response);
							}

						endif;


			endif;


		endif;

}



/*Function to send notification on payment complete to driver*/

public function PaymentCompleteNotification($trip_id, $payment_id)
{
	$checkRide = Trip::where('id', $trip_id)->first();
	$payment_details = Payments::where('id', $payment_id)->first();
	$pay_details = ($payment_details === null) ? array(): $payment_details;

	if($checkRide):

			$devices_ = Devices::where('user_id', $checkRide->fk_driver_id)->first();
			$driverDetails = User::where('users.id',  $checkRide->fk_driver_id)
			->leftJoin('car_models', 'users.car_model_id' , '=' , 'car_models.id')
			->first();
			$cab_service = CabService::where('id', $driverDetails->fk_cab_service_id)->first();
			$riderDetails = User::where('id',$checkRide->fk_rider_id)->first();

			$notiArray= array(
				"reciever_id" =>$checkRide->fk_rider_id,
				"sender_id" => 0,
				"title" => 'Ride Payment has been completed.',
				"message" => "Your rider has completed his ride payment.",
				"status" => "sent",
				"sent_on" => date('Y-m-d'),
				"sent_at" =>date('h:i:s a'),
			);

			$msg = array('message'=>$notiArray['message'],
				"start_lat_long" =>[
					"start_latitude" =>$checkRide->destination_from_lat,
					"start_longitude"=>$checkRide->destination_from_lng] ,
					"end_lat_long"=>[
						"latitude"=>  $checkRide->destination_to_lat,
						"longitude"=>$checkRide->destination_to_lng ],
						"driver_details"=> [
							"id"=> $driverDetails->id,
							"name"=> $driverDetails->name,
							"email"=> $driverDetails->email,
							"phone"=> $driverDetails->phone_no,
							"picture"=> ($driverDetails->image_url !== NULL ) ? url('/').'/uploads/drivers/'.$driverDetails->id.'/'.$driverDetails->image_url : "https://cdn-makehimyours.pressidium.com/wp-content/uploads/2016/11/Depositphotos_9830876_l-2015Optimised.jpg",
							"rating" => "4",

							"vehicle_details"=> [
								"vehicle_no" =>($driverDetails->car_number !== null)? $driverDetails->car_number:"",
								"vehicle_model" => ($driverDetails->car_model_name !== null)? $driverDetails->car_model_name:"",
								"latitude"=>($driverDetails->latitude !== null)?  $driverDetails->latitude:"",
								"longitude"=>($driverDetails->longitude !== null)? $driverDetails->longitude:"",
								"rider_space" => $cab_service->rider_space,
								"cab_service" => $cab_service->name,
								"car_img" => "https://pp.netclipart.com/pp/s/6-63856_car-clip-art-png-car-clipart-png.png",
							] 
						],
						"payment_details" =>[$pay_details],
						"rider_details"=>[
					               "rider_id"=> $riderDetails->id,
					               "email" => $riderDetails->email,
					               "name" =>$riderDetails->name,
					               "phone" =>$riderDetails->phone_no,
					               "picture" =>$riderDetails->image_url ? url('/').'/uploads/passengers/'.$request->id.'/'.$riderDetails->image_url :""],
						"fare" => $checkRide->fare,
						'pickup_address' => $checkRide->destination_from,
						'drop_address' => $checkRide->destination_to,
						"status"=>"Sent success",
						'type'=>'payment_completed',
					);

						if($devices_->device_type == "android" && $devices_->device_token !== null){

				$notifymsg=array(
					"title" =>$notiArray['title'],
					'data'=>$msg,
					'to'  =>$devices_->device_token,
					'sent_on'=>$notiArray['sent_on'],
					'sent_at'=>$notiArray['sent_at'],
					'type'=>'completed',
				);

				$this->android_deviceNotification($notifymsg);


    				} //  android noti
    				elseif($devices_->device_type =="ios" && $devices_->device_token !== null){


    					$body['aps'] = array(
    						'alert' => $notiArray['message'],
    						'sound' => 'default',
    						'badge' => 1,
    						'content-available' => 1,
    						"title" =>$notiArray['title'],
    						'data'=>$msg,
    						'type'=>'completed',
    					);
    					$this->ios_deviceNotification($devices_->device_token,$body);


    				} // ios noti

		endif;
}

} //End of payment controller