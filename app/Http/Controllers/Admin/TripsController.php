<?php

namespace App\Http\Controllers\Admin;

use App\Trip;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TripsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $trips = Trip::all();
    //->get();
        $data['trips'] = $trips;
        return view('admin.trips.index')->with($data);
    }

    public function  tripInvoice($id){
        $trips  = Trip::where("id",$id)->with("driver")->with("rider")->first();
        if($trips)
        {
             return view("admin.trips.invoice",compact('trips'));
            //  return $trips;  
        }
        session()->flash("error","Trip not found");
        return back();
    }
}
