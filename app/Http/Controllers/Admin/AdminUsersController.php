<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\User;
use Illuminate\Support\Facades\Hash;
class AdminUsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $users = User::where('user_type','SuperAdministrator')->Orwhere('user_type','DispatcherAdministrator')->get();
        $data['admin_users'] = $users;
        
        return view('admin.admin-users.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) 
        {
            return redirect('admin-users')->with('error',response()->json([
                'type' =>'error',
                'msg' => $validator->getMessageBag ()->toArray(),
            ]));
        
        } 
        else 
        {
            $admin_users_exist = User::where('email',$request->email)->first();
            if($admin_users_exist)
            {
                //email already exist
                return redirect('admin-users')->with('error','Email Already Exist');

            }
            else
            {
                $admin_user = new User();
                $admin_user->name = $request->name;
                $admin_user->email = $request->email;
                $admin_user->password = Hash::make($request->password);//$request->password;
                $request->user_role = preg_replace('/\s+/', '', $request->user_role);
                //dd($request->user_role);
                $admin_user->user_type = $request->user_role;
                
                $admin_user->status = "Active";
                $admin_user->save();
                 return redirect('admin-users')->with('success','User Added successfully!');
            }
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
        ]);
        if ($validator->fails()) 
        {
            return response()->json([
                'type' =>'error',
                'msg' => $validator->getMessageBag ()->toArray(),
            ]); 
        } 
        else 
        {
            
            
                $admin_user = User::where('id', $id)->first();
                $admin_user->name = $request->name;
                $admin_user->email = $request->email;
                //$admin_user->password = $request->password;
                $admin_user->user_type = $request->user_role;
                $admin_user->status = "Active";
                $admin_user->save();
                return redirect('admin-users')->with('success','User Updated successfully!');
                
        }
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
