<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Company;
use App\CarModels;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Hash;
use Session;
use Illuminate\Support\Facades\Redirect;
class CabModelsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }



     public function index()
    {
         $data = CarModels::get();
         return view('admin.cabs-models.index')->with('data',$data);
    }


 public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) 
        {
            return redirect('cabs-model-management')->with('error',response()->json([
                'type' =>'error',
                'msg' => $validator->getMessageBag ()->toArray(),
            ]));
        } else {

        	$cab_model = new CarModels();
        	$cab_model->car_model_name = ucfirst($request->name);
        	$cab_model->created_at = date('Y-m-d H:i:s');
        	$cab_model->save();
        	 return redirect('cabs-model-management')->with('success','Cab Model Added successfully!');


        }


    }




 public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) 
        {
            return response()->json([
                'type' =>'error',
                'msg' => $validator->getMessageBag ()->toArray(),
            ]); 
        } 
        else 
        {

        	 $cab_model  = CarModels::where('id', $id)->first();
                $cab_model->car_model_name = ucfirst($request->name);
                $cab_model->updated_at = date('Y-m-d H:i:s');
                $cab_model->save();
        	 return redirect('cabs-model-management')->with('success','Cab Model Updated successfully!');
        }


}


  public function destroy($id)
    {
         $exist = CarModels::where('id',$id)->first();
        if($exist)
        {
            $exist->delete();
            return redirect('cabs-model-management')->with('success','Cab Model Deleted successfully!');
        }
        else
        {
            return redirect('cabs-model-management')->with('error','Cab Model Not Found!');
        }
    }


} //End of controller