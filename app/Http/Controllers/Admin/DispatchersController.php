<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Trip;
use Illuminate\Support\Facades\Redirect;

class DispatchersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     public function index()
    {
        $drivers = User::where('user_type','driver')
        ->get();
        $trips = Trip::where('is_dispatch_trip',1)->get();
        $data['drivers'] = $drivers;
        $data['trips'] = $trips;
        
        return view('admin.dispatchers.index')->with($data,$drivers,$trips);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pickup_latitude' => 'required',
            'pickup_longitude' => 'required',
            'dropff_latitude' => 'required',
            'dropoff_longitude' => 'required',
            'rider_name' => 'required',
            'rider_tell' => 'required',
            'trip_category' => 'required',
            'select_driver' => 'required',
            'date' => 'required'
        ]);
        if ($validator->fails()) 
        {
            return redirect('dispatcher-panel')->with('error',response()->json([
                'type' =>'error',
                'msg' => $validator->getMessageBag ()->toArray(),
            ]));
        
            // return response()->json([
            //     'type' =>'error',
            //     'msg' => $validator->getMessageBag ()->toArray(),
            // ]); 
        } 
        else 
        {
            $driver = User::where('name',$request->select_driver)->where('user_type','driver')->first();
            //dd($driver->id);
            if($driver)
            {
                $trip = new Trip();
                //$trip->booking_no = $request->booking_no;
                
                $trip->destination_from = $request->pickup_loc;
                $trip->destination_to = $request->rider_dest;
                $trip->destination_from_lat = $request->pickup_latitude;
                $trip->destination_from_lng = $request->pickup_longitude;
                $trip->destination_to_lat = $request->dropff_latitude;
                $trip->destination_to_lng = $request->dropoff_longitude;
                $trip->is_dispatch_trip = true;
                $trip->trip_date = $request->date;
                $trip->dispatch_rider_name = $request->rider_name;
                $trip->dispatch_rider_phone_no = $request->rider_tell;
                
                //$trip->fare = $request->fare;
                //$trip->route_points = $request->route_points;
                $trip->fk_driver_id = $driver->id;
                //$trip->fk_rider_id = $request->fk_rider_id;
                //$trip->fk_company_id = $request->fk_company_id;
                $trip->trip_status = 'pending';
                $trip->trip_category = $request->trip_category;            
                if($request->trip_category === 'instant_trip')
                {
            
                }
                else if($request->trip_category === 'schedule_trip')
                {
                    //$timestamp1 = strtotime($request->trip_time1);
                    //$timestamp2= strtotime($request->trip_time2);
                    
                    
                    $trip->trip_time1 = $request->trip_time_one;//$timestamp1;
                    $trip->trip_time2 = $request->trip_time_two;//$timestamp2;
                                    
                }
                //dd($trip);
                $trip->save();
                return redirect('dispatcher-panel')->with('success','Trip Created successfully!');
            }
            else
            {
                
            }
            //date
        //trip_time_two
        //trip_time_one
            //dd($request->trip_time_two);
            
        }
    }
    
    
}
