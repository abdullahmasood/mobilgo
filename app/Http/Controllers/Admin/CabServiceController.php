<?php

namespace App\Http\Controllers\Admin;

use App\CabService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CabServiceController extends Controller
{
    public function index(){
        $service = CabService::all();
        return  view("admin.cab-service.index",compact('service'));
    }
}
