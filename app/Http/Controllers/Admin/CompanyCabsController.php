<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Company;
use Session;
use Illuminate\Support\Facades\Redirect;
class CompanyCabsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::all();
        $data['companies'] = $companies;
        return view('admin.company-cabs.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'company_name' => 'required',
            'email' => 'required',
            'mobile' => 'required',
            'document_url' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'type' =>'error',
                'msg' => $validator->getMessageBag ()->toArray(),
            ]); 
        } else {
            
            if ($request->hasFile('document_url')) {
            $filenameWithExt = $request->file('document_url')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('document_url')->getClientOriginalExtension();
            $filenameToStore = $filename . '_' . time() . '.' . $extension;
            $path = $request->file('document_url')->move('documents/companies', $filenameToStore);
        	} else {
            $filenameToStore = 'noimage.jpg';
        	}
            $company_exist = Company::where('company_name',$request->company_name)->first();
            if($company_exist)
            {
                return response()->json(['type' =>'Failed', 'msg' => 'Company Already Exist!']);
            }
            else
            {
                $company = new Company();
                $company->company_name = $request->company_name;
                $company->email = $request->email;
                //$driver->image_url = $path;//$filenameToStore;
                $company->mobile = $request->mobile;
                $company->document_url = $filenameToStore;
                $company->status = 'Active';
                $company->save();
                ///return response()->json(['type' =>'success', 'msg' => 'Company Added successfully!']);
                //Session::flash('success', 'Successfully updated bookings !!!');
                return redirect('company-cabs-management')->with('success','Company Added successfully!');
                //return redirect('/dashboard');
            }
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { //
        //return json_encode($request->all());
        $validator = Validator::make($request->all(), [
            'company_name' => 'required',
            'email' => 'required',
            'mobile' => 'required',
            'document_url' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'type' =>'error',
                'msg' => $validator->getMessageBag ()->toArray(),
            ]); 
        } else {
            
            if ($request->hasFile('document_url')) {
            $filenameWithExt = $request->file('document_url')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('document_url')->getClientOriginalExtension();
            $filenameToStore = $filename . '_' . time() . '.' . $extension;
            $path = $request->file('document_url')->move('documents/companies', $filenameToStore);
        	} else {
            $filenameToStore = 'noimage.jpg';
        	}
            $company_exist = Company::where('company_name',$request->company_name)->first();
            if($company_exist)
            {
                if($company_exist->id != $request->id){
                    return response()->json(['type' =>'Failed', 'msg' => 'Company Already Exist!']);
                }
            }
            
                $company = Company::find($request->id);
                $company->company_name = $request->company_name;
                $company->email = $request->email;
                //$driver->image_url = $path;//$filenameToStore;
                $company->mobile = $request->mobile;
                $company->document_url = $filenameToStore;
                $company->status = 'Active';
                $company->save();
                ///return response()->json(['type' =>'success', 'msg' => 'Company Added successfully!']);
                //Session::flash('success', 'Successfully updated bookings !!!');
                return redirect('company-cabs-management')->with('success','Company Added successfully!');
                //return redirect('/dashboard');
            
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $exist = Company::where('id',$id)->first();
        if($exist)
        {
            $exist->delete();
            return redirect('company-cabs-management')->with('success','Comapny-Cabs Deleted successfully!');
        }
        else
        {
            return redirect('company-cabs-management')->with('error','Comapny-Cabs Not Found!');
        }
    }
}
