<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Redirect;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    public function login(Request $request){
       // dd($request->email);
        //Dispatcher Administrator
        if (\Auth::attempt(['email' => $request->email, 'password' => $request->password, 'user_type' => 'SuperAdministrator'])) {
            $email = \Auth::user()->email;
            return redirect('/dashboard');
        }
        else if (\Auth::attempt(['email' => $request->email, 'password' => $request->password, 'user_type' => 'DispatcherAdministrator'])) {
            $email = \Auth::user()->email;
            return redirect('/dashboard');
        }
        else{
            return Redirect::back();//->withInput()->withErrors(['Email or password invalid.']);
        }
    }

    public function logout(Request $request) {
        \Auth::logout();
        return redirect('/login');
    }
    
}
