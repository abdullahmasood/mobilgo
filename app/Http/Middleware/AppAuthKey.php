<?php

namespace App\Http\Middleware;

use Closure;
use Crypt;
class AppAuthKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('APP_KEY');
        $actual_token = 'MobileGoEncyptionKey@BBIF322#4@3234637867663352';
       
        if (empty($token)) {
          return response()->json(['status' => false ,'message' => "App key not found."],401);
        }
        else{
            $decrypt_token  = Crypt::decrypt($token);
            $comparing = strcmp($decrypt_token, $actual_token);
            if ($comparing !== 0) {
                return response()->json(['status' => false , 'message' => "Invalid App key."],400);
            }
            else{
                 return $next($request);

            }

        }
    }
}
