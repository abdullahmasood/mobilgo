<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
   public function rider()
    {
        return $this->belongsTo('App\User','fk_rider_id','id');
    }

    public function trip()
    {
        return $this->belongsTo('App\Trip','fk_trip_id','id');
    }
}
