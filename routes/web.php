<?php
use App\User;
use App\Trip;
use App\Company;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// $a = Crypt::encrypt('MobileGoEncyptionKey@BBIF322#4@3234637867663352');

// echo $a; die;
Auth::routes();

Route::get('/', function () {
    return view('welcome');
    // echo "afa";
});
// Route::get('/login', function () {
//    // return view('login');
//     echo "ok";
// });
Route::get('/dashboard', function () 
{
    //$user = Session::get('user');
    $companies = Company::all();
    $drivers = User::where('user_type','driver')//->with(['company' => function ($query) {$query->select(['company_name']);}])
    ->get();
    $completed = Trip::where('trip_status','completed')//->with(['company' => function ($query) {$query->select(['company_name']);}])
    ->count();
    $cancelled = Trip::where('trip_status','cancelled')//->with(['company' => function ($query) {$query->select(['company_name']);}])
    ->count();

    $active = User::where('user_type','driver')->where("status","Active")//->with(['company' => function ($query) {$query->select(['company_name']);}])
    ->count();
    $Inactive = User::where('user_type','driver')->where("status","InActive")//->with(['company' => function ($query) {$query->select(['company_name']);}])
    ->count();

  
     $trips = Trip::all();
    //->get();
        $data['trips'] = $trips;
     $data['companies'] = $companies;
    $data['drivers'] = $drivers;
    $data['completed'] = $completed;
    $data['active'] = $active;
    $data['Inactive'] = $Inactive;
    $data['cancelled'] = $cancelled;
    return view('dashboard.index')->with($data,$drivers);
})->middleware('auth');

Route::resource('admin-users', 'Admin\AdminUsersController')->middleware('auth');
Route::resource('drivers-management', 'Admin\DriversController')->middleware('auth');
Route::resource('cabs-management', 'Admin\CabsController')->middleware('auth');
Route::resource('riders-management', 'Admin\RidersController')->middleware('auth');
Route::get('trip-history-management', 'Admin\TripsController@index')->name('trip-history-management')->middleware('auth');
Route::get('reviews-management', 'Admin\ReviewsController@index')->name('reviews-management')->middleware('auth');
Route::resource('promos-management', 'Admin\PromosController')->middleware('auth'); 
Route::get('accountant-panel', 'Admin\AccountantsController@index')->name('accountant-panel')->middleware('auth');
Route::get('dispatcher-panel', 'Admin\DispatchersController@index')->name('dispatcher-panel')->middleware('auth');
Route::post('store-dispatch', 'Admin\DispatchersController@store')->name('store-dispatch')->middleware('auth');
Route::resource('company-cabs-management', 'Admin\CompanyCabsController')->middleware('auth'); 
Route::get('payments-management', 'Admin\PaymentsController@index')->name('payments-management')->middleware('auth');
Route::get('heat-map', 'Admin\HeatMapsController@index')->name('heat-map')->middleware('auth');
Route::resource('cabs-model-management', 'Admin\CabModelsController')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::group(['middleware' => 'auth'], function() {
Route::get('/add-payment', function () {
    return view('payments.add-payment');
});
Route::post('add_payment_method', 'Admin\PaymentController@add_payment_method')->name('payment.method');

Route::post('make_payment', 'Admin\PaymentController@make_payment')->name('payment.make');

Route::get('payment-list/{trip_id}/{amount}', 'Admin\PaymentController@index')->name('payment.list');

// route for processing payment
Route::post('paypal', 'Admin\PaymentController@payWithpaypal');

// route for check status of the payment
Route::get('status', 'Admin\PaymentController@getPaymentStatus');

Route::post('order-request', 'Admin\PaymentController@save_order_request')->name('save.order');


// Trip Controller


Route::get('trip-invoice/{id}', 'Admin\TripsController@tripInvoice');


// Cab Service Controller 
Route::get('cab-service', 'Admin\CabServiceController@index');


// Rider Controller
Route::get('rider-history/{id}', 'Admin\RidersController@history');



});