<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/driver', 'ApiController@login_driver');

Route::post('/rider', 'ApiController@login_rider');

Route::post('/register', 'ApiController@register');

Route::post('/trip', 'ApiController@addNewTrip');

Route::get('/recent_trips/{id}', 'ApiController@getRecentTrips');
Route::get('/driver/{id}', 'ApiController@getDriver');

Route::get('/testSort', 'ApiController@testSort');

Route::post('/changeTripStatus','ApiController@changeTripStatus');
Route::get('/driverTripHistory/{id}','ApiController@getDriverTripHistory');

Route::post('/getDistance','ApiController@getDistance');
Route::post('/updateDriverTripLocation','ApiController@updateDriverTripLocation');
Route::get('/verify-users', 'ApiController@users');//dd
Route::post('/updateDriverLocation','ApiController@updateDriverLocation');

Route::post('/updateFcmToken','ApiController@updateFcmToken');
Route::post('/finishTrip','ApiController@finishTrip');
Route::post('/getDriverEarning','ApiController@getDriverEarning');
Route::get('/getScheduleTripHistory/{id}','ApiController@getScheduleTripHistory');
Route::get('/getDriverCompletedTripHistory/{id}','ApiController@getDriverCompletedTripHistory');
Route::get('/getNotification/{id}','ApiController@getNotification');
Route::post('/getCabServices','ApiController@getCabServicesForCustomer');
Route::get('/getCabServicesForDriver','ApiController@getCabServicesForDriver');



//Common routes


Route::group([ 'middleware' => ['AppAuthKey'],'prefix' => 'v1'], function () { 
    //All the routes are placed in here
    Route::post('/createAccount', 'Apis\CommonApisController@get_register');
    Route::post('/common_logout','Apis\CommonApisController@commonLogout');
    Route::post('/update_profile_picture','Apis\CommonApisController@updateProfilePicture');
    Route::post('/profile_details','Apis\CommonApisController@getProfileDetails');
    Route::get('/car_models','Apis\CommonApisController@getCarModelsList');
    Route::get('/cab_services_list','Apis\CommonApisController@cabServices');
    Route::post('/add_ratings','Apis\CommonApisController@AddRatingReviews');
    Route::post('/upcoming_rides_list','Apis\CommonApisController@UpcomingRides');
    
    
});

//End common routes

//Passengers routes

Route::group([ 'middleware' => ['AppAuthKey'],'prefix' => 'v1/passenger'], function () { 
    //All the routes are placed in here
    Route::post('/passenger_login', 'Apis\Passengers\PassengersApisController@passengerLogin');
    Route::post('/passenger_social_login', 'Apis\Passengers\PassengersApisController@passengerSocialLogin');
     Route::post('/find_ride', 'Apis\Passengers\PassengersApisController@requestRide');
     Route::post('/ride_estimation', 'Apis\Passengers\PassengersApisController@getRideEstimate');
     Route::post('/reset_ride_request', 'Apis\CommonApisController@resetRideRequest');
     Route::post('/rider_trips_history', 'Apis\Passengers\PassengersApisController@riderTripsHistory');
       Route::get('/promos_list', 'Apis\Passengers\PassengersApisController@getPromos');
       Route::post('/pay_using_stripe', 'Apis\PaymentController@chargeAmountFromCard');
       Route::post('/fetch_stripe_cards', 'Apis\PaymentController@customerStripeCard');
        Route::post('/stripe_generate_customer', 'Apis\PaymentController@GenerateStripeCustomer');
        Route::post('/remove_stripe_card', 'Apis\PaymentController@RemoveStripCard');
     Route::post('/cancel_ride', 'Apis\Passengers\PassengersApisController@CancelRideByRider');








});

//End of passengers routes

//Driver routes

Route::group([ 'middleware' => ['AppAuthKey'],'prefix' => 'v1/driver'], function () { 
    //All the routes are placed in here
    Route::post('/driver_login', 'Apis\Drivers\DriversApisController@driverLogin');
    Route::post('/ride_request_action', 'Apis\Drivers\DriversApisController@actionRideRequest');
    Route::post('/start_ride', 'Apis\Drivers\DriversApisController@startRide');
    Route::post('/end_ride', 'Apis\Drivers\DriversApisController@completeRide');
    Route::post('/driver_trips_history', 'Apis\Drivers\DriversApisController@driverTripsHistory');
    Route::post('/earning_details', 'Apis\Drivers\DriversApisController@EarningDetails');
    Route::post('/analytics_completed_trips', 'Apis\Drivers\EarningController@CompletedTripsAnalytics');
    Route::post('/analytics_canceled_trips', 'Apis\Drivers\EarningController@CancelTripsAnalytics');
    Route::post('/total_completed_trips', 'Apis\Drivers\EarningController@totalCompletedTrips');





});


//CRON routes

    Route::get('/reminder_cron', 'Apis\CronController@RideReminderCron');

