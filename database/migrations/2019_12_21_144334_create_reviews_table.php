<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('rider_rating');
            $table->text('comment');

            $table->unsignedBigInteger('fk_driver_id')->nullable();
            $table->unsignedBigInteger('fk_rider_id')->nullable();
            $table->unsignedBigInteger('fk_trip_id')->nullable();

            $table->foreign('fk_driver_id')
            ->references('id')
            ->on('users');
            $table->foreign('fk_rider_id')
            ->references('id')
            ->on('users');
            $table->foreign('fk_trip_id')
            ->references('id')
            ->on('trips');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
