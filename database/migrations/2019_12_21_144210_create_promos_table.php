<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('promo_code');
            $table->enum('user_type',['All Users'])->nullable();
            $table->enum('discount_type',['flat'])->nullable();
            $table->string('expiry_date')->nullable();
            $table->enum('status',['enable','disable'])->nullable();
            $table->integer('usage_count')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promos');
    }
}
