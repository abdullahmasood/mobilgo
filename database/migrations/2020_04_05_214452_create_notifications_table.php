<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('body')->nullable();
            $table->tinyInteger('read_receipt')->nullable();
            $table->tinyInteger('read_receipt_sender')->nullable();
            $table->tinyInteger('read_receipt_receiver')->nullable();
            $table->unsignedBigInteger('fk_sender_id')->nullable();
            $table->unsignedBigInteger('fk_receiver_id')->nullable();
            
            // $table->foreign('fk_sender_id')
            // ->references('id')
            // ->on('users');
            // $table->foreign('fk_receiver_id')
            // ->references('id')
            // ->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
