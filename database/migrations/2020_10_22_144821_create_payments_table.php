<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id'); 
            $table->string('txn_id');
            $table->unsignedBigInteger('fk_user_id')->nullable();
            $table->unsignedBigInteger('fk_trip_id')->nullable();
            $table->enum('payment_platform',['paypal','stripe']);
            $table->timestamps();
             $table->foreign('fk_trip_id')
            ->references('id')
            ->on('trips')
            ->onDelete('cascade');
              $table->foreign('fk_user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
