<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('booking_no');
            $table->string('destination_from')->nullable();
            $table->string('destination_to')->nullable();
            $table->string('destination_from_lat')->nullable();
            $table->string('destination_from_lng')->nullable();
            $table->string('destination_to_lat')->nullable();
            $table->string('destination_to_lng')->nullable();
            $table->string('trip_date')->nullable();
            $table->timeTz('trip_started_at')->nullable();
            $table->timeTz('trip_ended_at')->nullable();
            $table->string('trip_distance')->default(0);
            $table->float('fare')->nullable();
            $table->text('invoice_url')->nullable();
            //route info
           // $table->text('route_points')->nullable();
            $table->string('travel_minutes')->nullable();

            $table->enum('trip_status',['requested','accepted','rejected','cancelled','completed','inprogress']);
            $table->enum('trip_category',['instant_trip','schedule_trip'])->default('instant_trip');
            $table->unsignedBigInteger('fk_driver_id')->nullable();
            $table->unsignedBigInteger('fk_rider_id')->nullable();
            $table->unsignedBigInteger('fk_company_id')->nullable();
            $table->unsignedBigInteger('fk_promo_id')->nullable();
            $table->bigInteger('fk_service_type_id')->nullable();
            $table->foreign('fk_driver_id')
            ->references('id')
            ->on('users');
            $table->foreign('fk_rider_id')
            ->references('id')
            ->on('users');
            $table->foreign('fk_company_id')
            ->references('id')
            ->on('companies');
              $table->foreign('fk_promo_id')
            ->references('id')
            ->on('promos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
