<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->text('image_url')->nullable();
            $table->string('phone_no')->nullable();
            $table->float('wallet_balance')->nullable();
            $table->enum('status',['Active','InActive']);
            $table->string('user_type')->nullable();//driver,rider
            $table->string('admin_type')->nullable();//super administrator,dispatcher administrator
            //
            $table->decimal('latitude',5,4)->nullable();
            $table->decimal('longitude',5,4)->nullable();
            $table->string('fcm_token')->nullable();
            $table->rememberToken();
            $table->unsignedBigInteger('fk_company_id')->nullable();
            $table->unsignedBigInteger('fk_cab_service_id')->nullable();
            $table->timestamps();
            
            $table->foreign('fk_cab_service_id')
            ->references('id')
            ->on('cab_services')
            ->onDelete('cascade');
            $table->foreign('fk_company_id')
            ->references('id')
            ->on('companies')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
