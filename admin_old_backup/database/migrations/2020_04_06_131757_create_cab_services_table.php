<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCabServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cab_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('service_description');
            $table->string('rider_space');
            $table->text('image_url')->nullable();
            $table->float('price_per_kilometer')->default(0.0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cab_services');
    }
}
