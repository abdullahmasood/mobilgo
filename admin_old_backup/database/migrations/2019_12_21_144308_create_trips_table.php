<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('booking_no');
            $table->text('destination_from')->nullable();
            $table->text('destination_to')->nullable();
            $table->double('destination_from_lat',6,6)->nullable();
            $table->double('destination_from_lng',6,6)->nullable();
            $table->double('destination_to_lat',6,6)->nullable();
            $table->text('destination_to_lng',6,6)->nullable();
            $table->string('trip_date')->nullable();
            $table->double('trip_time1')->nullable();
            $table->double('trip_time2')->nullable();
            $table->double('trip_distance')->default(0);
            $table->float('fare')->nullable();
            $table->text('invoice_url')->nullable();
            //route info
            $table->text('route_points')->nullable();
            $table->string('travel_minutes')->nullable();

            $table->enum('trip_status',['pending','accepted','rejected','cancelled','completed','onway','reached','withdrawn']);
            $table->enum('trip_category',['instant_trip','schedule_trip'])->default('instant_trip');
            $table->unsignedBigInteger('fk_driver_id')->nullable();
            $table->unsignedBigInteger('fk_rider_id')->nullable();
            $table->unsignedBigInteger('fk_company_id')->nullable();
            $table->bigInteger('fk_service_type_id')->nullable();
            $table->foreign('fk_driver_id')
            ->references('id')
            ->on('users');
            $table->foreign('fk_rider_id')
            ->references('id')
            ->on('users');
            $table->foreign('fk_company_id')
            ->references('id')
            ->on('companies');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
