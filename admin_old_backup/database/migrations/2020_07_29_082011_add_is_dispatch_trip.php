<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsDispatchTrip extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trips', function (Blueprint $table) {
            // $table->tinyInteger('is_dispatch_trip',20)->after('fk_service_type_id')->unsigned()->nullable();

            $table->tinyInteger('is_dispatch_trip')->after('fk_service_type_id')->nullable();   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trips', function (Blueprint $table) {
            $table->tinyInteger('is_dispatch_trip')->after('fk_service_type_id')->nullable();
        });
    }
}
