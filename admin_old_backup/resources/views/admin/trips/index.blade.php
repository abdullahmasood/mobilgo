@extends('admin-layout.content')


@section('content')

	<div class="row">
		 <div class="col-lg-12">
            <div class="ibox ">

		 		<div class="ibox-content">

			 		<div class="table-responsive mt-3">
			 		
				 			<div class="col-12">
				 				<h2 class="font-weight-bold float-left">Trip History Mangement</h2>
				 			</div>
				 			<div class="col-12 ">
				 				
					 			<button class="btn btn-default float-right mr-5">Export</button>
					 			
				 			</div>
			 			
			 			<div class="clearfix"></div>
			 			<table class="table table-striped  table-hover dataTables-admin" >
			 				<thead>
			 					<tr>
			 						<th>Trip ID</th>
			 						<th>Booking No</th>
			 						<th>Driver</th>
			 						<th>Rider</th>
			 						<th>Destination (From)</th>
			 						<th>Destination (To)</th>
			 						<th>Trip Date</th>
			 						<th>Company</th>
			 						<th>Fare</th>
			 						<th>Invoice</th>
			 					</tr>
			 				</thead>
			 				<tbody>
			 				    @foreach ($trips as $trip)
			 					<tr class="gradeX">
			 						<td class="center">{{$trip->id}}</td>
			 						<td class="center">{{$trip->booking_no}}</td>
			 						<td class="center">{{$trip->driver->name}}</td>
			 						<td class="center">
			 						    @if(is_null($trip->rider))
                                            {{ '---' }} 
                                            @else
                                           {{ $trip->rider->name }}
                                        @endif
			 						<td class="center">{{$trip->destination_from}}</td>
			 						<td class="center">{{$trip->destination_to}}</td>
			 						<td class="center">{{$trip->trip_date}}</td>
			 						<td class="center">
			 						    @if(is_null($trip->company))
                                            {{ '---' }} 
                                            @else
                                           {{ $trip->company->company_name }}
                                        @endif
			 						    </td>
			 						<td class="center">{{$trip->fare}}</td>
			 						<td class="center"><span class="label label-success">View Invoice</span></td>
			 					</tr>
			 					@endforeach

			 				</tbody>
			 			</table>
			 			
			 			
			 		</div>
		 		</div>
		 	</div>
		</div>
	</div>

 @endsection
 @section('script-dashboard')

 <script>
        $(document).ready(function(){
            $('.dataTables-admin').DataTable({
            	"lengthChange": false,
            	"searching": false,
            	"bInfo" : false,
            	"bSort" : false,

                responsive: true,
               
				language: {
					paginate: {
						next: '<i class="fas fa-angle-right fa-lg"></i>',
						previous: '<i class="fas fa-angle-left fa-lg"></i>'  
					}
				}

            });

        });

    </script>
 @stop