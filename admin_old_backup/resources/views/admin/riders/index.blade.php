@extends('admin-layout.content')


@section('content')

	<div class="row">
		 <div class="col-lg-12">
            <div class="ibox ">

		 		<div class="ibox-content">

			 		<div class="table-responsive mt-3">
			 		
				 			<div class="col-12">
				 				<h2 class="font-weight-bold float-left">Rider Mangement</h2>
				 			</div>
				 			<div class="col-12 ">
				 				
					 			<button class="btn btn-default float-right mr-5">Export</button>
					 			
				 			</div>
			 			
			 			<div class="clearfix"></div>
			 			<table class="table table-striped  table-hover dataTables-admin" >
			 				<thead>
			 					<tr>
			 						<th>Name</th>
			 						<th>Email</th>
			 						<th>Phone Number</th>
			 						<th>Sign up date</th>
			 						<th>Money Spent</th>
			 						<th>Trip count</th>
			 						<th>History</th>
			 						<th>Action</th>
			 					</tr>
			 				</thead>
			 				<tbody>
			 					 @foreach ($riders as $rider)
			 					<tr class="gradeX">
			 						<td class="center">{{$rider->name}}</td>
			 						<td class="center">{{$rider->email}}</td>
			 						<td class="center">{{$rider->phone_no}}</td>
			 						<td class="center">{{$rider->created_at}}</td>
			 						<td class="center">$0</td>
			 						<td class="center">0</td>
			 						<td class="center"><i class="fas fa-file-alt"></i></td>
			 						<td class="center"><i class="fa fa-times"></i></td>
			 					</tr>
			 					@endforeach
			 				</tbody>
			 			</table>
			 			
			 			
			 		</div>
		 		</div>
		 	</div>
		</div>
	</div>

 @endsection
 @section('script-dashboard')

 <script>
        $(document).ready(function(){
            $('.dataTables-admin').DataTable({
            	"lengthChange": false,
            	"searching": false,
            	"bInfo" : false,
            	"bSort" : false,

                responsive: true,
               
				language: {
					paginate: {
						next: '<i class="fas fa-angle-right fa-lg"></i>',
						previous: '<i class="fas fa-angle-left fa-lg"></i>'  
					}
				}

            });

        });

    </script>
 @stop