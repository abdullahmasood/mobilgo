@extends('admin-layout.content')


@section('content')

	<div class="row">
		 <div class="col-lg-12">
            <div class="ibox ">

		 		<div class="ibox-content">

			 		<div class="table-responsive mt-3">
			 		
				 			<div class="col-12">
				 				<h2 class="font-weight-bold float-left">Admin supervision</h2>
				 			</div>
				 			<div>
                                @include('../../admin-layout/flash-message')
							</div>
				 			<div class="col-12 ">
				 				<button data-toggle="modal" 
   									data-target="#adminModal" class="btn btn-success float-right">Add Admin</button>
					 			<button class="btn btn-default float-right mr-5">Export</button>
					 			
				 			</div>
			 			
			 			<div class="clearfix"></div>
			 			<table class="table table-striped  table-hover dataTables-admin" >
			 				<thead>
			 					<tr>
			 						<th>ID</th>
			 						<th>Admin name</th>
			 						<th>Email</th>
			 						<th>Roles</th>
			 						<th>Status</th>
			 						<th></th>
			 						<th></th>
			 					</tr>
			 				</thead>
			 				<tbody>
			 				    @foreach ($admin_users as $admin_user)
			 					<tr class="gradeX">
			 						<td class="center">{{$admin_user->id}}</td>
			 						<td class="center">{{$admin_user->name}}</td>
			 						<td class="center">{{$admin_user->email}}</td>
			 						<td class="center">{{$admin_user->user_type}}</td>
			 						<td class="center"><span class="label label-primary">{{$admin_user->status}}</span></td>
			 						<td class="center"><i class="fas fa-pen" data-toggle="modal" data-target="#adminEditModal" onClick="return userInfo({{$admin_user}})" ></i></td>
			 						<td class="center"><i class="fa fa-times"></i></td>
			 					</tr>
			 				</tbody>
			 				    @endforeach
			 			</table>
			 			
			 			
			 		</div>
		 		</div>
		 	</div>
		</div>
	</div>


	<div class="modal fade" id="adminModal" 
			tabindex="-1" role="dialog" 
			aria-labelledby="adminModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" 
				id="adminModalLabel">Add new Admin</h4>
			</div>
			<div class="modal-body">
										<div class="col-md-12">

							<!--begin::Portlet-->
							<div class="kt-portlet">
														
								<!--begin::Form-->
								<form  action="{{route('admin-users.store')}}" method="post" enctype="multipart/form-data" class="kt-form ajax-form" data-cb="job_category_add"><!-- Form row -->
									{{csrf_field()}}
									<div class="kt-portlet__body">
										<div class="form-group form-inline">
											<label class="col-md-3 col-form-label">Name:</label>
											<input type="text" class="col-md-7 form-control" placeholder="Enter full name" name="name">
										</div>
										<div class="form-group form-inline">
											<label class="col-md-3 col-form-label" for="sel1">Select Role:</label>
											<select name="user_role" class="col-md-7 form-control" id="sel1">
												<option>Super Administrator</option>
												<option>Dispatcher Administrator</option>
											</select>
										</div>
										<div class="form-group form-inline">
											<label class="col-md-3 col-form-label">Email:</label>
											<input type="email" class="col-md-7 form-control" placeholder="Enter Email Address" name="email">
										</div>
										<div class="form-group form-inline">
											<label class="col-md-3 col-form-label">Password:</label>
											<input type="password" class="col-md-7 form-control" placeholder="Enter Password" name="password">
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions text-center">
											<input type="submit" value="Save" class="btn btn-primary"/> 
											<button type="button" 
										class="btn btn-default" 
										data-dismiss="modal">Close</button>
										</div>
									</div>
								</form>

								<!--end::Form-->
							</div>
							</div>

			</div>
			<!-- <div class="modal-footer">
				<button type="button" 
				class="btn btn-default" 
				data-dismiss="modal">Close</button>
				<span class="pull-right">
				<button type="button" class="btn btn-primary">
					Save
				</button>
				</span>
			</div> -->
			</div>
		</div>
		</div>


<!--Edit-->
		<div class="modal fade" id="adminEditModal" 
			tabindex="-1" role="dialog" 
			aria-labelledby="adminEditModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" 
				id="adminEditModalLabel">Edit Admin User</h4>
			</div>
			<div class="modal-body">
										<div class="col-md-12">

							<!--begin::Portlet-->
							<div class="kt-portlet">
								<!--begin::Form-->
								<form id="update_form"  method="POST" enctype="multipart/form-data" class="kt-form ajax-form" data-cb="job_category_add"><!-- Form row -->
									{{csrf_field()}}
									{{ method_field('PUT') }}
									<input type="hidden" name="id" id="id">
    								<!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->
									<div class="kt-portlet__body">
										<div class="form-group">
											<label>Name:</label>
											<input type="text" class="form-control" placeholder="Enter full name" name="name" id="name">
										</div>
										<div class="form-group">
											<label for="user_role">Select Role:</label>
											
											<select name="user_role" class="form-control" id="user_role">
												<!-- <option>Logixcess</option>
												<option>NetSol</option>
												<option>Systems Limited</option>
												<option>ArbiSoft</option> -->
												<option>Super Administrator</option>
												<option>Dispatcher Administrator</option>
											
												
											</select>
										</div>
										<div class="form-group">
											<label>Email:</label>
											<input type="email" class="form-control" placeholder="Enter Email Address" name="email" id="email">
										</div>
										<div class="form-group">
											<label>Password:</label>
											<input type="password" class="form-control" placeholder="Enter Password" name="password" id="password">
										</div>
									
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<input type="submit" value="Update" class="btn btn-primary"/> 
											<button type="button" 
										class="btn btn-default" 
										data-dismiss="modal">Close</button>
										</div>
									</div>
								</form>

								<!--end::Form-->
							</div>
							</div>

			</div>
			<!-- <div class="modal-footer">
				<button type="button" 
				class="btn btn-default" 
				data-dismiss="modal">Close</button>
				<span class="pull-right">
				<button type="button" class="btn btn-primary">
					Save
				</button>
				</span>
			</div> -->
			</div>
		</div>
		</div>




 @endsection
 @section('script-dashboard')

 <script>
        $(document).ready(function(){
            $('.dataTables-admin').DataTable({
            	"lengthChange": false,
            	"searching": false,
            	"bInfo" : false,
            	"bSort" : false,

                responsive: true,
               
				language: {
					paginate: {
						next: '<i class="fas fa-angle-right fa-lg"></i>',
						previous: '<i class="fas fa-angle-left fa-lg"></i>'  
					}
				}

            });

        });
        
        
        function userInfo(data){
          $('.modal-title').html('Edit Admin User');
          $("#name").val(data.name);
          $("#email").val(data.email);
          $("#password").val(data.password);
		  $('#user_role').val(data.user_type);
		  $('#id').val(data.id);
		  
		  var url = '{{ route("admin-users.update", ":slug") }}';
		  url = url.replace(':slug', data.id);
		  $('#update_form').attr('action', url);
		  
		  
		  $('#adminEditModal').modal('show');
      }

    </script>
 @stop