@extends('admin-layout.content')


@section('content')

	<div class="row">
		 <div class="col-lg-12">
            <div class="ibox ">

		 		<div class="ibox-content">

			 		<div class="table-responsive mt-3">
			 		
				 			<div class="col-12">
				 				<h2 class="font-weight-bold float-left">Cab Mangement</h2>
				 			</div>
				 			<div class="col-12 ">
				 				
					 			<button class="btn btn-default float-right mr-5">Export</button>
					 			
				 			</div>
			 			
			 			<div class="clearfix"></div>
			 			<table class="table table-striped  table-hover dataTables-admin" >
			 				<thead>
			 					<tr>
			 						<th>Cab Type</th>
			 						<th>Company Name</th>
			 						<th>Driver</th>
			 						<th>Document</th>
			 						<th>Status</th>
			 						<th></th>
			 						<th></th>
			 					</tr>
			 				</thead>
			 				<tbody>
			 					<tr class="gradeX">
			 						<td class="center">345</td>
			 						<td class="center">John Doe</td>
			 						<td class="center">johndoe@gmail.com</td>
			 						<td class="center">Super Administrator</td>
			 						<td class="center"><span class="label label-primary">Active</span></td>
			 						<td class="center"><i class="fas fa-pen"></i></td>
			 						<td class="center"><i class="fa fa-times"></i></td>
			 					</tr>
			 				</tbody>
			 			</table>
			 			
			 			
			 		</div>
		 		</div>
		 	</div>
		</div>
	</div>

 @endsection
 @section('script-dashboard')

 <script>
        $(document).ready(function(){
            $('.dataTables-admin').DataTable({
            	"lengthChange": false,
            	"searching": false,
            	"bInfo" : false,
            	"bSort" : false,

                responsive: true,
               
				language: {
					paginate: {
						next: '<i class="fas fa-angle-right fa-lg"></i>',
						previous: '<i class="fas fa-angle-left fa-lg"></i>'  
					}
				}

            });

        });

    </script>
 @stop