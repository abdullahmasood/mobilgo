@extends('admin-layout.content')


@section('content')

	<div class="row">
		 <div class="col-lg-12">
            <div class="ibox ">

		 		<div class="ibox-content">

			 		<div class="table-responsive mt-3">
			 		
				 			<div class="col-12">
				 				<h2 class="font-weight-bold float-left">Promo Mangement</h2>
				 			</div>
							 <div>
                                @include('../../admin-layout/flash-message')
							</div>
				 			<div class="col-12 ">
				 				<button data-toggle="modal" 
   									data-target="#promosModal" class="btn btn-success float-right">Create new promo code</button>
					 			<button class="btn btn-default float-right mr-5">Export</button>
					 			
				 			</div>
			 			
			 			<div class="clearfix"></div>
			 			<table class="table table-striped  table-hover dataTables-admin" >
			 				<thead>
			 					<tr>
			 						<th>#</th>
			 						<th>Promo Code</th>
			 						<th>User Type</th>
			 						<th>Discount Type</th>
			 						<th>Usage Count</th>
			 						<th>Experiation</th>
			 						<th>Status</th>
			 						<th>Action</th>
			 					
			 					</tr>
			 				</thead>
			 				<tbody>
							@foreach ($promos as $promo)
			 					<tr class="gradeX">
			 						<td class="center">{{$promo->id}}</td>
			 						<td class="center">{{$promo->promo_code}}</td>
			 						<td class="center">{{$promo->user_type}}</td>
			 						<td class="center">{{$promo->discount_type}}</td>
			 						<td class="center">{{$promo->usage_count}}</td>
			 						<td class="center">{{$promo->expiry_date}}</td>
			 						<td><span class="label label-primary">Enable</span>
			 							<span class="label">Disable</span>
			 						</td>
			 						<td><i class="fas fa-pen" data-toggle="modal" data-target="#promosEditModal" onClick="return promoInfo({{$promo}})"></i>
			 							<i class="fa fa-times" data-toggle="modal" data-target="#deleteModal" onClick="return deletePromo({{$promo->id}})"></i>
			 						</td>
			 					</tr>
							@endforeach
			 				</tbody>
			 			</table>
			 			
			 			
			 		</div>
		 		</div>
		 	</div>
		</div>
	</div>


	<div class="modal fade" id="promosModal" 
			tabindex="-1" role="dialog" 
			aria-labelledby="promosModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header text-center">
				<h4 class="modal-title text-center justify-content-center" 
				id="promosModalLabel">Add new Promo Code</h4>
			</div>
			<div class="modal-body">
										<div class="col-md-12">

							<!--begin::Portlet-->
							<div class="kt-portlet">
														
								<!--begin::Form-->
								<form  action="{{route('promos-management.store')}}" method="post" enctype="multipart/form-data" class="kt-form ajax-form" data-cb="job_category_add"><!-- Form row -->
									{{csrf_field()}}
									<div class="kt-portlet__body">
										<div class="form-group form-inline">
											<label class="col-md-4 col-form-label">PromoCode:</label>
											<input type="text" class="col-md-7 form-control" placeholder="Enter promo code" name="promo_code">
										</div>
										<div class="form-group form-inline">
											<label class="col-md-4 col-form-label" for="sel1">Discount Type</label>
											<select class="col-md-7 form-control" name="discount_type" id="sel1">
												<option>Full</option>
												<option>Half</option>
												<option>20%</option>
												<option>10%</option>
											</select>
										</div>
										<div class="form-group form-inline">
											<label class="col-md-4 col-form-label" for="sel2">User Type</label>
											<select class="col-md-7 form-control" name="user_type" id="sel2">
												<option>Super</option>
												<option>Regular</option>
												<option>Normal</option>
											</select>
										</div>
										<!-- <div class="form-group form-inline">
											<label class="col-md-4 col-form-label">Starting at</label>
											<input type="text" class="col-md-7 form-control" placeholder="Enter starting price" name="start_price">
										</div> -->
										<div class="form-group form-inline">
											<label class="col-md-4 col-form-label">Usage Count</label>
											<input type="text" class="col-md-7 form-control" placeholder="Enter usage count" name="usage_count">
										</div>
										<!-- <div class="form-group form-inline">
											<label class="col-md-4 col-form-label">Discount</label>
											<input type="text" class="col-md-7 form-control" placeholder="Enter Discount" name="discount">
										</div> -->
										<div class="form-group form-inline">
											<label class="col-md-4 col-form-label">Expiry Date</label>
											<input type="text" class="col-md-7 form-control" placeholder="Enter Expiry Date" name="expiry_date">
										</div>
									</div>
									<div class="kt-portlet__foot text-center">
										<div class="kt-form__actions text-center">
											<input type="submit" value="Submit" class="btn btn-primary"/> 
											<button type="button" 
										class="btn btn-default" 
										data-dismiss="modal">Close</button>
										</div>
										
									</div>
								</form>

								<!--end::Form-->
							</div>
							</div>

			</div>
			<!-- <div class="modal-footer">
				<button type="button" 
				class="btn btn-default" 
				data-dismiss="modal">Close</button>
				<span class="pull-right">
				<button type="button" class="btn btn-primary">
					Save
				</button>
				</span>
			</div> -->
			</div>
		</div>
		</div>



	<!-- Edit Promo-->

		<!--Edit-->
		<div class="modal fade" id="promosEditModal" 
			tabindex="-1" role="dialog" 
			aria-labelledby="promosEditModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" 
				id="promosEditModalLabel">Edit Promo</h4>
			</div>
			<div class="modal-body">
										<div class="col-md-12">

							<!--begin::Portlet-->
							<div class="kt-portlet">
														<!--route('drivers-management.update_driver  -->
								<!--begin::Form-->
								<form id="update_form"  method="POST" enctype="multipart/form-data" class="kt-form ajax-form"><!-- Form row -->
									{{csrf_field()}}
									{{ method_field('PUT') }}
									<input type="hidden" name="id" id="id">
									<div class="kt-portlet__body">
										<div class="form-group form-inline">
											<label class="col-md-4 col-form-label">PromoCode:</label>
											<input id="promo_code" type="text" class="col-md-7 form-control" placeholder="Enter promo code" name="promo_code">
										</div>
										<div class="form-group form-inline">
											<label class="col-md-4 col-form-label" for="discount_type">Discount Type</label>
											<select class="col-md-7 form-control" name="discount_type" id="discount_type">
												<option>Full</option>
												<option>Half</option>
												<option>20%</option>
												<option>10%</option>
											</select>
										</div>
										<div class="form-group form-inline">
											<label class="col-md-4 col-form-label" for="user_type">User Type</label>
											<select class="col-md-7 form-control" name="user_type" id="user_type">
												<option>Super</option>
												<option>Regular</option>
												<option>Normal</option>
											</select>
										</div>
										<!-- <div class="form-group form-inline">
											<label class="col-md-4 col-form-label">Starting at</label>
											<input type="text" class="col-md-7 form-control" placeholder="Enter starting price" name="start_price">
										</div> -->
										<div class="form-group form-inline">
											<label class="col-md-4 col-form-label">Usage Count</label>
											<input type="text" class="col-md-7 form-control" id="usage_count" placeholder="Enter usage count" name="usage_count">
										</div>
										<!-- <div class="form-group form-inline">
											<label class="col-md-4 col-form-label">Discount</label>
											<input type="text" class="col-md-7 form-control" placeholder="Enter Discount" name="discount">
										</div> -->
										<div class="form-group form-inline">
											<label class="col-md-4 col-form-label">Expiration</label>
											<input type="text" id="expiry_date" class="col-md-7 form-control" placeholder="Enter Expiration" name="expiry_date">
										</div>
									</div>
									<div class="kt-portlet__foot text-center">
										<div class="kt-form__actions text-center">
											<input type="submit" value="Update" class="btn btn-primary"/> 
											<button type="button" 
										class="btn btn-default" 
										data-dismiss="modal">Close</button>
										</div>
										
									</div>
								</form>
								<!--end::Form-->
							</div>
							</div>

			</div>
			<!-- <div class="modal-footer">
				<button type="button" 
				class="btn btn-default" 
				data-dismiss="modal">Close</button>
				<span class="pull-right">
				<button type="button" class="btn btn-primary">
					Save
				</button>
				</span>
			</div> -->
			</div>
		</div>
		</div>


	<!-- Delete Modal -->
<!-- Modal HTML -->
<div id="deleteModal" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<!-- <div class="icon-box">
					<i class="glyphicon glyphicon-trash"></i>
				</div>				 -->
				<h2 class="modal-title">Are you sure ?</h4>	
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
			<input type="hidden" class="form-control" name="input_del" id="input_del">
				<p>Do you really want to delete these records? This process cannot be undone.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
				<form id="delete_form"  method="POST">
   				{{ method_field('DELETE') }}
				   {{ csrf_field() }}
   				<!-- {{ csrf_field() }} -->
   				<input type="submit" value="Delete" class="btn btn-danger btn-block">
				</form>

				<!-- <a href="{{ URL::to('drivers-management/' . '2') }}" class="btn btn-danger">Delete</a> -->
				
				<!-- <button href="{{ route('drivers-management.destroy',1) }}" type="button" class="btn btn-danger">Delete</button> -->
			</div>
		</div>
	</div>
 @endsection
 @section('script-dashboard')

 <script>
        $(document).ready(function(){
            $('.dataTables-admin').DataTable({
            	"lengthChange": false,
            	"searching": false,
            	"bInfo" : false,
            	"bSort" : false,

                responsive: true,
               
				language: {
					paginate: {
						next: '<i class="fas fa-angle-right fa-lg"></i>',
						previous: '<i class="fas fa-angle-left fa-lg"></i>'  
					}
				}

            });

        });

	function promoInfo(data){
          $('.modal-title').html('Edit Promo');
          $("#promo_code").val(data.promo_code);
          $("#user_type").val(data.user_type);
          $("#discount_type").val(data.discount_type);
		  $('#expiry_date').val(data.expiry_date);
		  $('#usage_count').val(data.usage_count);
		  $('#id').val(data.id);
		  
		  var url = '{{ route("promos-management.update", ":slug") }}';
		  url = url.replace(':slug', data.id);
		  $('#update_form').attr('action', url);
		  
		  
		  $('#promosEditModal').modal('show');
      }
	  function deletePromo(slug){
		$('.modal-title').html('Delete Promo');
          $('#input_del').val(slug);
		  var url = '{{ route("promos-management.destroy", ":slug") }}';
		  url = url.replace(':slug', slug);
		  $('#delete_form').attr('action', url);
		  $('#deleteModal').modal('show');
      }

    </script>
 @stop