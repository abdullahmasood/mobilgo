@extends('admin-layout.content')


@section('content')

	<div class="row">
		 <div class="col-lg-12">
            <div class="ibox ">

		 		<div class="ibox-content">

			 		<div class="table-responsive mt-3">
			 		
				 			<div class="col-12">
				 				<h2 class="font-weight-bold float-left">Payment Mangement</h2>
				 			</div>
				 			
			 			
			 			<div class="clearfix"></div>
			 			<table class="table table-striped  table-hover dataTables-admin" >
			 				<thead>
			 					<tr>
			 						<th>Booking ID</th>
			 						<th>Driver</th>
			 						<th>Rider</th>
			 						<th>Trip date</th>
			 						<th>Total fare</th>
			 						<th>Plartform fee</th>
			 						<th>Promo discounr</th>
			 						<th>Ride status</th>
			 						<th>Payment method</th>
			 						<th>Driver Payment status</th>
			 					</tr>
			 				</thead>
			 				<tbody>
			 					<tr class="gradeX">
			 						<td class="center">3325</td>
			 						<td class="center">John Doe</td>
			 						<td class="center">Andrew Maverl</td>
			 						<td class="center">23-08-2019</td>
			 						<td class="center">$300</td>
			 						<td class="center">$2.54</td>
			 						<td class="center">0.0</td>
			 						<td><span class="label label-primary">Successful</span></td>
			 						<td class="center">Cash</td>
			 						<td class="center"><span class="label label-danger">Pending</span></td>
			 					</tr>
			 				</tbody>
			 			</table>
			 			
			 			
			 		</div>
		 		</div>
		 	</div>
		</div>
	</div>

 @endsection
 @section('script-dashboard')

 <script>
        $(document).ready(function(){
            $('.dataTables-admin').DataTable({
            	"lengthChange": false,
            	"searching": false,
            	"bInfo" : false,
            	"bSort" : false,

                responsive: true,
               
				language: {
					paginate: {
						next: '<i class="fas fa-angle-right fa-lg"></i>',
						previous: '<i class="fas fa-angle-left fa-lg"></i>'  
					}
				}

            });

        });

    </script>
 @stop