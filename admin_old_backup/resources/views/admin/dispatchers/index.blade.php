@extends('admin-layout.content')


@section('content')

	<div class="row">
		 <div class="col-lg-12">
            <div class="ibox ">

		 		<div class="ibox-content">

			 		<div class="table-responsive mt-3">
			 		
				 			<div class="col-12">
				 				<h2 class="font-weight-bold float-left">Dispatcher Panel</h2>
				 			</div>
							 <div>
                                @include('../../admin-layout/flash-message')
							</div>
							 <div class="col-12 ">
				 				<button data-toggle="modal" 
   									data-target="#dispatcherModal" class="btn btn-success float-right">Create new disptacher</button>
					 			
					 			
				 			</div>
			 			
			 			<div class="clearfix"></div>
			 			<table class="table table-striped  table-hover dataTables-admin" >
			 				<thead>
							 	<tr>
			 						<th>Name</th>
			 						<th>Phone number</th>
			 						<th>Destination (from)</th>
			 						<th>Destination (to)</th>
			 						<th>Acessebility</th>
			 						<th>Assign Driver</th>
			 						<th>Status</th>
			 					</tr>
			 				</thead>
			 			<tbody>
			 			    	@foreach ($trips as $trip)
			 					<tr class="gradeX">
			 					    <td class="center">
			 					        @if(is_null($trip->dispatch_rider_name))
                                            {{ '---' }} 
                                            @else
                                           {{ $trip->dispatch_rider_name }}
                                        @endif
			 					        
			 					    </td>
			 						<td class="center">
			 						    
			 						     @if(is_null($trip->dispatch_rider_phone_no))
                                            {{ '---' }} 
                                            @else
                                           {{ $trip->dispatch_rider_phone_no }}
                                        @endif
			 						    
			 						</td>
			 						<td class="center">{{$trip->destination_from}}</td>
			 						<td class="center">{{$trip->destination_to}}</td>
			 						<td class="center">Handicap</td>
			 						<td class="center">{{$trip->driver->name}}</td>
			 						<td class="center"><span class="label label-primary">Assigned</span></td>
			 					</tr>
			 					
			 				    @endforeach
			 				</tbody>
			 			</table>
			 			
			 			
			 		</div>
		 		</div>
		 	</div>
		</div>
	</div>


	<div class="modal fade" id="dispatcherModal" 
			tabindex="-1" role="dialog" 
			aria-labelledby="dispatcherModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" 
				id="dispatcherModalLabel">Add Dispatcher</h4>
			</div>
			<div class="modal-body">
										<div class="col-md-12">

							<!--begin::Portlet-->
							<div class="kt-portlet">
														
								<!--begin::Form-->
								<form  action="{{route('store-dispatch')}}" method="post" enctype="multipart/form-data" class="kt-form ajax-form" data-cb="job_category_add"><!-- Form row -->
									{{csrf_field()}}
									<div class="kt-portlet__body">
										<div class="form-group form-inline">
											<label class="col-md-3 col-form-label">Name</label>
											<input type="text" class="col-md-7 form-control" placeholder="Enter rider name" name="rider_name">
										</div>
										<div class="form-group form-inline">
											<label class="col-md-3 col-form-label">Tel Number</label>
											<input type="text" class="col-md-7 form-control" placeholder="Rider tell" name="rider_tell">
										</div>
										<div class="form-group form-inline">
											<label class="col-md-3 col-form-label" for="sel1">Select Driver</label>
											<select class="col-md-7 form-control" name="select_driver" id="select_driver">
												 @foreach ($drivers as $driver)
													<option>{{$driver->name}}</option>
												 @endforeach
											</select>
										</div>
										<div class="form-group form-inline">
											<label class="col-md-3 col-form-label" for="sel1">Trip</label>
											<select class="col-md-7 form-control" name="trip_category" id="trip_category">
												<option>Category</option>
												<option value="instant_trip">Instant Trip</option>
												<option value="schedule_trip">Schedule Trip</option>
											</select>
										</div>
										<input type="hidden" name="schedule_trip_date" value="" id="schedule_trip_date">
										
										<div class="form-group form-inline">
											<label class="col-md-3 col-form-label">Date</label>
											<input type="text" class="col-md-7 form-control" placeholder="Date" name="date" id="date">
										</div>
										
										
										<div class="form-group form-inline  date_add" id="time_check1">
						                    <label class="col-md-3 col-form-label">Start Time:</label>
						                <input type="text" class="col-md-7 form-control" placeholder="Enter Trip Start Time" name="trip_time_one" autocomplete="off" id="time1">
					                    </div>
					                    <div class="form-group form-inline date_add" id="time_check2">
						                    <label class="col-md-3 col-form-label">End Time:</label>
						                    <input type="text" class="col-md-7 form-control" placeholder="Enter Trip End Time" name="trip_time_two" autocomplete="off" id="time2">
					                    </div>
										
										
										
										<div class="form-group form-inline">
											<label class="col-md-3 col-form-label">Pickup</label>
											<input type="text" class="col-md-7 form-control map-set" placeholder="pickup location" name="pickup_loc" id="pickup">
										</div>
										<div class="form-group form-inline">
											<label class="col-md-3 col-form-label">Dropoff</label>
											<input type="text" class="col-md-7 form-control map-set" placeholder="Enter rider destination" name="rider_dest" id="dropoff">
										</div>
                						
                                        <input type="hidden" name="pickup_latitude" value="" id="pickup_lat">
                                        <input type="hidden" name="pickup_longitude" value="" id="pickup_lng">
                                        
                                        <input type="hidden" name="dropff_latitude" value="" id="dropoff_lat">
                                        <input type="hidden" name="dropoff_longitude" value="" id="dropoff_lng">
                                        
										<div class="form-group form-inline">
											<label class="col-md-3 col-form-label" for="sel1">Accessibility</label>
											<select class="col-md-7 form-control" id="sel1">
												<option>Normal</option>
												<option>Fast</option>
												<option>Slow</option>
											</select>
										</div>
									
									</div>
									<div class="kt-portlet__foot text-center">
										<div class="kt-form__actions text-center">
											<input type="submit" value="Save" class="btn btn-primary"/> 
											<button type="button" 
										class="btn btn-default" 
										data-dismiss="modal">Close</button>
								
										</div>
									</div>
								</form>

								<!--end::Form-->
							</div>
							</div>

			</div>
			<!-- <div class="modal-footer">
				<button type="button" 
				class="btn btn-default" 
				data-dismiss="modal">Close</button>
				<span class="pull-right">
				<button type="button" class="btn btn-primary">
					Save
				</button>
				</span>
			</div> -->
			</div>
		</div>
		</div>

 @endsection
 @section('script-dashboard')
 <script src="//geodata.solutions/includes/countrystatecity.js"></script>
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAztLRM2c-6I3w681cHGtNQgjLVzmIQdt0&libraries=places,geometry"></script>
    <script>
    	google.maps.event.addDomListener(window, 'load', function () {
    		var pickup = new google.maps.places.Autocomplete(document.getElementById('pickup'));
    		var dropoff = new google.maps.places.Autocomplete(document.getElementById('dropoff'));
    		google.maps.event.addListener(pickup, 'place_changed', function () {
    			var place= pickup.getPlace();
    			var location=place.formatted_address;
    			location += place.geometry.location + "<br/>";
    			latitudeS = place.geometry.location.lat();
    			longitudeS = place.geometry.location.lng();
    			document.getElementById("pickup_lat").value=latitudeS;
                document.getElementById("pickup_lng").value=longitudeS;
    	
    		});
    		google.maps.event.addListener(dropoff, 'place_changed', function () {
    			var place= dropoff.getPlace();
    			var location=place.formatted_address;
    			location += place.geometry.location + "<br/>";
    			latitudeS = place.geometry.location.lat();
    			longitudeS = place.geometry.location.lng();
    		    
    		    document.getElementById("dropoff_lat").value=latitudeS;
                document.getElementById("dropoff_lng").value=longitudeS;
    		});
    	});
    </script>
    <script>
    	$(document).ready(function(){
    		$('#date').datepicker();
    		//$('#time1').timepicker();
    		//$('#time1').mdtimepicker();
    // 		 $('#time1').datetimepicker({
            
    //                 format: 'HH:mm:ss'
            
    //             }); 
                
    //         $('#time2').datetimepicker({
            
    //                 format: 'HH:mm:ss'
            
    //             });
                
                
            
            //$('#time1').mdtimepicker();

    		///$('#time2').mdtimepicker();

    		$('#trip_category').on('change',function() {
    			if($(this).val() == 'schedule_trip') {
    				$('#time_check1').removeClass('date_add');
    				$('#time_check2').removeClass('date_add');
    			} else {
    				$('#time_check1').addClass('date_add');
    				$('#time_check2').addClass('date_add');

    				//$('#time_check2').removeClass('date_add');
    				
    			}
    		});
    	});
    </script>
 <script>
        $(document).ready(function(){
            $('.dataTables-admin').DataTable({
            	"lengthChange": false,
            	"searching": false,
            	"bInfo" : false,
            	"bSort" : false,

                responsive: true,
               
				language: {
					paginate: {
						next: '<i class="fas fa-angle-right fa-lg"></i>',
						previous: '<i class="fas fa-angle-left fa-lg"></i>'  
					}
				}

            });

        });

    </script>
 @stop