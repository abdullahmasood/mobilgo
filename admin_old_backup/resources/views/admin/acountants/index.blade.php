@extends('admin-layout.content')


@section('content')

	<div class="row">
		 <div class="col-lg-12">
            <div class="ibox ">

		 		<div class="ibox-content">

			 		<div class="table-responsive mt-3">
			 		
				 			<div class="col-12">
				 				<h2 class="font-weight-bold float-left">Accountant Panel</h2>
				 			</div>
				 			
			 			
			 			<div class="clearfix"></div>
			 			<table class="table table-striped  table-hover dataTables-admin" >
			 				<thead>
			 					<tr>
			 						<th></th>
			 						<th>Name</th>
			 						<th>Email</th>
			 						<th>Phone Number</th>
			 						<th>Sign up date</th>
			 						<th>Trip count</th>
			 						<th>Action</th>
			 					</tr>
			 				</thead>
			 				<tbody>
			 					<tr class="gradeX">
			 						<td class="center"></td>
			 						<td class="center">John Doe</td>
			 						<td class="center">johndoe@gmail.com</td>
			 						<td class="center">342346656</td>
			 						<td class="center">23-07-2018</td>
			 						<td class="center">3</td>
			 						<td><i class="fas fa-pen"></i>
			 							<i class="fa fa-times"></i>
			 						</td>
			 					</tr>
			 				</tbody>
			 			</table>
			 			
			 			
			 		</div>
		 		</div>
		 	</div>
		</div>
	</div>

 @endsection
 @section('script-dashboard')

 <script>
        $(document).ready(function(){
            $('.dataTables-admin').DataTable({
            	"lengthChange": false,
            	"searching": false,
            	"bInfo" : false,
            	"bSort" : false,

                responsive: true,
               
				language: {
					paginate: {
						next: '<i class="fas fa-angle-right fa-lg"></i>',
						previous: '<i class="fas fa-angle-left fa-lg"></i>'  
					}
				}

            });

        });

    </script>
 @stop