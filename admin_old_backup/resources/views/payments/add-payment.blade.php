<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Mobilego Login Page</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="csrf-token" content="{{ Session::token() }}"> 
	<meta content="Coderthemes" name="author" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<!-- App favicon -->

	<link href="{{asset('public/assets/css/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
	<link href="{{asset('public/assets/css/bootstrap-latest.min.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('public/assets/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
	<link href="{{asset('public/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />
	<style>
		.form-control-custom, .form-control-custom:active,.form-control-custom:focus {
			border: none;
			

			width: 100%;
			padding: 20px 10px;
			background: #f7f7fa;
		}
		.btn-success-custom {
			background: #40AC56;
			border-radius: 5px;

		}
		.checkbox-info-go input[type=checkbox]:checked+label::before {
			background-color: #40AC56;
			border-color: #40AC56;
		}
	
		.card-custom {
			-webkit-box-shadow: -1px -1px 23px -16px rgba(0,0,0,0.75);
			-moz-box-shadow: -1px -1px 23px -16px rgba(0,0,0,0.75);
			box-shadow: -1px -1px 23px -16px rgba(0,0,0,0.75);
			

		}
		.card-custom .card-body {
			padding: 0;
		}
		.p-4-custom {
			padding: 3rem;
		}
		.header-custom{
			background: #40AC56;
			color: #fff;
		}
		.custom-color {
			color:#019a76;
		}
		.payment-method {
			background: #fff;
		}
		.new-payment {
			
			padding: 15px 0;
			margin: 5% 0;
		}
		.new-payment a {
			font-size: 16px;
			font-weight: 500;
		}
		.payment-method-list {
			-webkit-box-shadow: -1px -1px 23px -16px rgba(0,0,0,0.75);
			-moz-box-shadow: -1px -1px 23px -16px rgba(0,0,0,0.75);
			box-shadow: -1px -1px 23px -16px rgba(0,0,0,0.75);
			background: #fcfcfc;
			border-radius: 5px;
		}
		.payment-method-list p{
			margin: 3% 0 !important;
		}
		.form-group-custom {
			margin: 3% 5%;
		}
		.btn-confirm-custom {
			padding: 5% 35% !important;
			font-size: 20px !important;
			text-align: center !important;
			margin-bottom: 5% !important;
			background: #40AC56 !important;
		}
		.swal2-title {
			color: #40AC56 !important;
		}
		.card_error {
			display: inline-block !important;
			color: red;
		}
			@media screen and (max-width: 401px) {
			.header-custom {
				font-size: 20px;
			}
			.fa-lg {
				font-size: 0.9em;
			}	
		}
		@media screen and (max-width: 348px) {
			.header-custom {
				font-size: 16px;
			}		
		}
		@media screen and (max-width: 375px) {
			.ml-3 {
				margin-left: 1rem!important;
			}
			.mr-3 {
				margin-right: 1rem!important;
			}	
		}
	</style>

</head>

<body class="bg-white">

	<div class="account-pages mt-5 mb-5">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8 col-lg-6 col-xl-5">
					<div class="card card-custom">
						<div class="card-header header-custom">

							<div class="row align-items-center  ">
								<div class="col-2 text-left">
									<i class="fas fa-arrow-left fa-lg"></i>
								</div>
								<div class="col-8 text-center ">
									<h3 class="text-white font-weight-normal header-custom">Add new Payment</h3>
								</div>
								<div class="col-2 text-right">
									<i class="far fa-bell fa-lg"></i>
								</div>
							</div>
							
						</div>

						<div class="card-body">
							
							
							<form action="#" method="post" class="add_pay">
								<div class="row">
									<div class="col-12">
										<div class="form-group form-group-custom">
											<label>Credit or Debit Card Number</label>

											<input class="form-control form-control-custom form-field" type="text" id="card" required="" name="card">
											<span class="add_card_class" style="display: none;">Invalid Card Number</span>
										</div>
									</div>

									<div class="col-xs-12 col-sm-6 col-md-6">
										<div class="form-group mt-1 ml-3 mr-xs-3 mr-sm-0  mr-md-0 mr-lg-0 mr-3">
											<label>Security Card (CVV)</label>

											<input class="form-control form-control-custom form-field" type="text" required="" id="security_card" name="security_card">
											<span class="add_cvv_class" style="display: none;">Invalid CVV</span>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6">
										<div class="form-group mt-1 mr-3  ml-3 ml-xs-3  ml-sm-0 ml-xs-0 ml-lg-0">
											<label>Expiry Date (MM/YY)</label>

											<input class="form-control form-control-custom form-field" type="text" required="" id="expiry_date" name="expiry_date">
										</div>
									</div>
									<div class="col-12">
										<div class="form-group form-group-custom mt-1 mb-5 pb-3">
											<label>Zip/Postal Code</label>

											<input class="form-control form-control-custom form-field" type="text"  required="" id="zip" name="zip">
										</div>
									</div>

								</div>



								<div class="form-group mb-0 text-center mx-3 mt-3">
									<button class="btn btn-success btn-block btn-success-custom rounded py-2 add_payment_method" type="button"> Add new Payment </button>
								</div>

							</form>
							



						</div> <!-- end card-body -->
					</div>
					<!-- end card -->


					<!-- end row -->

				</div> <!-- end col -->
			</div>
			<!-- end row -->
		</div>
		<!-- end container -->
	</div>
	<!-- end page -->




	<!-- Vendor js -->
	<script src="{{ URL::asset('public/assets/js/vendor.min.js')}}"></script>

	<!-- App js -->
	<script src="{{ URL::asset('public/assets/js/app.min.js')}}"></script>
	<script src="{{ URL::asset('public/assets/js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>
	 <script src="{{ URL::asset('public/assets/js/sweetalert2.min.js')}}"></script>
	 <script>

	 	function check_card() {
	 		var card = $('#card').val();
	 		var card_type = detectCardType(card);

	 		if (typeof card_type === "undefined") {

	 			$('.add_card_class').addClass('card_error');
	 			$('.add_payment_method').prop('disabled',true);

	 		} else {
	 			$('.add_card_class').removeClass('card_error');
	 			$('.add_payment_method').prop('disabled',false);

	 		}
	 	}
	 	function validateForm() {
	 		var isValid = true;
	 		$('.form-field').each(function() {
	 			if ( $(this).val() === '' )
	 				isValid = false;
	 		});
	 		return isValid;
	 	}
	 	

	 </script>

       
	<script type="text/javascript">
		$(document).ready(function() {
			$('.add_payment_method').prop('disabled',true);
			var security_card =$('#security_card').val();

			$(".form-field").keyup(function() {
				var validate = validateForm();
				if (validate == true) {
					check_card();
					
				}
			});



			
			
			$('.add_payment_method').on('click',function() {

				var card = $('#card').val();
				var card_type = detectCardType(card);
				
				
				var security_card = $('#security_card').val();
				var expiry_date	= $('#expiry_date').val();
				var zip	= $('#zip').val();
				$.ajax({
					url: "{{route('payment.method')}}",
					type : 'post',
					data: {
						card: card,
						security_card:security_card,
						expiry_date:expiry_date,
						zip:zip,
						card_type:card_type,
						'_token': $('meta[name=csrf-token]').attr('content')
					},
					dataType: "json",
					success: function(data ) {
						Swal.fire({
							title:"Successful",
							type:"success",
							 showConfirmButton:!1,
				// 			html: ''
						});
					},
				});

			});
			var date = new Date();
			date.setDate(date.getDate()-1);
			$('#expiry_date').datepicker({
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				calendarWeeks: true,
				autoclose: true,
                format: "mm/yyyy",
                 startDate: date
			});

		});
		function detectCardType(number) {
			var re = {
				electron: /^(4026|417500|4405|4508|4844|4913|4917)\d+$/,
				maestro: /^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/,
				dankort: /^(5019)\d+$/,
				interpayment: /^(636)\d+$/,
				unionpay: /^(62|88)\d+$/,
				visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
				mastercard: /^5[1-5][0-9]{14}$/,
				amex: /^3[47][0-9]{13}$/,
				diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
				discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
				jcb: /^(?:2131|1800|35\d{3})\d{11}$/
			}

			for(var key in re) {
				if(re[key].test(number)) {
					return key
				}
			}
		}
	</script>


</body>
</html>