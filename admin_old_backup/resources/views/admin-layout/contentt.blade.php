
@include('admin-layout.head')
<!-- @include('admin-layout.header') -->
<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->
<div class="wrapper wrapper-content">
    {{-- @include('admin-layout.flash-message') --}}
    @yield('content')
</div>
@include('admin-layout.footer')



