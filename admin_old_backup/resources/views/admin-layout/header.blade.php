<body class=" fixed-nav js-focus-visible pace-done">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side custom-nav" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                     <li style="padding-top: 20px;">
                        <a class="nav-label heading-nav">
                            @if(Auth::user()->user_type === 'SuperAdministrator')
                            Admin Dashboard
                            @endif
                            @if(Auth::user()->user_type === 'DispatcherAdministrator')
                            Dispatcher Dashboard
                            @endif
                            </a>
                        
                    </li>
                    @if(Auth::user()->user_type === 'SuperAdministrator')
                    <li class="{{ (request()->is('dashboard')) ? 'active' : '' }}">
                        <a href="{{url('/dashboard')}}"><i class="fa fa-anchor"></i> <span class="nav-label">Dashboard</span> </a>
                        
                    </li>
                    
                    <li class="{{request()->routeIs('admin-users.index') ? 'active' : '' }}">
                        <a href="{{route('admin-users.index')}}"><i class="fas fa-users-cog"></i> <span class="nav-label heading-nav">Admin User</span></a>
                    </li>
                    <li class="{{request()->routeIs('heat-map') ? 'active' : '' }}">
                        <a href="{{route('heat-map')}}"><i class="fa fa-map-marked"></i> <span class="nav-label">Heat Map</span></a>
                    </li>
                    <li >
                        <a class="nav-label heading-nav">Driver Management</a>
                        
                    </li>
                   
                    
                    <li class="{{request()->routeIs('drivers-management.index') ? 'active' : '' }}">
                        <a href="{{route('drivers-management.index')}}"><i class="fa fa-user-tie"></i> <span class="nav-label">Drivers</span>  </a>
                    </li>
                    <li class="{{request()->routeIs('cabs-management.index') ? 'active' : '' }}">
                        <a href="{{route('cabs-management.index')}}"><i class="fa fa-car"></i> <span class="nav-label">Cab Management</span></a>
                    </li>
                     <li>
                        <a class="nav-label heading-nav">Rider Management</a>
                        
                    </li>
                    <li class="{{request()->routeIs('riders-management.index') ? 'active' : '' }}">
                        <a href="{{route('riders-management.index')}}"><i class="fa fa-users"></i> <span class="nav-label">Riders</span></a>
                    </li>
                    <li>
                        <a class="nav-label heading-nav">General</a>
                        
                    </li>

                    <li class="{{request()->routeIs('trip-history-management') ? 'active' : '' }}">
                        <a href="{{route('trip-history-management')}}"><i class="fa fa-money-bill-alt"></i> <span class="nav-label">Trips</span>  </a>
                    </li>
                    <li class="{{request()->routeIs('reviews-management') ? 'active' : '' }}">
                        <a href="{{route('reviews-management')}}"><i class="fa fa-award"></i> <span class="nav-label">Reviews</span></a>
                    </li>
                    <li class="{{request()->routeIs('promos-management.index') ? 'active' : '' }}">
                        <a href="{{route('promos-management.index')}}"><i class="fa fa-box-open"></i> <span class="nav-label">Promo and Coupons</span></a>
                    </li>
                    <li>
                        <a class="nav-label heading-nav">Panel Management</a>
                        
                    </li>
                      <li class="{{request()->routeIs('accountant-panel') ? 'active' : '' }}">
                        <a href="{{route('accountant-panel')}}"><i class="fa fa-spa"></i> <span class="nav-label">Accountant panel</span></a>
                    </li>
                    @endif
                    <li class="{{request()->routeIs('dispatcher-panel') ? 'active' : '' }}">
                        <a href="{{route('dispatcher-panel')}}"><i class="fa fa-meteor"></i> <span class="nav-label">Dispatcher panel</span></a>
                    </li>
                    @if(Auth::user()->user_type === 'SuperAdministrator')
                     <li class="{{request()->routeIs('company-cabs-management.index') ? 'active' : '' }}">
                        <a href="{{route('company-cabs-management.index')}}"><i class="fas fa-car-side"></i> <span class="nav-label">Cab Company panel</span></a>
                    </li>
                    <li>
                        <a class="nav-label heading-nav">Payment Setting</a>
                        
                    </li>
                     <li class="{{request()->routeIs('payments-management') ? 'active' : '' }}">
                        <a href="{{route('payments-management')}}"><i class="fa fa-cog"></i> <span class="nav-label">Payment Report</span></a>
                    </li>
                    @endif
                </ul>

            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-fixed-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header align-items-center w-50 d-flex">
                     <a class="navbar-minimalize minimalize-styl-2 ">
                        <img src="{{ URL::asset('assets/img/logo.svg')}}" alt="user-image" class="rounded-circle" />
                        
                    </a>
                    {{-- <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a> --}}
                   
                    <form role="search" class="mx-auto w-50" action="search_results.html">
                     <div class="input-group custom-input-group">
                        <i class="fa fa-search custom-fa-search align-self-center"></i>
                        <input type="text" class="form-control search-top" placeholder="Search transaction and information" name="top-search" id="top-search">
                    </div>
                           
                       
                    </form>
                </div>
                <ul class="nav  navbar-right mr-3 align-items-center">
                    <li style="padding: 20px" class="mr-3">
                    <select class="form-control">
                        <option>Select language</option>
                    </select>
                    </li>
                    <li class="dropdown mr-1">
                        <a class="count-info" data-toggle="dropdown" href="#">
                            <i class="fas fa-comments"></i>  
                        </a>
                       
                    </li>
                    <li class="dropdown mr-1">
                        <a class=" count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-bell"></i>  <span class="label label-danger">0</span>
                        </a>
                      
                    </li>
                     <li class="dropdown">
                        <a class="dropdown-toggle custom-dropdown" data-toggle="dropdown" href="#">
                           {{Auth::user()->name}}
                        </a>
                        <ul class="dropdown-menu dropdown-messages dropdown-menu-right">
                            <li>
                                <div>
                                    <!-- <a style="color: #000000;text-decoration: none;" href="{{route('logout')}}"><i class="fa fa-sign-out" aria-hidden="true"></i> <span class="nav-label"><strong>Logout</strong></span> </a> -->
                                    <a style="color: #000000;text-decoration: none;" href="{{ route('logout') }}" class="dropdown-item notify-item"  onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        <i class="fe-log-out"></i>
                                        <span>{{ __('Logout') }}</span>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                          
                        </ul>
                    </li>
                    <li class="mr-1">
                        <img src="{{ URL::asset('assets/img/landing/avatar5.jpg')}}" class="img-sm rounded-circle">
                    </li>


                   
                </ul>

            </nav>
        </div>

