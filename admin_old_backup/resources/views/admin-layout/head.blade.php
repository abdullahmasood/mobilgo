<!DOCTYPE html>
<html>
	<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="csrf-token" content="{{ Session::token() }}"> 
		<title>Mobilego | Dashboard</title>
		<link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
		<link href="{{asset('assets/fontawesome/css/all.css')}}" rel="stylesheet">
		<link href="{{asset('assets/css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">

    <link href="{{asset('assets/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
    

		<!-- Toastr style -->
		<link href="{{asset('assets/css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
		<!-- Gritter -->
		<link href="{{asset('assets/js/plugins/gritter/jquery.gritter.css')}}" rel="stylesheet">

		<link href="{{asset('assets/css/animate.css')}}" rel="stylesheet">
		<link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
		<link href="{{asset('assets/css/custom-style.css')}}" rel="stylesheet">
		<style>
		    .pac-container {
        z-index: 10000 !important;
    }
    .date_add{
        display:none;
    }
		</style>
	</head>