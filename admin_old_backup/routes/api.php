<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/driver', 'ApiController@login_driver');

Route::post('/rider', 'ApiController@login_rider');

Route::post('/register', 'ApiController@register');

Route::post('/trip', 'ApiController@addNewTrip');

Route::get('/recent_trips/{id}', 'ApiController@getRecentTrips');
Route::get('/driver/{id}', 'ApiController@getDriver');

Route::get('/testSort', 'ApiController@testSort');

Route::post('/changeTripStatus','ApiController@changeTripStatus');
Route::get('/driverTripHistory/{id}','ApiController@getDriverTripHistory');

Route::post('/getDistance','ApiController@getDistance');
Route::post('/updateDriverTripLocation','ApiController@updateDriverTripLocation');
Route::get('/verify-users', 'ApiController@users');//dd
Route::post('/updateDriverLocation','ApiController@updateDriverLocation');

Route::post('/updateFcmToken','ApiController@updateFcmToken');
Route::post('/finishTrip','ApiController@finishTrip');
Route::post('/getDriverEarning','ApiController@getDriverEarning');
Route::get('/getScheduleTripHistory/{id}','ApiController@getScheduleTripHistory');
Route::get('/getDriverCompletedTripHistory/{id}','ApiController@getDriverCompletedTripHistory');
Route::get('/getNotification/{id}','ApiController@getNotification');
Route::post('/getCabServices','ApiController@getCabServicesForCustomer');
Route::get('/getCabServicesForDriver','ApiController@getCabServicesForDriver');

