<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];
    // 'name', 'email', 'password',
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
//'password', 'remember_token',
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function company()
    {
        return $this->belongsTo('App\Company','fk_company_id','id');
    }
    public function cabservice()
    {
        return $this->belongsTo('App\CabService','fk_cab_service_id','id');
    }
    public function customer_trips()
    {
        return $this->hasMany('App\Trip','fk_rider_id','id');
    }
    public function driver_trips()
    {
        return $this->hasMany('App\Trip','fk_driver_id','id')->where('trip_status','pending')->latest()->take(2);
    }
    public function driver_all_trips()
    {
        return $this->hasMany('App\Trip','fk_driver_id','id');
    }

}
