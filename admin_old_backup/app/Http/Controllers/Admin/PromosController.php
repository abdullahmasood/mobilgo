<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Promo;
use Validator;
class PromosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promos = Promo::all();
        //dd($companies[0]->company_name);
        //dd($drivers[0]->company->company_name);
        $data['promos'] = $promos;
       
        return view('admin.promos.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'promo_code' => 'required',
            'user_type' => 'required',
            'discount_type' => 'required',
            'usage_count' => 'required',
        ]);
        if ($validator->fails()) 
        {
            return redirect('promos-management')->with('error',response()->json([
                'type' =>'error',
                'msg' => $validator->getMessageBag ()->toArray(),
            ]));
        
             
        } else {
            
            // if ($request->hasFile('image_url')) {
            // $filenameWithExt = $request->file('image_url')->getClientOriginalName();
            // $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // $extension = $request->file('image_url')->getClientOriginalExtension();
            // $filenameToStore = $filename . '_' . time() . '.' . $extension;
            // $path = $request->file('image_url')->move('images/job_categories', $filenameToStore);
        	// } else {
            // $filenameToStore = 'noimage.jpg';
        	// }
                $promo = new Promo();
                $promo->promo_code = $request->promo_code;
                $promo->user_type = $request->user_type;
                $promo->discount_type = $request->discount_type;
                $promo->expiry_date = $request->expiry_date;
                $promo->usage_count = $request->usage_count;
                $promo->status = "enable";
                $promo->save();
             return redirect('promos-management')->with('success','Promo Added successfully!');            
        }   

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'promo_code' => 'required',
            'user_type' => 'required',
            'discount_type' => 'required',
            'usage_count' => 'required',
        ]);
        if ($validator->fails()) 
        {
            return redirect('promos-management')->with('error',response()->json([
                'type' =>'error',
                'msg' => $validator->getMessageBag ()->toArray(),
            ]));
           
        } else {
            
            $promo_exist = Promo::where('id',$request->id)->first();
            if($promo_exist)
            {
                $promo  = Promo::where('id',$request->id)->first();
                $promo->promo_code = $request->promo_code;
                $promo->user_type = $request->user_type;
                $promo->discount_type = $request->discount_type;
                $promo->expiry_date = $request->expiry_date;
                $promo->usage_count = $request->usage_count;
                $promo->save();
             return redirect('promos-management')->with('success','Promo Updated successfully!');
            }
            else
            {
                return redirect('promos-management')->with('error','Promo Not Found!');
            }
                            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $exist = Promo::where('id',$id)->first();
        if($exist)
        {
            $exist->delete();
            return redirect('promos-management')->with('success','Promo Deleted successfully!');
        }
        else
        {
            return redirect('promos-management')->with('error','Promo Not Found!');
        }
    }
}
