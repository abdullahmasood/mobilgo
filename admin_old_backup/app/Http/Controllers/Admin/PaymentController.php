<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Illuminate\Support\Facades\Session;

use \Illuminate\Support\Facades\Validator;

use App\User;
use App\Company;
use App\Trip;
use App\CabService;
use App\Notification;
class PaymentController extends Controller
{
	private $_api_context;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    	/** PayPal api context **/
    	$paypal_conf = \Config::get('paypal');
    	$this->_api_context = new ApiContext(new OAuthTokenCredential(
    		$paypal_conf['client_id'],
    		$paypal_conf['secret'])
    );
    	$this->_api_context->setConfig($paypal_conf['settings']);
    	

    }
    public function index(Request $request) {
        Session::put('trip_id',$request->trip_id);
        Session::put('amount',$request->amount);
     	$curl = curl_init();
     	curl_setopt_array($curl, array(
     		CURLOPT_URL => "https://api.sandbox.paypal.com/v1/vault/credit-card",
     		CURLOPT_RETURNTRANSFER => true,
     		CURLOPT_ENCODING => "",
     		CURLOPT_MAXREDIRS => 10,
     		CURLOPT_TIMEOUT => 0,
     		CURLOPT_FOLLOWLOCATION => true,
     		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
     		CURLOPT_CUSTOMREQUEST => "GET",
     		CURLOPT_HTTPHEADER => array(
     			"Authorization: Bearer ".$this->get_access_token(),
     		),
     	));
     	$response = curl_exec($curl);
     	curl_close($curl);
     	$cards = json_decode($response,true);
        $cards_data = 
        $cards_data = [];
        $user_cards =  \DB::table('payment_methods')->where('user_id',\Auth::user()->id)->get();
        foreach($cards['items'] as $items) {
            foreach($user_cards as $user_card) {
                if($items['id'] === $user_card->card_id) {
                    $cards_data[] = $items;
                }
            }
        }

     	return view('payments.payment-list',compact('cards_data'));
		}
		public function add_payment_method(Request $request) {
			
			$card = $request->card;
			$security_card = $request->security_card;
			$expiry_date = explode('/',$request->expiry_date);
			$zip = $request->zip;
			$card_type = $request->card_type;

			$curl = curl_init();
			$json_arr = json_encode(array('number' => $card,'type' => $card_type,'expire_month' => $expiry_date[0],'expire_year' => $expiry_date[1], 'cvv2' => $security_card));
			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://api.sandbox.paypal.com/v1/vault/credit-card",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS =>$json_arr,
				CURLOPT_HTTPHEADER => array(
					"Authorization: Bearer ".$this->get_access_token(),
					"Content-Type: application/json"
				),
			));
			$response = curl_exec($curl);
			curl_close($curl);
			$cards = json_decode($response,true);
            
            \DB::table('payment_methods')->insert(
                ['card_id' => $cards['id'], 'user_id'=> \Auth::user()->id]
            );
		}
		public function make_payment(Request $request) {
			$amount = Session::get('amount');
			$card = $request->card_id;

			$json_arr = [
				'intent' => 'sale',
				'payer' => [
					'payment_method' => 'credit_card',
					'funding_instruments' => [
							[
							'credit_card_token' => [
								'credit_card_id' => $card
							],
						],
					],
				],
				'transactions' => [
					[
						'amount' => [
							'total' => $amount,
							'currency' => 'USD'
						],
						'description' => 'Payment by vaulted credit card'
					],
				],
			];

			$curl = curl_init();
			
			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://api.sandbox.paypal.com/v1/payments/payment",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => json_encode($json_arr),
				CURLOPT_HTTPHEADER => array(
					"Authorization: Bearer ".$this->get_access_token(),
					"Content-Type: application/json"
				),
			));
			$response = curl_exec($curl);
			curl_close($curl);
			echo $response;
			exit();
		}

    
    public function payWithpaypal(Request $request)
    {

    	$payer = new Payer();
    	$payer->setPaymentMethod('paypal');

    	$item_1 = new Item();

    	$item_1->setName('Item 1') /** item name **/
    	->setCurrency('USD')
    	->setQuantity(1)
    	->setPrice($request->get('amount')); /** unit price **/

    	$item_list = new ItemList();
    	$item_list->setItems(array($item_1));

    	$amount = new Amount();
    	$amount->setCurrency('USD')
    	->setTotal($request->get('amount'));

    	$transaction = new Transaction();
    	$transaction->setAmount($amount)
    	->setItemList($item_list)
    	->setDescription('Your transaction description');

    	$redirect_urls = new RedirectUrls();
    	$redirect_urls->setReturnUrl(URL::to('status')) /** Specify return URL **/
    	->setCancelUrl(URL::to('status'));

    	$payment = new Payment();
    	$payment->setIntent('Sale')
    	->setPayer($payer)
    	->setRedirectUrls($redirect_urls)
    	->setTransactions(array($transaction));
    	/** dd($payment->create($this->_api_context));exit; **/
    	try {

    		$payment->create($this->_api_context);

    	} catch (\PayPal\Exception\PPConnectionException $ex) {

    		if (\Config::get('app.debug')) {

    			\Session::put('error', 'Connection timeout');
    			return \Redirect::to('/');

    		} else {

    			\Session::put('error', 'Some error occur, sorry for inconvenient');
    			return \Redirect::to('/');

    		}

    	}

    	foreach ($payment->getLinks() as $link) {

    		if ($link->getRel() == 'approval_url') {

    			$redirect_url = $link->getHref();
    			break;

    		}

    	}

    	/** add payment ID to session **/
    	\Session::put('paypal_payment_id', $payment->getId());

    	if (isset($redirect_url)) {

    		/** redirect to paypal **/
    		return \Redirect::away($redirect_url);

    	}

    	\Session::put('error', 'Unknown error occurred');
    	return \Redirect::to('/');

    }

    public function getPaymentStatus(Request $request)
    {
    	/** Get the payment ID before session clear **/
    	$payment_id = \Session::get('paypal_payment_id');

    	/** clear the session payment ID **/
    	\Session::forget('paypal_payment_id');
    	if (empty($request->input('PayerID')) || empty($request->input('token'))) {

    		\Session::put('error', 'Payment failed');
    		return \Redirect::to('/');

    	}

    	$payment = Payment::get($payment_id, $this->_api_context);
    	$execution = new PaymentExecution();
    	$execution->setPayerId($request->input('PayerID'));

    	/**Execute the payment **/
    	$result = $payment->execute($execution, $this->_api_context);
            $transactions = $result->getTransactions();
            $relatedResources = $transactions[0]->getRelatedResources();
            $sale = $relatedResources[0]->getSale();
            $saleId = $sale->getId();
    	if ($result->getState() == 'approved') {

    		\Session::put('success', 'Payment success');
    		
    		return redirect()->route('payment.list');

    	}

    	\Session::put('error', 'Payment failed');
    	return \Redirect::to('/');

    }
    function get_access_token() {
       	$chr = curl_init();
		$client_id = "AcICFoQx4Sg5iILQrusVTOBUivwhnI_6q4n-Wqa_n83x4z5A0spQjGHMRtE3fBCbTV-2a4eGsK2KXfN4";
		$secret = "EMbqiWyiSWwmIKRL8Y4STgqcBCMMxzwRDzrSemqNUodQXbu-AD3uC_s7uGynYIf1na-hZ3Eng0iooebR";
		curl_setopt($chr, CURLOPT_URL, "https://api.sandbox.paypal.com/v1/oauth2/token");
		curl_setopt($chr, CURLOPT_HEADER, false);
		curl_setopt($chr, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($chr, CURLOPT_SSLVERSION , 6); //NEW ADDITION
		curl_setopt($chr, CURLOPT_POST, true);
		curl_setopt($chr, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($chr, CURLOPT_USERPWD, $client_id.":".$secret);
		curl_setopt($chr, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

		$result = curl_exec($chr);

		if(empty($result)) {
			die("Error: No response.");
		} else {
			$json = json_decode($result);
		}

		curl_close($chr); //get access token for next api
		return $json->access_token;
    }
     public function save_order_request(Request $request)
    {
        
        $trip_id = Session::get('trip_id');
			
		$this->changeTripStatus($trip_id);
        echo json_encode($trip_id);
        
        Session::forget('trip_id');
        Session::forget('amount');
        exit();
    }
    
    
    
    public function changeTripStatus($trip_id)
    {
        $trip_exist = Trip::where('id',$trip_id)->first();
                    if($trip_exist)
                    {
                        $trip_exist->trip_status = 'paid';
                        $customer = User::where('id',$trip_exist->fk_rider_id)->first();
                        $driver = User::where('id',$trip_exist->fk_driver_id)->first();
                        $trip_exist->fk_driver_id = $trip_exist->fk_driver_id;
                        $trip_exist->save();
                        $this->notify('Order has created now',$trip_exist->fk_driver_id,201,$driver->fcm_token);
                        $this->setNotification('Get Ready Driver is on his way','Order has created now',$customer->id,$request->fk_driver_id);
                        
                        // $response->status = 'success';
                        // $response->code = 200;
                        // $response->message = 'Get Ready Driver is on his way';
                        
                        // $inner_response->customer = $customer;
                        // $inner_response->$trip = $trip_exist; 
                        // $response->response = $inner_response;
                        
                        // //$response->response = $trip_exist;
                        // return response()->json($response);
                    }
                    else
                    {
                        // $response->status = 'failed';
                        // $response->code = 409;
                        // $response->message = 'Trip with this ID not found';
                        // $response->response = [];
                        // return response()->json($response);
                    }
                
    }
    
    
    public function notify($title,$id,$status,$token)
    {
        //dd($driver);
        $notification_data=[];
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $notification = [
            'title' => $title,
            'sound' => true,
            'id' => $id,
            'status' =>$status,
            'message' => $notification_data,
            //'message' => $message,
        ];
        
        $extraNotificationData = ["message" => $notification,"moredata" =>null];
        
        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $token, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];
        //dd($driver);
        //dd($fcmNotification);
        ////AIzaSyAA3raTvslg-OL_FnEEThVOD8ybWfqSR-I
        ///dd($fcmNotification);
//AIzaSyC1Zz4VCu5sQH27KUS3nLEEFHfos6V6oEc
        $headers = [
            'Authorization: key=AAAAHFHzlRI:APA91bHn-jb2k67s5mE56_GWVy4X4ByRJuiybh825Nx4JC7b90xZS8u3qrrkzadmIOB22KffYxOlrvrUjblb5zYFqpreqih9IKZcjqBGUtNP0AY0wxQW8PtWhV-ZDbLVIAEfz9OfvaPB',
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        //dd($result);
        curl_close($ch);
        return true;
    }

//     public function notifyWithData($title,$status,$token,$order,$customer)
//     {
//         $response = new \stdClass();
//         $response->status = 'success';
//         $response->code = 200;   
//         $response->order = $order;
//         $response->customer = $customer;
//         $notification_data=[];
//         $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
//         $notification = [
//             'title' => $title,
//             'sound' => true,
//             'status' =>$status,
//             'message' => $notification_data,
//             //'message' => $message,
//         ];
        
//         //$extraNotificationData = ["message" => $notification,"moredata" =>null];
//         $extraNotificationData = ["message" => $notification,"moredata" =>$response];
//         $fcmNotification = [
//             //'registration_ids' => $tokenList, //multple token array
//             'to'        => $token, //single token
//             'notification' => $notification,
//             'data' => $extraNotificationData
//         ];
//         //dd($driver);
//         //dd($fcmNotification);
//         ////AIzaSyAA3raTvslg-OL_FnEEThVOD8ybWfqSR-I
//         ///dd($fcmNotification);
// //AIzaSyC1Zz4VCu5sQH27KUS3nLEEFHfos6V6oEc
//         $headers = [
//             'Authorization: key=AAAAHFHzlRI:APA91bHn-jb2k67s5mE56_GWVy4X4ByRJuiybh825Nx4JC7b90xZS8u3qrrkzadmIOB22KffYxOlrvrUjblb5zYFqpreqih9IKZcjqBGUtNP0AY0wxQW8PtWhV-ZDbLVIAEfz9OfvaPB',
//             'Content-Type: application/json'
//         ];


//         $ch = curl_init();
//         curl_setopt($ch, CURLOPT_URL,$fcmUrl);
//         curl_setopt($ch, CURLOPT_POST, true);
//         curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//         curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
//         $result = curl_exec($ch);
//         //dd($result);
//         curl_close($ch);
//         return true;
//     }

    public function notifyWithData($title,$status,$token,$order,$customer)
    {
        $response = new \stdClass();
        $inner_response = new \stdClass();
        $response->status = 'success';
        $response->code = 200;   
        $response->order = $order;
        $response->customer = $customer;
        $inner_response->setting = '1002';
        $inner_response->objects = $response;
        
        $notification_data=[];
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $notification = [
            'title' => $title,
            'body' => $status,
            'sound' => true,
            'status' =>$status,
            'message' => $notification_data,
            //'message' => $message,
        ];
        
        //$extraNotificationData = ["message" => $notification,"moredata" =>null];
        $extraNotificationData = ["message" => $notification,"moredata" =>$inner_response];
        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $token, //single token
            'notification' => $notification,
            'status' => '1001',
            'data' => $inner_response//$extraNotificationData
        ];
        //dd($driver);
        //dd($fcmNotification);
        ////AIzaSyAA3raTvslg-OL_FnEEThVOD8ybWfqSR-I
        ///dd($fcmNotification);
//AIzaSyC1Zz4VCu5sQH27KUS3nLEEFHfos6V6oEc
        
        $headers = [
            'Authorization: key=AAAAHFHzlRI:APA91bHn-jb2k67s5mE56_GWVy4X4ByRJuiybh825Nx4JC7b90xZS8u3qrrkzadmIOB22KffYxOlrvrUjblb5zYFqpreqih9IKZcjqBGUtNP0AY0wxQW8PtWhV-ZDbLVIAEfz9OfvaPB',
            'Content-Type: application/json'
        ];

        // AAAAHFHzlRI:APA91bHn-jb2k67s5mE56_GWVy4X4ByRJuiybh825Nx4JC7b90xZS8u3qrrkzadmIOB22KffYxOlrvrUjblb5zYFqpreqih9IKZcjqBGUtNP0AY0wxQW8PtWhV-ZDbLVIAEfz9OfvaPB

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        //dd($result);
        curl_close($ch);
        return true;
    }
    public function setNotification($title,$body,$fk_receiver_id,$fk_sender_id)
    {
            $response = new \stdClass();
            $notification = new Notification();
            $notification->fk_receiver_id = $fk_receiver_id;
            $notification->fk_sender_id = $fk_sender_id;
            $notification->title = $title;
            $notification->body = $body;
            $notification->save();
            
            $response->status = 'success';
            $response->code = 200;
            $response->message = 'notification sent successful';
            $response->response = $notification;
            return response()->json($response);
           
    }
    
    
}
