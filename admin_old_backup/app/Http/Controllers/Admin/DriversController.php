<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Company;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Hash;
use Session;
use Illuminate\Support\Facades\Redirect;
class DriversController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::all();
        $drivers = User::where('user_type','driver')//->with(['company' => function ($query) {$query->select(['company_name']);}])
    ->get();
        //dd($companies[0]->company_name);
        //dd($drivers[0]->company->company_name);
        $data['companies'] = $companies;
        $data['drivers'] = $drivers;
        return view('admin.drivers.index')->with($data,$drivers);//,$companies);//compact(['companies' => $companies]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.drivers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);
        if ($validator->fails()) 
        {
            return redirect('drivers-management')->with('error',response()->json([
                'type' =>'error',
                'msg' => $validator->getMessageBag ()->toArray(),
            ]));
        
            // return response()->json([
            //     'type' =>'error',
            //     'msg' => $validator->getMessageBag ()->toArray(),
            // ]); 
        } else {
            
            // if ($request->hasFile('image_url')) {
            // $filenameWithExt = $request->file('image_url')->getClientOriginalName();
            // $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // $extension = $request->file('image_url')->getClientOriginalExtension();
            // $filenameToStore = $filename . '_' . time() . '.' . $extension;
            // $path = $request->file('image_url')->move('images/job_categories', $filenameToStore);
        	// } else {
            // $filenameToStore = 'noimage.jpg';
        	// }
            $company_exist = Company::where('company_name',$request->company)->first();
            if($company_exist)
            {
                $driver = new User();
                $driver->name = $request->name;
                $driver->email = $request->email;
                //$driver->image_url = $path;//$filenameToStore;
                $driver->phone_no = $request->phone;
                $driver->fk_company_id = $company_exist->id;
                $driver->user_type = "driver";
                $driver->password = Hash::make($request->password);
                $driver->status = "Active";
                $driver->save();
             return redirect('drivers-management')->with('success','Driver Added successfully!');
                
            }
            else
            {
                return redirect('drivers-management')->with('error','Company Not Found!');
            }
            
        }   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);
        if ($validator->fails()) 
        {
            return response()->json([
                'type' =>'error',
                'msg' => $validator->getMessageBag ()->toArray(),
            ]); 
        } 
        else 
        {
            $company_exist = Company::where('company_name',$request->company)->first();
            if($company_exist)
            {
                $driver  = User::where('id', $id)->first();
                $driver->name = $request->name;
                $driver->email = $request->email;
                $driver->phone_no = $request->phone;
                $driver->fk_company_id = $company_exist->id;
                //->where('destination', 'San Diego')
                //User::where('id', $request->input('id'))
                //->update($driver);
                $driver->save();
                return redirect('drivers-management')->with('success','Driver Updated successfully!');
                //->update(['delayed' => 1]);
            }
            else
            {
               return redirect('drivers-management')->with('error','Company Not Found!');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $exist = User::where('id',$id)->first();
        if($exist)
        {
            $exist->delete();
            return redirect('drivers-management')->with('success','Driver Deleted successfully!');
        }
        else
        {
            return redirect('drivers-management')->with('error','Driver Not Found!');
        }
    }
}
