<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Mobilego Login Page</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="csrf-token" content="{{ Session::token() }}"> 
	<meta content="Coderthemes" name="author" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<!-- App favicon -->
	<link href="{{asset('public/assets/css/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
	<link href="{{asset('public/assets/css/bootstrap-latest.min.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('public/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />

	<!-- Vendor js -->
	<script src="{{ URL::asset('public/assets/js/vendor.min.js')}}"></script>

	<!-- App js -->
	<script src="{{ URL::asset('public/assets/js/app.min.js')}}"></script>
	<script src="{{ URL::asset('public/assets/js/sweetalert2.min.js')}}"></script>
	<style>
		.form-control-custom {
			border: none;
			border-bottom: 1px solid #b1bbc4;;

			width: 100%;
			padding: 20px 10px;
		}
		.btn-success-custom {
			background: #40AC56;
		}
		.checkbox-info-go input[type=checkbox]:checked+label::before {
			background-color: #40AC56;
			border-color: #40AC56;
		}
		.form-control-custom::-webkit-input-placeholder { /* WebKit, Blink, Edge */
			color:    #000;
		}
		.form-control-custom:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
			color:    #000;
			opacity:  1;
		}
		.form-control-custom::-moz-placeholder { /* Mozilla Firefox 19+ */
			color:    #000;
			opacity:  1;
		}
		.form-control-custom:-ms-input-placeholder { /* Internet Explorer 10-11 */
			color:    #000;
		}
		.form-control-custom::-ms-input-placeholder { /* Microsoft Edge */
			color:    #000;
		}

		.form-control-custom::placeholder { /* Most modern browsers support this now. */
			color:    #000;
		}
		.card-custom {
			-webkit-box-shadow: -1px -1px 23px -16px rgba(0,0,0,0.75);
			-moz-box-shadow: -1px -1px 23px -16px rgba(0,0,0,0.75);
			box-shadow: -1px -1px 23px -16px rgba(0,0,0,0.75);
			background: #f9f9f9;

		}
		.card-custom .card-body {
			padding: 0;
		}
		.p-4-custom {
			padding: 3rem;
		}
		.header-custom{
			background: #40AC56;
			color: #fff;
		}
		.custom-color {
			color:#019a76;
		}
		.payment-method {
			background: #fff;
		}
		.new-payment {
			
			padding: 15px 0;
			margin: 5% 0;
		}
		.new-payment a {
			font-size: 16px;
			font-weight: 500;
		}
		.payment-method-list {
			-webkit-box-shadow: -1px -1px 23px -16px rgba(0,0,0,0.75);
			-moz-box-shadow: -1px -1px 23px -16px rgba(0,0,0,0.75);
			box-shadow: -1px -1px 23px -16px rgba(0,0,0,0.75);
			background: #fcfcfc;
			border-radius: 5px;
		}
		.payment-method-list p{
			margin: 3% 0 !important;
		}
		@media screen and (max-width: 401px) {
			.header-custom {
				font-size: 20px;
			}
			.fa-lg {
				font-size: 0.9em;
			}	
		}
		@media screen and (max-width: 348px) {
			.header-custom {
				font-size: 16px;
			}
			
		}
		@media screen and (max-width: 360px) {
			.new-payment a {
				font-size: 14px;
				
			}
			
		}
		
	</style>

</head>

@if (\Session::has('success'))
    <script>
        $(document).ready(function(){
        	Swal.fire({
        		title:"Successful",
        		type:"success",
        		showConfirmButton:!1,
        		html: ''
        	});
        
        });
    
    </script>
@endif
s


<body class="bg-white">

	<div class="account-pages mt-5 mb-5">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8 col-lg-6 col-xl-5">
					<div class="card card-custom">
						<div class="card-header header-custom">

							<div class="row align-items-center  ">
								<div class="col-2 text-left">
									<i class="fas fa-arrow-left fa-lg"></i>
								</div>
								<div class="col-8 text-center ">
									<h3 class="text-white font-weight-normal header-custom">Payment</h3>
								</div>
								<div class="col-2 text-right">
									<i class="far fa-bell fa-lg"></i>
								</div>
							</div>
							
						</div>

						<div class="card-body">
							<div class="new-payment payment-method">
								<div class="row">
									<div class="col-2 text-right ml-1 ml-lg-0 ml-md-0">

										<i class="custom-color fas fa-plus fa-2x"></i>
									</div>
									<div class="col-8 text-center">
										<a href="{{url('add-payment')}}" class="custom-color ">Add new payment method</a>
									</div>
								</div>
							</div>
							
							@foreach($cards_data as $card)
							
							<a href="javascript:void(0)" class="submit_payment" data-id ="{{$card['id']}}">
								<div class="payment-method-list payment-method p-2 m-3">
								
									<div class="row align-items-center">
										<div class="col-4">
											<img src="{{ URL::asset('public/assets/visa.png')}}" alt="" style="max-width: 75%; width: 100%;">
										</div>
										<div class="col-6">
											<p class="text-center m-0 font-weight-bold">Andre Yahoma</p>
											<p class="text-center m-0">{{$card['number']}}</p>
										</div>
									</div>
									
								</div>
							</a>
							@endforeach
							<a href="javascript:void(0)" class="submit_payment" data-id="paypal">
								<div class="payment-method-list payment-method p-2 m-3">

									<div class="row align-items-center">
										<div class="col-4">
											<p>Paypal</p>
										</div>
										<div class="col-6">
											<p class="text-center m-0 font-weight-bold">Noman Ghous</p>
											<p class="text-center m-0"></p>
										</div>
									</div>
									
								</div>
							</a>
							<form action="#">
								<input type="hidden" name="credit_card_id" value="" class="credit_card_id">
				
								


								<div class="form-group mb-0 text-center">
									<button class="btn btn-success btn-block btn-success-custom rounded make_payment" type="button"> Make Payment </button>
								</div>

							</form>
						
							<form class="w3-container w3-display-middle w3-card-4 w3-padding-16" method="POST" id="payment-form"
							action="{!! URL::to('paypal') !!}" style="visibility:hidden;">
							
							{{ csrf_field() }}
							
							
							<input class="w3-input w3-border" id="amount" type="text" name="amount" value="12"></p>
							<button class="w3-btn w3-blue">Make Payment</button>
						</form>



						</div> <!-- end card-body -->
					</div>
					<!-- end card -->


					<!-- end row -->

				</div> <!-- end col -->
			</div>
			<!-- end row -->
		</div>
		<!-- end container -->
	</div>
	<!-- end page -->




	
	<script>
		$(document).ready(function() {
			$('.submit_payment').on('click',function() {
				$('.credit_card_id').val($(this).data('id'));
			});
			$('.make_payment').on('click',function() {
				var credit_card_id = $('.credit_card_id').val();
				var transaction_id = $('.transaction_id').val();
				var check_credit_value = true;
				if(credit_card_id == '') {
					check_credit_value = false;
				}
				if(check_credit_value == false) {
					alert('choose payment');
				} else if(credit_card_id == 'paypal') {

					$('#payment-form').submit();

				} else {
					$.ajax({
						url: "{{route('payment.make')}}",
						type : 'post',
						data: {
							card_id: credit_card_id,
							'_token': $('meta[name=csrf-token]').attr('content')
						},
						dataType: "json",
						success: function(data ) {
		var amount = data.transactions[0].related_resources[0].sale.amount.total;
		var trans_id = data.transactions[0].related_resources[0].sale.id;
    						$.ajax({
    						url: "{{route('save.order')}}",
    						type : 'post',
    						data: {
    							amount: amount,
    							trans_id: trans_id,
    							'_token': $('meta[name=csrf-token]').attr('content')
    						},
    						dataType: "json",
    						success: function(data ) {
    						    
    						    
    						    
    							Swal.fire({
    								title:"Successful",
    								type:"success",
    								showConfirmButton:!1,
    								html: ''
    							});
    				
    				
    						},
    					});
						    
						    
						    
				
				
						},
					});
				}

			});
		});
	</script>
</body>
</html>