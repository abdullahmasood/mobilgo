@extends('admin-layout.content')


@section('content')

	<div class="row">
		 <div class="col-lg-12">
            <div class="ibox ">

		 		<div class="ibox-content">

			 		<div class="table-responsive mt-3">
			 		
				 			<div class="col-12">
				 				<h2 class="font-weight-bold float-left">Reviews Mangement</h2>
				 			</div>
				 			<div class="col-12 ">
				 				
					 			<button class="btn btn-default float-right mr-5">Export</button>
					 			
				 			</div>
			 			
			 			<div class="clearfix"></div>
			 			<table class="table table-striped  table-hover dataTables-admin" >
			 				<thead>
			 					<tr>
			 						<th>Trip ID</th>
			 						<th>Booking Number</th>
			 						<th>Driver Name (Rating)</th>
			 						<th>Rider name</th>
			 						<th>Rider rating</th>
			 						<th>Date</th>
			 						<th>Comment</th>
			 					</tr>
			 				</thead>
			 				<tbody>
			 					<tr class="gradeX">
			 						<td class="center">345</td>
			 						<td class="center">3254</td>
			 						<td class="center">Andre dabi (4.5)</td>
			 						<td class="center">James Gunn</td>
			 						<td class="center">4.9</td>
			 						<td class="center">28th July 2019 11:32pm</td>
			 						<td class="center">Excellent driving</td>
			 					</tr>
			 				</tbody>
			 			</table>
			 			
			 			
			 		</div>
		 		</div>
		 	</div>
		</div>
	</div>

 @endsection
 @section('script-dashboard')

 <script>
        $(document).ready(function(){
            $('.dataTables-admin').DataTable({
            	"lengthChange": false,
            	"searching": false,
            	"bInfo" : false,
            	"bSort" : false,

                responsive: true,
               
				language: {
					paginate: {
						next: '<i class="fas fa-angle-right fa-lg"></i>',
						previous: '<i class="fas fa-angle-left fa-lg"></i>'  
					}
				}

            });

        });

    </script>
 @stop