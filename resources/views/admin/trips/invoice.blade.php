@extends('admin-layout.content')


@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">

            <div class="ibox-content">

                <div class="table-responsive mt-3">

                    <div class="col-12">
                        <h2 class="font-weight-bold float-left">Trip Invoice</h2>
                    </div>
                    <div class="col-12 ">
                        <table class="table table-striped  table-hover dataTables-admin">
                            <tr>
                                <td>Booking Number</td>
                                <td>{{$trips->booking_no}}</td>
                            </tr>

                            <tr>
                                <td>Destination From</td>
                                <td>{{$trips->destination_from}}</td>
                            </tr>

                            <tr>
                                <td>Destination To</td>
                                <td>{{$trips->destination_to}}</td>
                            </tr>

                            
                            <tr>
                                <td>Destination From Latitude,Longitude</td>
                                <td>{{$trips->destination_from_lat}},{{$trips->destination_from_lng}}</td>
                            </tr>

                            <tr>
                                <td>Destination To Latitude,Longitude</td>
                                <td>{{$trips->destination_to_lat}},{{$trips->destination_to_lng}}</td>
                            </tr>

                            <tr>
                                <td>Trip Date</td>
                                <td>{{$trips->trip_date}}</td>
                            </tr>

                            <tr>
                                <td>Trip Started At</td>
                                <td>{{$trips->trip_started_at}}</td>
                            </tr>

                            <tr>
                                <td>Trip Ended At</td>
                                <td>{{$trips->trip_ended_at}}</td>
                            </tr>

                            <tr>
                                <td>Trip Distance</td>
                                <td>{{$trips->trip_distance}}</td>
                            </tr>

                            <tr>
                                <td>Fare</td>
                                <td>{{$trips->fare}}</td>
                            </tr>

                            <tr>
                                <td>Trip Status</td>
                                <td>{{$trips->trip_status}}</td>
                            </tr>

                            <tr>
                                <td>Trip Category</td>
                                <td>{{$trips->trip_category}}</td>
                            </tr>

                            <tr>
                                <td>Driver Name</td>
                                <td>{{$trips->driver->name}}</td>
                            </tr>
                            <tr>
                                <td>Driver Name</td>
                                <td>{{$trips->driver->name}}</td>
                            </tr>
                            <tr>
                                <td>Rider Name</td>
                                <td>{{$trips->rider->name}}</td>
                            </tr>
                            <tr>
                                <td>Payment Status</td>
                                <td>{{$trips->payment_status}}</td>
                            </tr>

                        </table>
                    </div>

                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script-dashboard')

<script>
    $(document).ready(function(){
            $('.dataTables-admin').DataTable({
            	"lengthChange": false,
            	"searching": false,
            	"bInfo" : false,
            	"bSort" : false,

                responsive: true,
               
				language: {
					paginate: {
						next: '<i class="fas fa-angle-right fa-lg"></i>',
						previous: '<i class="fas fa-angle-left fa-lg"></i>'  
					}
				}

            });

        });

</script>
@stop