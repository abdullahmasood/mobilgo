@extends('admin-layout.content')

@section('sidebar')



@section('content')

<div class="col-md-12">

    <!--begin::Portlet-->
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Add Driver
                </h3>
            </div>
        </div>
                                   
        <!--begin::Form-->
        <form  action="{{route('drivers-management.store')}}" method="post" enctype="multipart/form-data" class="kt-form ajax-form" data-cb="job_category_add"><!-- Form row -->
            {{csrf_field()}}
            <div class="kt-portlet__body">
                <div class="form-group">
                    <label>Name:</label>
                    <input type="text" class="form-control" placeholder="Enter full name" name="name">
                </div>
                <div class="form-group">
                    <label for="sel1">Select Company:</label>
                    <select class="form-control" id="sel1">
                        <option>Logixcess</option>
                        <option>NetSol</option>
                        <option>Systems Limited</option>
                        <option>ArbiSoft</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Email:</label>
                    <input type="email" class="form-control" placeholder="Enter Email Address" name="email">
                </div>
                <div class="form-group">
                    <label>Phone Number:</label>
                    <input type="text" class="form-control" placeholder="Enter Phone Number" name="phone">
                </div>
                <!-- <div class="form-group">
						<label>Job Category Image</label>
						<input type="file" class="form-control form-control-line" name="image_url" required="required"> 
						
					</div> -->
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <input type="submit" value="Submit" class="btn btn-primary"/> 
                </div>
            </div>
        </form>

        <!--end::Form-->
    </div>
</div>

<!--end::Portlet-->

<!--begin::Portlet-->




@endsection
@section('script-dashboard')

 <script>
        $(document).ready(function(){
            $('.dataTables-admin').DataTable({
            	"lengthChange": false,
            	"searching": false,
            	"bInfo" : false,
            	"bSort" : false,

                responsive: true,
               
				language: {
					paginate: {
						next: '<i class="fas fa-angle-right fa-lg"></i>',
						previous: '<i class="fas fa-angle-left fa-lg"></i>'  
					}
				}

            });

        });

    </script>
 @stop