@extends('admin-layout.content')


@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="ibox ">

			<div class="ibox-content">

				<div class="table-responsive mt-3">

					<div class="col-12">
						<h2 class="font-weight-bold float-left">Driver Mangement</h2>
					</div>
					<div>
						@include('../../admin-layout/flash-message')
					</div>
					<div class="col-12 ">
						<button data-toggle="modal" 
						data-target="#driversModal" class="btn btn-success float-right">Add Driver
					</button> 
				<!-- 	<a href="{{route('drivers-management.create')}}" class="btn btn-success float-right">
						<i class="la la-plus"></i>
								Add Driver
								</a> -->
							<!-- 	<select class="btn btn-default float-right mr-5">
									<option>Options</option>
								</select>
 -->
								<button class="btn btn-default float-right mr-5">Export</button>

								
							</div>

							<div class="clearfix"></div>
							<table class="table table-striped  table-hover dataTables-admin" >
								<thead>
									<tr>
										<th>Name</th>
										<th>Company Name</th>
										<th>Email</th>
										<th>Phone number</th>
										<th>Sign up date</th>
										<th>Wallet balance</th>
										<th>Cab count</th>
										<th>Status</th>
										<th></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									@foreach ($drivers as $driver)
									<tr class="gradeX">
										<td class="center">{{$driver->name}}</td>
										<td class="center">
											@if(is_null($driver->company))
											{{ 'Empty' }} 
											@else
											{{ $driver->company->company_name }}
											@endif
										</td>
										<td class="center">{{$driver->email}}</td>
										<td class="center">{{$driver->phone_no}}</td>
										<td class="center">{{$driver->created_at}}</td>
										<td class="center">0</td>
										<td class="center">1</td>
										<td class="center"><span class="label label-primary">{{$driver->status}}</span></td>
										<td class="center"><i class="fas fa-pen" data-toggle="modal" data-target="#driversEditModal" onClick="return driverInfo({{$driver}})">
										</i> 
									</td>
									<td class="center"><i class="fa fa-times" data-toggle="modal" data-target="#deleteModal" onClick="return deleteDriver({{$driver->id}})"></i></td>
								</tr>
								@endforeach
							</tbody>
						</table>


					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="driversModal" 
	tabindex="-1" role="dialog" 
	aria-labelledby="driversModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" 
				id="driversModalLabel">Add Driver</h4>
			</div>
			<div class="modal-body">
				<div class="col-md-12">

					<!--begin::Portlet-->
					<div class="kt-portlet">

						<!--begin::Form-->
						<form  action="{{route('drivers-management.store')}}" method="post" enctype="multipart/form-data" class="kt-form ajax-form" data-cb="job_category_add"><!-- Form row -->
							{{csrf_field()}}
							<div class="kt-portlet__body">
								<div class="form-group">
									<label>Name:</label>
									<input type="text" class="form-control" placeholder="Enter full name" name="name">
								</div>
								<div class="form-group">
									<label for="sel1">Select Company:</label>

									<select name="company" class="form-control" id="sel1">
												<!-- <option>Logixcess</option>
												<option>NetSol</option>
												<option>Systems Limited</option>
												<option>ArbiSoft</option> -->
												<option value="">Select Company</option>
												@foreach ($companies as $company)

												<option>{{$company->company_name}}</option>
												@endforeach

												
											</select>
										</div>
										<div class="form-group">
											<label>Email:</label>
											<input type="email" class="form-control" placeholder="Enter Email Address" name="email">
										</div>
										<div class="form-group">
											<label>Password:</label>
											<input type="password" class="form-control" placeholder="Enter Password" name="password">
										</div>
										<div class="form-group">
											<label>Phone Number:</label>
											<input type="text" class="form-control" placeholder="Enter Phone Number" name="phone">
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<input type="submit" value="Save" class="btn btn-primary"/> 
											<button type="button" 
											class="btn btn-default" 
											data-dismiss="modal">Close</button>
										</div>
									</div>
								</form>

								<!--end::Form-->
							</div>
						</div>

					</div>
			<!-- <div class="modal-footer">
				<button type="button" 
				class="btn btn-default" 
				data-dismiss="modal">Close</button>
				<span class="pull-right">
				<button type="button" class="btn btn-primary">
					Save
				</button>
				</span>
			</div> -->
		</div>
	</div>
</div>



<!--Edit-->
<div class="modal fade" id="driversEditModal" 
tabindex="-1" role="dialog" 
aria-labelledby="driversEditModalLabel">
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title" 
			id="driversEditModalLabel">Edit Driver</h4>
		</div>
		<div class="modal-body">
			<div class="col-md-12">

				<!--begin::Portlet-->
				<div class="kt-portlet">
					<!--route('drivers-management.update_driver  -->
					<!--begin::Form-->
					<form id="update_form"  method="POST" enctype="multipart/form-data" class="kt-form ajax-form" data-cb="job_category_add"><!-- Form row -->
						{{csrf_field()}}
						@method('PUT')
						<input type="hidden" name="id" id="id">
						<!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->
						<div class="kt-portlet__body">
							<div class="form-group">
								<label>Name:</label>
								<input type="text" class="form-control" placeholder="Enter full name" name="name" id="name">
							</div>
							<div class="form-group">
								<label for="sel1">Select Company:</label>

								<select name="company" class="form-control" id="company">
												<!-- <option>Logixcess</option>
												<option>NetSol</option>
												<option>Systems Limited</option>
												<option>ArbiSoft</option> -->
												<option value="">Select Company</option>
												@foreach ($companies as $company)
												<option>{{$company->company_name}}</option>
												@endforeach

												
											</select>
										</div>
										<div class="form-group">
											<label>Email:</label>
											<input type="email" class="form-control" placeholder="Enter Email Address" name="email" id="email">
										</div>
										<div class="form-group">
											<label>Password:</label>
											<input type="password" class="form-control" placeholder="Enter Password" name="password" id="password">
										</div>
										<div class="form-group">
											<label>Phone Number:</label>
											<input type="text" class="form-control" placeholder="Enter Phone Number" name="phone" id="phone">
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<input type="submit" value="Update" class="btn btn-primary"/> 
											<button type="button" 
											class="btn btn-default" 
											data-dismiss="modal">Close</button>
										</div>
									</div>
								</form>

								<!--end::Form-->
							</div>
						</div>

					</div>
			<!-- <div class="modal-footer">
				<button type="button" 
				class="btn btn-default" 
				data-dismiss="modal">Close</button>
				<span class="pull-right">
				<button type="button" class="btn btn-primary">
					Save
				</button>
				</span>
			</div> -->
		</div>
	</div>
</div>



<!-- Delete Modal -->
<!-- Modal HTML -->
<div id="deleteModal" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<!-- <div class="icon-box">
					<i class="glyphicon glyphicon-trash"></i>
				</div>				 -->
				<h2 class="modal-title">Are you sure ?</h4>	
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<input type="hidden" class="form-control" name="input_del" id="input_del">
					<p>Do you really want to delete these records? This process cannot be undone.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
					<!-- action="{{ route('drivers-management.destroy', 2) }}" -->
					<form id="delete_form"  method="POST">
						{{ method_field('DELETE') }}
						{{ csrf_field() }}
						<!-- {{ csrf_field() }} -->
						<input type="submit" value="Delete" class="btn btn-danger btn-block">
					</form>

					<!-- <a href="{{ URL::to('drivers-management/' . '2') }}" class="btn btn-danger">Delete</a> -->

					<!-- <button href="{{ route('drivers-management.destroy',1) }}" type="button" class="btn btn-danger">Delete</button> -->
				</div>
			</div>
		</div>


		@endsection
		@section('script-dashboard')

		<script>
			$(document).ready(function(){
				$('.dataTables-admin').DataTable({
					"lengthChange": false,
					"searching": false,
					"bInfo" : false,
					"bSort" : false,

					responsive: true,

					language: {
						paginate: {
							next: '<i class="fas fa-angle-right fa-lg"></i>',
							previous: '<i class="fas fa-angle-left fa-lg"></i>'  
						}
					}

				});

			});

			function driverInfo(data){

				//console.log(data);
				$('.modal-title').html('Edit Driver');
				$("#name").val(data.name);
				$("#email").val(data.email);
				$("#password").val(data.password);
				$('#phone').val(data.phone_no);
				$('#id').val(data.id);
				if(data.fk_company_id != null){
				$('#company').val(data.company.company_name);
				}

				var url = '{{ route("drivers-management.update", ":slug") }}';
				url = url.replace(':slug', data.id);
				$('#update_form').attr('action', url);


				$('#driversEditModal').modal('show');
			}
			function deleteDriver(slug){
				$('#input_del').val(slug);
				var url = '{{ route("drivers-management.destroy", ":slug") }}';
				url = url.replace(':slug', slug);
				$('#delete_form').attr('action', url);
				$('#deleteModal').modal('show');
			}
		</script>
		@stop