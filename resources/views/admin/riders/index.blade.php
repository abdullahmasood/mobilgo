@extends('admin-layout.content')


@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="ibox ">

			<div class="ibox-content">

				<div class="table-responsive mt-3">

					<div class="col-12">
						<h2 class="font-weight-bold float-left">Rider Mangement</h2>
					</div>
					<div>
						@include('../../admin-layout/flash-message')
					</div>
					<div class="col-12 ">

						<button class="btn btn-default float-right mr-5">Export</button>

					</div>

					<div class="clearfix"></div>
					<table class="table table-striped  table-hover dataTables-admin">
						<thead>
							<tr>
								<th>Name</th>
								<th>Email</th>
								<th>Phone Number</th>
								<th>Sign up date</th>
								<th>Money Spent</th>
								<th>Trip count</th>
								<th>History</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($riders as $rider)
							<tr class="gradeX">
								<td class="center">{{$rider->name}}</td>
								<td class="center">{{$rider->email}}</td>
								<td class="center">{{$rider->phone_no}}</td>
								<td class="center">{{$rider->created_at}}</td>
								<td class="center">$0</td>
								<td class="center">0</td>
								<td class="center">
									<a href="{{ url('rider-history') }}/{{$rider->id}}">
										<i class="fa fa-file-alt"></i>
									</a>
								</td>
								<td class="center"><i class="fa fa-times" data-toggle="modal" data-target="#deleteModal"
										onClick="return deleteRider({{$rider->id}})"></i></td>
							</tr>
							@endforeach
						</tbody>
					</table>


				</div>
			</div>
		</div>
	</div>
</div>


<!-- Delete Modal -->
<!-- Modal HTML -->
<div id="deleteModal" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<!-- <div class="icon-box">
					<i class="glyphicon glyphicon-trash"></i>
				</div>				 -->
				<h2 class="modal-title">Are you sure ?</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<input type="hidden" class="form-control" name="input_del" id="input_del">
				<p>Do you really want to delete these records? This process cannot be undone.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
				<!-- action="{{ route('drivers-management.destroy', 2) }}" -->
				<form id="delete_form" method="POST">
					{{ method_field('DELETE') }}
					{{ csrf_field() }}
					<!-- {{ csrf_field() }} -->
					<input type="submit" value="Delete" class="btn btn-danger btn-block">
				</form>

				<!-- <a href="{{ URL::to('drivers-management/' . '2') }}" class="btn btn-danger">Delete</a> -->

				<!-- <button href="{{ route('drivers-management.destroy',1) }}" type="button" class="btn btn-danger">Delete</button> -->
			</div>
		</div>
	</div>

	@endsection
	@section('script-dashboard')

	<script>
		$(document).ready(function(){
            $('.dataTables-admin').DataTable({
            	"lengthChange": false,
            	"searching": false,
            	"bInfo" : false,
            	"bSort" : false,

                responsive: true,
               
				language: {
					paginate: {
						next: '<i class="fas fa-angle-right fa-lg"></i>',
						previous: '<i class="fas fa-angle-left fa-lg"></i>'  
					}
				}

            });

        });


        function deleteRider(slug){
				$('#input_del').val(slug);
				var url = '{{ route("riders-management.destroy", ":slug") }}';
				url = url.replace(':slug', slug);
				$('#delete_form').attr('action', url);
				$('#deleteModal').modal('show');
			}

	</script>
	@stop