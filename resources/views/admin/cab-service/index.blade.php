@extends('admin-layout.content')


@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">

            <div class="ibox-content">

                <div class="table-responsive mt-3">

                    <div class="col-12">
                        <h2 class="font-weight-bold float-left">Cab Model Mangement</h2>
                    </div>
                    <div>
                        @include('../../admin-layout/flash-message')
                    </div>
                    <!-- <div class="col-12 ">
				 				
					 			<button class="btn btn-default float-right mr-5">Export</button>
					 			
				 			</div> -->
                    <div class="col-12 ">
                        <div class="clearfix"></div>
                        <table class="table table-striped  table-hover dataTables-admin">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Service Description</th>
                                    <th>Rider Space</th>
                                    <th>Price Per Kilometer</th>
                                    <th>Created At</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($service)
                                @foreach ($service as $item)
                                <tr>
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->service_description}}</td>
                                    <td>{{$item->rider_space}}</td>
                                    <td>{{$item->price_per_kilometer}} </td>
                                    <td>{{$item->created_at}}</td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>


    @endsection
    @section('script-dashboard')

    <script>
        $(document).ready(function(){
            $('.dataTables-admin').DataTable({
            	"lengthChange": false,
            	"searching": false,
            	"bInfo" : false,
            	"bSort" : false,

                responsive: true,
               
				language: {
					paginate: {
						next: '<i class="fas fa-angle-right fa-lg"></i>',
						previous: '<i class="fas fa-angle-left fa-lg"></i>'  
					}
				}

            });

        });

        function modelInfo(data){
        	console.log(data);
        	$("#name").val(data.car_model_name);
        	$('#id').val(data.id);
        	var url = '{{ route("cabs-model-management.update", ":slug") }}';
				url = url.replace(':slug', data.id);
				$('#update_form').attr('action', url);


				$('#cabMEditModal').modal('show');
        }


        function deleteCarModel(slug){
				$('#input_del').val(slug);
				var url = '{{ route("cabs-model-management.destroy", ":slug") }}';
				url = url.replace(':slug', slug);
				$('#delete_form').attr('action', url);
				$('#deleteModal').modal('show');
			}

    </script>
    @stop