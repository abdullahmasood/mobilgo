@extends('admin-layout.content')


@section('content')

	<div class="row">
		 <div class="col-lg-12">
            <div class="ibox ">

		 		<div class="ibox-content">

			 		<div class="table-responsive mt-3">
			 		
				 			<div class="col-12">
				 				<h2 class="font-weight-bold float-left">Cab Model Mangement</h2>
				 			</div>
				 			<div>
						@include('../../admin-layout/flash-message')
					</div>
				 			<!-- <div class="col-12 ">
				 				
					 			<button class="btn btn-default float-right mr-5">Export</button>
					 			
				 			</div> -->
				 			<div class="col-12 ">
						<button data-toggle="modal" 
						data-target="#cabMModal" class="btn btn-success float-right">Add Cab Model
					</button>
			 			
			 			<div class="clearfix"></div>
			 			<table class="table table-striped  table-hover dataTables-admin" >
			 				<thead>
			 					<tr>
			 						<th>#</th>
			 						<th>Cab Model</th>
			 						<th>Actions</th>
			 					</tr>
			 				</thead>
			 				<tbody>
			 					@if(sizeof($data) > 0 )
			 						<?php $n=0; ?>
			 					  @foreach ($data as $values)
			 					  <?php $n++; ?>
			 					<tr class="gradeX">
			 						<td class="center">{{$n}}</td>
			 						<td class="center">{{$values->car_model_name}}</td>
			 						<td class="center">
			 						<i class="fas fa-pen" data-toggle="modal" data-target="#cabMEditModal" onClick="return modelInfo({{$values}})">
			 							</i>&nbsp;
			 							<i class="fa fa-times" data-toggle="modal" data-target="#deleteCarModel" onClick="return deleteCarModel({{$values->id}})"></i></td>
			 						
			 					</tr>
			 					@endforeach
			 					@endif
			 				</tbody>
			 			</table>
			 			
			 			
			 		</div>
		 		</div>
		 	</div>
		</div>
	</div>



	<div class="modal fade" id="cabMModal" 
	tabindex="-1" role="dialog" 
	aria-labelledby="cabMModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" 
				id="cabMModalLabel">Add Cab Model</h4>
			</div>
			<div class="modal-body">
				<div class="col-md-12">

					<!--begin::Portlet-->
					<div class="kt-portlet">

						<!--begin::Form-->
						<form  action="{{route('cabs-model-management.store')}}" method="post" enctype="multipart/form-data" class="kt-form ajax-form" data-cb="job_category_add"><!-- Form row -->
							{{csrf_field()}}
							<div class="kt-portlet__body">
								<div class="form-group">
									<label>Name:</label>
									<input type="text" class="form-control" placeholder="Enter cab model name" name="name">
								</div>
	
										
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<input type="submit" value="Save" class="btn btn-primary"/> 
											<button type="button" 
											class="btn btn-default" 
											data-dismiss="modal">Close</button>
										</div>
									</div>
								</form>

								<!--end::Form-->
							</div>
						</div>

					</div>
			<!-- <div class="modal-footer">
				<button type="button" 
				class="btn btn-default" 
				data-dismiss="modal">Close</button>
				<span class="pull-right">
				<button type="button" class="btn btn-primary">
					Save
				</button>
				</span>
			</div> -->
		</div>
	</div>
</div>


<!-- Edit model -->
	<div class="modal fade" id="cabMEditModal" 
	tabindex="-1" role="dialog" 
	aria-labelledby="cabMEditModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" 
				id="cabMEditModal">Add Cab Model</h4>
			</div>
			<div class="modal-body">
				<div class="col-md-12">

					<!--begin::Portlet-->
					<div class="kt-portlet">

						<!--begin::Form-->
						<form id="update_form"  method="post" enctype="multipart/form-data" class="kt-form ajax-form"><!-- Form row -->
							{{csrf_field()}}
							@method('PUT')
							<input type="hidden" name="id" id="id">
							<div class="kt-portlet__body">
								<div class="form-group">
									<label>Name:</label>
									<input type="text" class="form-control" placeholder="Enter cab model name" id="name" name="name">
								</div>
	
										
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<input type="submit" value="Save" class="btn btn-primary"/> 
											<button type="button" 
											class="btn btn-default" 
											data-dismiss="modal">Close</button>
										</div>
									</div>
								</form>

								<!--end::Form-->
							</div>
						</div>

					</div>
			<!-- <div class="modal-footer">
				<button type="button" 
				class="btn btn-default" 
				data-dismiss="modal">Close</button>
				<span class="pull-right">
				<button type="button" class="btn btn-primary">
					Save
				</button>
				</span>
			</div> -->
		</div>
	</div>
</div>

<!-- End of edit model -->


<!-- Delete Modal -->
<!-- Modal HTML -->
<div id="deleteModal" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<!-- <div class="icon-box">
					<i class="glyphicon glyphicon-trash"></i>
				</div>				 -->
				<h2 class="modal-title">Are you sure ?</h4>	
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<input type="hidden" class="form-control" name="input_del" id="input_del">
					<p>Do you really want to delete these records? This process cannot be undone.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
					<!-- action="{{ route('drivers-management.destroy', 2) }}" -->
					<form id="delete_form"  method="POST">
						{{ method_field('DELETE') }}
						{{ csrf_field() }}
						<!-- {{ csrf_field() }} -->
						<input type="submit" value="Delete" class="btn btn-danger btn-block">
					</form>

					<!-- <a href="{{ URL::to('drivers-management/' . '2') }}" class="btn btn-danger">Delete</a> -->

					<!-- <button href="{{ route('drivers-management.destroy',1) }}" type="button" class="btn btn-danger">Delete</button> -->
				</div>
			</div>
		</div>

 @endsection
 @section('script-dashboard')

 <script>
        $(document).ready(function(){
            $('.dataTables-admin').DataTable({
            	"lengthChange": false,
            	"searching": false,
            	"bInfo" : false,
            	"bSort" : false,

                responsive: true,
               
				language: {
					paginate: {
						next: '<i class="fas fa-angle-right fa-lg"></i>',
						previous: '<i class="fas fa-angle-left fa-lg"></i>'  
					}
				}

            });

        });

        function modelInfo(data){
        	console.log(data);
        	$("#name").val(data.car_model_name);
        	$('#id').val(data.id);
        	var url = '{{ route("cabs-model-management.update", ":slug") }}';
				url = url.replace(':slug', data.id);
				$('#update_form').attr('action', url);


				$('#cabMEditModal').modal('show');
        }


        function deleteCarModel(slug){
				$('#input_del').val(slug);
				var url = '{{ route("cabs-model-management.destroy", ":slug") }}';
				url = url.replace(':slug', slug);
				$('#delete_form').attr('action', url);
				$('#deleteModal').modal('show');
			}

    </script>
 @stop