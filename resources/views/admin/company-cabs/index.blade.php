@extends('admin-layout.content')


@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="ibox ">

			<div class="ibox-content">

				<div class="table-responsive mt-3">

					<div class="col-12">
						<h2 class="font-weight-bold float-left">Company Cab Mangement</h2>
					</div>
					<div>
						@include('../../admin-layout/flash-message')
					</div>
					<div class="col-12 ">
						<button data-toggle="modal" data-target="#companyModal" class="btn btn-success float-right">Add
							New Company</button>
					</div>

					<div class="clearfix"></div>
					<table class="table table-striped  table-hover dataTables-admin">
						<thead>
							<tr>
								<th class="center">Company Name</th>
								<th>Drivers</th>
								<th>Email</th>
								<th>Mobiles</th>
								<th>View/Edit Document</th>
								<th>Status</th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach ($companies as $company)
							<tr class="gradeX">
								<td class="center">{{$company->company_name}}</td>
								<td class="center">{{count($company->drivers)}}</td>
								<td class="center">{{$company->email}}</td>
								<td class="center">{{$company->mobile}}</td>
								<td class="center"><a href="{{ asset('documents/companies/'.$company->document_url) }}"> <i class="fas fa-file-alt"></i></a></td>
								<td class="center"><span class="label label-primary">{{$company->status}}</span></td>
								<td><i class="fas fa-pen" data-toggle="modal" data-target="#companyModal1"
										onClick="return promoInfo({{$company}})"></i>
										<i class="fa fa-times" data-toggle="modal" data-target="#deleteModal"
										 onClick="return deletePromo({{$company->id}})"></i>
							</tr>
							@endforeach
						</tbody>
					</table>


				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="companyModal" tabindex="-1" role="dialog" aria-labelledby="companyModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="companyModalLabel">Add Company</h4>
			</div>
			<div class="modal-body">
				<div class="col-md-12">

					<!--begin::Portlet-->
					<div class="kt-portlet">

						<!--begin::Form-->
						<form action="{{route('company-cabs-management.store')}}" method="post"
							enctype="multipart/form-data" class="kt-form ajax-form" data-cb="job_category_add">
							<!-- Form row -->
							{{csrf_field()}}
							<div class="kt-portlet__body">
								<div class="form-group form-inline">
									<label class="col-md-4 col-form-label">Company Name</label>
									<input type="text" class="col-md-7 form-control" placeholder="Enter company name"
										name="company_name">
								</div>
								<div class="form-group form-inline">
									<label class="col-md-4 col-form-label">Email</label>
									<input type="email" class="col-md-7 form-control" placeholder="Enter Email Address"
										name="email">
								</div>
								<div class="form-group form-inline">
									<label class="col-md-4 col-form-label">Mobile Number</label>
									<input type="text" class="col-md-7 form-control" placeholder="Enter Mobile Number"
										name="mobile">
								</div>
								<div class="form-group form-inline">
									<label class="col-md-4 col-form-label">Upload Document</label>
									<input type="file" class="col-md-7 form-control form-control-line"
										name="document_url" required="required">
								</div>
							</div>
							<div class="kt-portlet__foot">
								<div class="kt-form__actions text-center">
									<input type="submit" value="Save" class="btn btn-primary" />
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>
						</form>

						<!--end::Form-->
					</div>
				</div>

			</div>
			<!-- <div class="modal-footer">
				<button type="button" 
				class="btn btn-default" 
				data-dismiss="modal">Close</button>
				<span class="pull-right">
				<button type="button" class="btn btn-primary">
					Save
				</button>
				</span>
			</div> -->
		</div>
	</div>
</div>

<div class="modal fade" id="companyModal1" tabindex="-1" role="dialog" aria-labelledby="companyModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="companyModalLabel">Add Company</h4>
			</div>
			<div class="modal-body">
				<div class="col-md-12">

					<!--begin::Portlet-->
					<div class="kt-portlet">

						<!--begin::Form-->
						<form id="update_form"   method="POST"
							enctype="multipart/form-data" class="kt-form ajax-form" data-cb="job_category_add">
							<!-- Form row -->
							{{csrf_field()}}
							{{ method_field('PUT') }}
							<input type="hidden" name="id" id="id">
							<div class="kt-portlet__body">
								<div class="form-group form-inline">
									<label class="col-md-4 col-form-label">Company Name</label>
									<input type="text" class="col-md-7 form-control" id="name"
										placeholder="Enter company name" name="company_name">
								</div>
								<div class="form-group form-inline">
									<label class="col-md-4 col-form-label">Email</label>
									<input type="email" class="col-md-7 form-control" id="email"
										placeholder="Enter Email Address" name="email">
								</div>
								<div class="form-group form-inline">
									<label class="col-md-4 col-form-label">Mobile Number</label>
									<input type="text" class="col-md-7 form-control" id="mobile"
										placeholder="Enter Mobile Number" name="mobile">
								</div>
								<div class="form-group form-inline">
									<label class="col-md-4 col-form-label">Upload Document</label>
									<input type="file" class="col-md-7 form-control form-control-line"
										name="document_url" required="required">
								</div>
							</div>
							<div class="kt-portlet__foot">
								<div class="kt-form__actions text-center">
									<input type="submit" value="Update" class="btn btn-primary" />
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>
						</form>

						<!--end::Form-->
					</div>
				</div>

			</div>
			<!-- <div class="modal-footer">
				<button type="button" 
				class="btn btn-default" 
				data-dismiss="modal">Close</button>
				<span class="pull-right">
				<button type="button" class="btn btn-primary">
					Save
				</button>
				</span>
			</div> -->
		</div>
	</div>
</div>


<!-- Delete Modal -->
<!-- Modal HTML -->
<div id="deleteModal" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<!-- <div class="icon-box">
					<i class="glyphicon glyphicon-trash"></i>
				</div>				 -->
				<h2 class="modal-title">Are you sure ?</h4>	
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
			<input type="hidden" class="form-control" name="input_del" id="input_del">
				<p>Do you really want to delete these records? This process cannot be undone.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
				<form id="delete_form"  method="POST">
   				{{ method_field('DELETE') }}
				   {{ csrf_field() }}
   				<!-- {{ csrf_field() }} -->
   				<input type="submit" value="Delete" class="btn btn-danger btn-block">
				</form>
			</div>
		</div>
	</div>

@endsection
@section('script-dashboard')

<script>
	$(document).ready(function(){
            $('.dataTables-admin').DataTable({
            	"lengthChange": false,
            	"searching": false,
            	"bInfo" : false,
            	"bSort" : false,

                responsive: true,
               
				language: {
					paginate: {
						next: '<i class="fas fa-angle-right fa-lg"></i>',
						previous: '<i class="fas fa-angle-left fa-lg"></i>'  
					}
				}

            });

        });
		function promoInfo(data){
			$('.modal-title').html('Edit Company Cab');
			$("#name").val(data.company_name);
			$("#email").val(data.email);
			$("#mobile").val(data.mobile);
			$("#id").val(data.id);
			var url = '{{ route("company-cabs-management.update", ":slug") }}';
			url = url.replace(':slug', data.id);
			$('#update_form').attr('action', url);
			
			
			$('#promosEditModal').modal('show');
		}

		function deletePromo(slug){
			$('.modal-title').html('Delete Company Cab');
			  $('#input_del').val(slug);
			  var url = '{{ route("company-cabs-management.destroy", ":slug") }}';
			  url = url.replace(':slug', slug);
			  $('#delete_form').attr('action', url);
			  $('#deleteModal').modal('show');
		  }

		  
</script>
@stop