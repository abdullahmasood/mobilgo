<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	
	<title>Mobilego Login23 Page</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta content="Coderthemes" name="author" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<!-- App favicon -->



	<link href="{{ URL::asset('public/assets/css/bootstrap-latest.min.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{ URL::asset('public/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />
	<style>
		.form-control-custom {
			border: none;
			border-bottom: 1px solid #b1bbc4;;

			width: 100%;
			padding: 20px 10px;
		}
		.btn-success-custom {
			background: #40AC56;
		}
		.checkbox-info-go input[type=checkbox]:checked+label::before {
			background-color: #40AC56;
			border-color: #40AC56;
		}
		.form-control-custom::-webkit-input-placeholder { /* WebKit, Blink, Edge */
			color:    #000;
		}
		.form-control-custom:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
			color:    #000;
			opacity:  1;
		}
		.form-control-custom::-moz-placeholder { /* Mozilla Firefox 19+ */
			color:    #000;
			opacity:  1;
		}
		.form-control-custom:-ms-input-placeholder { /* Internet Explorer 10-11 */
			color:    #000;
		}
		.form-control-custom::-ms-input-placeholder { /* Microsoft Edge */
			color:    #000;
		}

		.form-control-custom::placeholder { /* Most modern browsers support this now. */
			color:    #000;
		}
		.card-custom {
			-webkit-box-shadow: -1px -1px 23px -16px rgba(0,0,0,0.75);
			-moz-box-shadow: -1px -1px 23px -16px rgba(0,0,0,0.75);
			box-shadow: -1px -1px 23px -16px rgba(0,0,0,0.75);
		}
		.p-4-custom {
			padding: 3rem;
		}
		
	</style>

</head>

<body class="bg-white">

	<div class="account-pages mt-5 mb-5">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8 col-lg-6 col-xl-5">
					<div class="card card-custom">

						<div class="card-body  p-4-custom">

							<div class="text-center w-100 m-auto">
								<a href="index.html">
									<span><img src="{{ URL::asset('public/assets/img/logo.svg')}}" alt="user-image"  height="45"/></span>
								</a><p class="text-muted mb-4 mt-3">Welcome back! Please login to your accountss</p>
							</div>



							<form action="{{ route('login_admin') }}" method="POST">
                                @csrf
								<div class="form-group mb-3">
									<input class="form-control form-control-custom @error('email') is-invalid @enderror" type="email" name="email" id="emailaddress" required="" placeholder="Username">
									@error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                	@enderror
								</div>

								<div class="form-group mb-3">
									<input class="form-control form-control-custom @error('password') is-invalid @enderror" type="password" name="password" required="" id="password" placeholder="Password" required autocomplete="current-password">
									@error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                	@enderror
								</div>

								<div class="form-group mb-5">
									<div class="custom-control custom-checkbox checkbox-info checkbox-info-go float-left">
										<input type="checkbox" class="custom-control-input" id="checkbox-signin" {{ old('remember') ? 'checked' : '' }}>
										<label class="custom-control-label " for="checkbox-signin">Remember me</label>
									</div>
									<div class="float-right">
										<!-- <a href="pages-recoverpw.html" class="text-muted ml-1">Forgot password</a> -->
										@if (Route::has('password.request'))
										<a class="text-muted ml-1" href="{{ route('password.request') }}">
											{{ __('Forgot Your Password?') }}
										</a>
                                		@endif
									</div>
								</div>
								

								<div class="form-group mb-0 text-center">
									<button class="btn btn-success btn-block btn-success-custom" type="submit">  {{ __('Login') }} </button>
								</div>

							</form>

							<form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>

						</div> <!-- end card-body -->
					</div>
					<!-- end card -->

					
					<!-- end row -->

				</div> <!-- end col -->
			</div>
			<!-- end row -->
		</div>
		<!-- end container -->
	</div>
	<!-- end page -->




	<!-- Vendor js -->
	<script src="{{ URL::asset('public/assets/js/vendor.min.js')}}"></script>

	<!-- App js -->
	<script src="{{ URL::asset('public/assets/js/app.min.js')}}"></script>

</body>
</html>