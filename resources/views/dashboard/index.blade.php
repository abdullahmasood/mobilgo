@extends('admin-layout.content')

@section('content')
<div class="col-12">
	<h2 class="font-weight-bold">Dashboard Overview</h2>
</div>
<div class="row">
	<div class="col-lg-3">
		<div class="ibox">
			<div class="ibox-content ibox-content-custom ibox-color-1">
				<h5 class="m-b-md">Total Earning</h5>
				<h3 class="text-center font-weight-bold ">$0
				</h3>
				
			</div>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="ibox">
			<div class="ibox-content ibox-content-custom ibox-color-2">
				<h5 class="m-b-md">Total No of Drivers</h5>
				<h3 class="text-center font-weight-bold ">{{count($drivers)}}
				</h3>
				
			</div>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="ibox">
			<div class="ibox-content ibox-content-custom ibox-color-3">
				<h5 class="m-b-md">Total No of Company</h5>
				<h3 class="text-center font-weight-bold ">{{count($companies)}}
				</h3>
				
			</div>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="ibox">
			<div class="ibox-content ibox-content-custom ibox-color-4">
				<h5 class="m-b-md">Total No of Completed Rides</h5>
				<h3 class="text-center font-weight-bold text-black">0
				</h3>
				
			</div>
		</div>
	</div>

</div>
<div class="row">
	<div class="col-lg-6">
		<div class="ibox ">
			<div class="ibox-title">
				<h5>Rides Count</h5>
				
				<div class="row">
					<div class="col-lg-6">
						<canvas id="myChart"></canvas>
					</div>
					<div class="col-lg-6">
						<div id="js-legend" class="pieLegend"></div>
					</div>
				</div>
			</div>


			
			<div class="ibox-content"></div>

		</div>
	</div>


	<div class="col-lg-6">
		
		<div class="ibox ">
			<div class="ibox-title">
				<h5>Drivers</h5>

				<div class="row">
					<div class="col-lg-6">
						<canvas id="myCharts"></canvas>
					</div>
					<div class="col-lg-6">
						<div id="js-legend2" class="pieLegend"></div>
					</div>
				</div>
			</div>


			<div class="ibox-content"></div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="ibox ">

			<div class="ibox-content">
				<div class="table-responsive mt-3">

					<div class="col-12">
						<h4 class="font-weight-bold">Recent Trips</h4>
					</div>


					<div class="clearfix"></div>
					<table class="table table-striped  table-hover dataTables-admin" >
						<thead>
							<!--<tr>-->
								<!--	<th>Driver Name</th>-->
								<!--	<th>Cab ID</th>-->
								<!--	<th>Cab Type</th>-->
								<!--	<th>Destination (From)</th>-->
								<!--	<th>Destination (To)</th>-->
								<!--	<th>Time</th>-->
								<!--	<th>Trip Status</th>-->
								<!--</tr>-->
								<tr>
									<th>Trip ID</th>
									<th>Booking No</th>
									<th>Driver</th>
									<th>Rider</th>
									<th>Cab Type</th>
									<th>Destination (From)</th>
									<th>Destination (To)</th>
									<th>Trip Date</th>
									<th>Time</th>
									<th>Company</th>
									<th>Fare</th>
									<th>Trip Status</th>
									<th>Invoice</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($trips as $trip)
								@if($trip->trip_status == 'completed')


								<tr class="gradeX">
									<td class="center">{{$trip->id}}</td>
									<td class="center">{{$trip->booking_no}}</td>
									<td class="center">
										@if(is_null($trip->fk_driver_id))
										{{ '---' }} 

										@else
										{{$trip->driver->name}}
										@endif
									</td>
									<td class="center">
										@if(is_null($trip->rider))
										{{ '---' }} 
										@else
										{{ $trip->rider->name }}
										@endif
									</td>
									<td class="center"> {{$trip->driver->car_model_name}}</td>
									<td class="center">{{$trip->destination_from}}</td>
									<td class="center">{{$trip->destination_to}}</td>
									<td class="center">{{$trip->trip_date}}</td>
									<td class="center">{{date('H:i A', strtotime($trip->trip_ended_at))}}</td>
									<td class="center">
										@if(is_null($trip->company))
										{{ '---' }} 
										@else
										{{ $trip->company->company_name }}
										@endif
									</td>
									<td class="center">{{$trip->fare}}</td>
									<td class="center">

										@if($trip->trip_status == 'completed')
										<span class="badge badge-success" style="background-color: green;">Success</span>

										@elseif($trip->trip_status == 'cancelled')
										<span class="badge badge-danger" >Canceled</span>
										@endif
									</td>
									<td class="center"><a href="{{ url('trip-invoice') }}/{{$trip->id}}"
										 class="label label-success">View Invoice</span>
										</a>
										</td>
								</tr>
								@endif
								@endforeach

							</tbody>
						</table>


					</div>
				</div>
			</div>
		</div>
	</div>
	@endsection
	@section('script-dashboard')
	<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
	<script>
		$(document).ready(function(){
			var ctx = document.getElementById('myChart').getContext('2d');
			var chart = new Chart(ctx, {
			// The type of chart we want to create
			type: 'pie',
			// The data for our dataset
			data: {
				labels: ['Successful Ride count', 'Canceled Ride Count'],
				datasets: [{
					label: 'My First dataset',
					backgroundColor: [
					'rgb(255, 131, 115)',
					'rgb(85, 216, 254)'
					],
				//	borderColor: 'rgb(255, 99, 132)',
				data: ['{{$completed}}', '{{$cancelled}}']
			}]
		},
				// Configuration options go here
				options: {
					cutoutPercentage: 60,
					responsive: false,
					legend: {
					// Or you can do it here
					display: false
				},

			}
		});
			$("#js-legend").html(chart.generateLegend());





			$('.dataTables-admin').DataTable({
				"lengthChange": false,
				"searching": false,
				"bInfo" : false,
				"bSort" : false,

				responsive: true,

				language: {
					paginate: {
						next: '<i class="fas fa-angle-right fa-lg"></i>',
						previous: '<i class="fas fa-angle-left fa-lg"></i>'  
					}
				}

			});

		});

	</script>
	<script>
		$(document).ready(function(){

			var ctx = document.getElementById('myCharts').getContext('2d');
			var chart = new Chart(ctx, {
				// The type of chart we want to create
				type: 'pie',

				// The data for our dataset
				data: {
					labels: ['Active','In Active'],
					datasets: [{
						label: 'My First dataset',
						backgroundColor: [
						'rgb(255, 131, 115)',
						'rgb(85, 216, 254)'
						],
						//borderColor: 'rgb(255, 99, 132)',
						data: ['{{$active}}', '{{$Inactive}}',0,0]
					}]
				},
					// Configuration options go here
					options: {
						cutoutPercentage: 60,
						responsive: false,
						legend: {
						// Or you can do it here
						display: false
					},

				}
			});
			$("#js-legend2").html(chart.generateLegend());
		});
	</script>
	@stop